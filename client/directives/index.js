'use strict';

const angular = require('angular');

export default angular.module('gfgApp.gfgDirectives', [])
  .name;
