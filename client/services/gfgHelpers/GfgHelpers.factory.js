'use strict';

const angular = require('angular');

export default function GfgHelpers($document, $anchorScroll, $state, $stateParams) {
  'ngInject';

  let factoryObject = {};

  factoryObject.refreshPage = function(stateOptions = {}, scrollToTop = false) {
    let defaultStateOptions = {
      reload: true,
      inherit: false,
      notify: true
    };

    let stateOpts = angular.extend({}, defaultStateOptions, stateOptions);
    $state.transitionTo($state.current, $stateParams, stateOpts);

    if(scrollToTop) {
      $anchorScroll();
    }
  };


  factoryObject.setPageTitle = function(title = null) {
    if(title) {
      $document[0].title = `GoGas - ${title}`;
    }
    else {
      $document[0].title = 'GoGas';
    }
  };


  factoryObject.capitalizeFirstLetterOfString = function(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  return factoryObject;
}
