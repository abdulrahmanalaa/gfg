'use strict';

const angular = require('angular');

import GfgHelpers from './GfgHelpers.factory';

export default angular.module('gfgApp.gfgHelpers', [])
  .factory('GfgHelpers', GfgHelpers)
  .name;
