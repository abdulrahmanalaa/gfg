'use strict';

const angular = require('angular');

import modalDeleteController from './controllers/modal-delete';
import modalCustomerSelectController from './controllers/modal.customer-select';
import modalAddEditSupplierContact from './controllers/modal-add.edit-supplier.contact';
import modalAddEditSupplierPricing from './controllers/modal-add.edit-supplier.pricing';
import modalSalesOrderCompleteController from './controllers/sales-order-complete';
import modalSalesOrderCancelController from './controllers/sales-order-cancel';

export default function GfgModal($q, $uibModal, toastr, apiBaseUrl, DbRootResource) {
  'ngInject';

  let factoryObject = {};
  let modalDefaultOptions = {
    animation: true,
    ariaLabelledBy: 'modal-title',
    ariaDescribedBy: 'modal-body',
    template: require('./templates/modal-delete.html'),
    size: 'md',
    appendTo: undefined,
    resolve: {},
    controller: function() {},
    controllerAs: 'vm'
  };

  factoryObject.openDeleteModal = function(modalOptions, callback) {
    modalDefaultOptions.controller = modalDeleteController;

    let cb = callback || angular.noop;
    let opts = Object.assign({}, modalDefaultOptions, modalOptions);

    return $uibModal.open(opts)
      .result
      .then(
        () => cb(true),
        () => cb(false)
      );
  };


  factoryObject.openSalesOrderCompleteModal = function(modalOptions, callback) {
    modalDefaultOptions.template = require('./templates/sales-order-complete.html');
    modalDefaultOptions.controller = modalSalesOrderCompleteController;
    modalDefaultOptions.size = 'lg';

    let cb = callback || angular.noop;
    let opts = Object.assign({}, modalDefaultOptions, modalOptions);

    return $uibModal.open(opts)
      .result
      .then(
        () => cb(true),
        () => cb(false)
      );
  };


  factoryObject.openSalesOrderCancelModal = function(modalOptions, callback) {
    modalDefaultOptions.template = require('./templates/sales-order-cancel.html');
    modalDefaultOptions.controller = modalSalesOrderCancelController;
    modalDefaultOptions.size = 'lg';
    modalOptions.resolve = {
      TaxonomyOrderCancellationReasons: function() {
        let defer = $q.defer();

        DbRootResource.list(`${apiBaseUrl}/taxonomy.orderCancelReasons`)({ fields: 'title' }, (data, error) => {
          if(error) {
            defer.reject(error);
            return;
          }
          defer.resolve(data);
        });

        return defer.promise;
      }
    };

    let cb = callback || angular.noop;
    let opts = Object.assign({}, modalDefaultOptions, modalOptions);

    return $uibModal.open(opts)
      .result
      .then(
        (data) => cb(data),
        () => cb(false)
      );
  };


  factoryObject.openCustomerSelectModal = function(modalOptions, customersList, callback) {
    modalDefaultOptions.template = require('./templates/modal.customer-select.html');
    modalDefaultOptions.controller = modalCustomerSelectController;
    modalDefaultOptions.size = 'lg';
    modalOptions.resolve = {
      customersList: function() {
        return customersList;
      }
    };

    let cb = callback || angular.noop;
    let opts = angular.extend({}, modalDefaultOptions, modalOptions);

    return $uibModal.open(opts)
      .result
      .then(
        (res) => cb(res),
        () => cb(false)
      );
  };


  /**
   * Supplier contact modal.
   *
   * @param modalOptions
   * @param supplierContact
   * @param callback
   */
  factoryObject.openSupplierContactModal = function(modalOptions, supplierContact, callback) {
    modalDefaultOptions.template = require('./templates/modal-add.edit-supplier.contact.html');
    modalDefaultOptions.controller = modalAddEditSupplierContact;
    modalDefaultOptions.size = 'lg';
    modalOptions.bindToController = true;

    modalOptions.resolve = {
      supplierContact: function() {
        return supplierContact;
      },
      // get list of designations from database.
      dbDesignations: function() {
        let queryParams = { fields: 'title', sort: 'title', direction: 'asc' };
        let deferred = $q.defer();
        DbRootResource.list(`${apiBaseUrl}/taxonomy.designations`)(queryParams, (data, error) => {
          if(data) {
            deferred.resolve(data);
          }
          else {
            deferred.reject(error);
          }
        });
        return deferred.promise;
      }
    };

    let cb = callback || angular.noop;
    let opts = angular.extend({}, modalDefaultOptions, modalOptions);

    return $uibModal.open(opts)
      .result
      .then(
        (res) => cb(res),
        (res) => cb(res)
      );
  };


  /**
   * Supplier pricing info modal.
   *
   * @param modalOptions
   * @param supplierPricing
   * @param callback
   */
  factoryObject.openSupplierPricingModal = function(modalOptions, supplierPricing, callback) {
    modalDefaultOptions.template = require('./templates/modal-add.edit-supplier.pricing.html');
    modalDefaultOptions.controller = modalAddEditSupplierPricing;
    modalDefaultOptions.size = 'lg';

    modalOptions.resolve = {
      supplierPricing: function() {
        return supplierPricing;
      },
      // get list of materials from database.
      dbMaterials: function() {
        let queryParams = { fields: 'title', sort: 'title', direction: 'asc' };
        let deferred = $q.defer();
        DbRootResource.list(`${apiBaseUrl}/materials`)(queryParams, (data, error) => {
          if(data) {
            deferred.resolve(data);
          }
          else {
            deferred.reject(error);
          }
        });
        return deferred.promise;
      }
    };

    let cb = callback || angular.noop;
    let opts = angular.extend({}, modalDefaultOptions, modalOptions);

    return $uibModal.open(opts)
      .result
      .then(
        (res) => cb(res),
        (res) => cb(res)
      );
  };


  return factoryObject;
}
