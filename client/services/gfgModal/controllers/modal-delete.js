'use strict';

export default function modalDeleteController($scope, $uibModalInstance) {
  'ngInject';

  $scope.yes = function() {
    $uibModalInstance.close(true);
  };

  $scope.no = function() {
    $uibModalInstance.dismiss('cancel');
  };
}
