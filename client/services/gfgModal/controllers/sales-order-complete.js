'use strict';

export default function modalSalesOrderCompleteController($scope, $uibModalInstance) {
  'ngInject';

  $scope.yes = function() {
    $uibModalInstance.close(true);
  };

  $scope.no = function() {
    $uibModalInstance.dismiss('cancel');
  };
}
