'use strict';

const _ = require('lodash');

// export default function modalAddEditSupplierContact($scope, $uibModalInstance, toastr, apiBaseUrl, x, DbRootResource, supplierContact, supplierContactOriginal) {
export default function modalAddEditSupplierContact($scope, $uibModalInstance, toastr, apiBaseUrl, dbDesignations, supplierContact) {
  'ngInject';

  $scope.dbDesignations = dbDesignations;
  $scope.contactItem = {};
  $scope.contactItemOriginal = supplierContact;
  $scope.addMode = _.isEmpty(supplierContact);
  $scope.editMode = !$scope.addMode;

  if($scope.editMode) {
    $scope.contactItem = $scope.contactItemOriginal;
  }

  // console.log(supplierContact, 'supplierContact');
  // console.log($scope.contactItem, '$scope.contactItem');
  // console.log(dbDesignations, 'dbDesignations');

  $scope.add = function() {
    if($scope.form.$valid) {
      $uibModalInstance.close($scope.contactItem);
    }
  };

  $scope.cancel = function() {
    alert('cancel');
    // console.log(supplierContactOriginal, supplierContactOriginal);
    // $uibModalInstance.close(null);
    // $uibModalInstance.close($scope.contactItemOriginal);
    $uibModalInstance.dismiss(null);
  };
}
