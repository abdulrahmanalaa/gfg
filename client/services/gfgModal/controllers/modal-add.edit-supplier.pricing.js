'use strict';

export default function modalAddEditSupplierContact($scope, $uibModalInstance, dbMaterials, supplierPricing) {
  'ngInject';

  $scope.item = {};
  $scope.dbMaterials = dbMaterials;
  $scope.dateFormat = 'dd/MM/yyyy';
  $scope.datePopupOptions = {
    dateDisabled: false,
    formatYear: 'yyyy',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };

  $scope.add = function() {
    if($scope.form.$valid) {
      $uibModalInstance.close($scope.item);
    }
  };

  $scope.cancel = function() {
    alert('cancel');
    // console.log(supplierContactOriginal, supplierContactOriginal);
    // $uibModalInstance.close(null);
    // $uibModalInstance.close($scope.contactItemOriginal);
    $uibModalInstance.dismiss(null);
  };
}
