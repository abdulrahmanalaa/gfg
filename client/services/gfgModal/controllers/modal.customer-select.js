'use strict';

export default function modalCustomerSelectController($scope, $uibModalInstance, customersList) {
  'ngInject';

  $scope.customersList = customersList;

  $scope.selectCustomer = function(idx) {
    $uibModalInstance.close($scope.customersList[idx]);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss(false);
  };
}
