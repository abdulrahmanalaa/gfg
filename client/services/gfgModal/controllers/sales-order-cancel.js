'use strict';

export default function modalSalesOrderCancelController($scope, $uibModalInstance, TaxonomyOrderCancellationReasons) {
  'ngInject';

  $scope.reasons = TaxonomyOrderCancellationReasons;

  $scope.yes = function() {
    $uibModalInstance.close($scope.selectedReason);
  };

  $scope.no = function() {
    $uibModalInstance.dismiss('cancel');
  };
}
