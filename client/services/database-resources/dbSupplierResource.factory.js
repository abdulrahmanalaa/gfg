'use strict';

const angular = require('angular');

export default function DbSupplierResource($resource, apiBaseUrl, DbRootResource) {
  'ngInject';

  let baseEndpoint = `${apiBaseUrl}/suppliers`;

  return {
    init: function() {
      return $resource(baseEndpoint,
        {},
        {
          updateMultipleSuppliers: {
            method: 'POST',
            url: `${baseEndpoint}/updateMultiple`
          },
          deleteMultipleSuppliers: {
            method: 'POST',
            url: `${baseEndpoint}/deleteMultiple`
          }
        }
      );
    },


    /**
     * {@inheritDoc}
     * @see: DbRootResource.get()
     */
    get: DbRootResource.get(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.list()
     */
    list: DbRootResource.list(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.listAdmin()
     */
    listAdmin: DbRootResource.listAdmin(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.create()
     */
    create: DbRootResource.create(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.update()
     */
    update: DbRootResource.update(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.delete()
     */
    delete: DbRootResource.delete(baseEndpoint),


    updateMultiple: function(suppliersIds, fieldsAndValuesToUpdate, callback) {
      let cb = callback || angular.noop;

      return this.init()
        .updateMultipleSuppliers({ ids: suppliersIds, fields: fieldsAndValuesToUpdate })
        .$promise
        .then(
          (data) => cb(data, null),
          (error) => cb(null, error)
        );
    },


    deleteMultiple: function(suppliersIds, callback) {
      let cb = callback || angular.noop;

      return this.init()
        .deleteMultipleSuppliers({ ids: suppliersIds })
        .$promise
        .then(
          (data) => cb(data, null),
          (error) => cb(null, error)
        );
    }
  };
}
