'use strict';

const angular = require('angular');

export default function DbRootResource($resource) {
  'ngInject';

  let factoryObject = {};

  /**
   * Initialize new request.
   *
   * Usage:
   *      var endpoint = DbRootResource.initialize('/path/to/endpoint');
   *
   * @param endpoint
   * @returns angular $resource
   */
  factoryObject.initialize = function(endpoint) {
    return $resource(endpoint + '/:id/:controller',
      {
        id: '@_id'
      },
      {
        update: {
          method: 'PUT'
        }
      });
  };


  /**
   * Lists all resources (optionally filtered by queryParams object).
   *
   * Usage:
   *      DbRootResource.list({fields: 'name,age'}, (data, err) => {});
   *
   * @param endpoint
   * @returns {Function}
   */
  factoryObject.list = function(endpoint) {
    return function(queryParams, callback) {
      let cb = callback || angular.noop;
      queryParams = queryParams || {};

      return factoryObject.initialize(endpoint)
        .query(queryParams,
          (data) => cb(data, null),
          (error) => cb(null, error));
    };
  };


  /**
   * Lists all resources for admin (optionally filtered by queryParams object).
   *
   * Usage:
   *      DbRootResource.listAdmin({fields: 'name,age'}, (data, err) => {});
   *
   * @param endpoint
   * @returns {Function}
   */
  factoryObject.listAdmin = function (endpoint) {
    return function(queryParams, callback) {
      let cb = callback || angular.noop;
      queryParams = queryParams || {};

      endpoint = endpoint + '/indexAdmin';
      return factoryObject.initialize(endpoint)
        .query(queryParams,
          (data) => cb(data, null),
          (error) => cb(null, error));
    };
  };


  /**
   * Gets a single resource.
   *
   * Usage:
   *      DbRootResource.get('RESOURCE_ID', (data, err) => {});
   *
   * @param endpoint
   * @returns {Function}
   */
  factoryObject.get = function(endpoint) {
    return function(id, callback) {
      let cb = callback || angular.noop;

      return factoryObject.initialize(endpoint)
        .get({ id: id },
          (data) => cb(data, null),
          (error) => cb(null, error));
    };
  };


  /**
   * Creates a new resource.
   *
   * Usage:
   *      DbRootResource.create({DB_VALUES}, (data, err) => {})
   *
   * @param endpoint
   * @returns {Function}
   */
  factoryObject.create = function(endpoint) {
    return function(item, callback) {
      let cb = callback || angular.noop;

      return factoryObject.initialize(endpoint)
        .save(item,
          (data) => cb(data, null),
          (err) => cb(null, err));
    };
  };


  /**
   * Updates existing resource.
   *
   * Usage:
   *      DbRootResource.update('RESOURCE_ID', {DB_VALUES}, (data, err) => {})
   *
   * @param endpoint
   * @returns {Function}
   */
  factoryObject.update = function(endpoint) {
    return function(id, item, callback) {
      let cb = callback || angular.noop;

      return factoryObject.initialize(endpoint)
        .update({ id: id },
          item,
          (data) => cb(data, null),
          (err) => cb(null, err));
    };
  };


  /**
   * Deletes existing resource.
   *
   * Usage:
   *      DbRootResource.delete('RESOURCE_ID', (data, err) => {})
   *
   * @param endpoint
   * @returns {Function}
   */
  factoryObject.delete = function(endpoint) {
    return function(id, callback) {
      let cb = callback || angular.noop;

      return factoryObject.initialize(endpoint)
        .delete({ id: id },
          (data) => cb(data, null),
          (error) => cb(null, error));
    };
  };


  return factoryObject;
}
