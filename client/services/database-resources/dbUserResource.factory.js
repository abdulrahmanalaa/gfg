'use strict';

export default function DbUserResource(apiBaseUrl, DbRootResource) {
  'ngInject';

  let baseEndpoint = `${apiBaseUrl}/users`;

  return {
    /**
     * {@inheritDoc}
     * @see: DbRootResource.get()
     */
    get: DbRootResource.get(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.list()
     */
    list: DbRootResource.list(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.listAdmin()
     */
    listAdmin: DbRootResource.listAdmin(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.create()
     */
    create: DbRootResource.create(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.update()
     */
    update: DbRootResource.update(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.delete()
     */
    delete: DbRootResource.delete(baseEndpoint)
  };
}
