'use strict';

const angular = require('angular');

export default function DbTaxonomyAddressSectionResource($resource, apiBaseUrl, DbRootResource) {
  'ngInject';

  let baseEndpoint = `${apiBaseUrl}/taxonomy.addressSections`;

  return {
    init: function() {
      return $resource(baseEndpoint,
        {},
        {
          listBySectionType: {
            method: 'GET',
            url: `${baseEndpoint}/listBySectionType/:sectionType`,
            isArray: true
          },
          listByParentId: {
            method: 'GET',
            url: `${baseEndpoint}/listByParentId/:parentId`,
            isArray: true
          }
        }
      );
    },


    /**
     * {@inheritDoc}
     * @see: DbRootResource.get()
     */
    get: DbRootResource.get(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.list()
     */
    list: DbRootResource.list(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.listAdmin()
     */
    listAdmin: DbRootResource.listAdmin(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.create()
     */
    create: DbRootResource.create(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.update()
     */
    update: DbRootResource.update(baseEndpoint),


    /**
     * {@inheritDoc}
     * @see: DbRootResource.delete()
     */
    delete: DbRootResource.delete(baseEndpoint),


    listBySectionType: function(sectionType, callback) {
      let cb = callback || angular.noop;

      return this.init()
        .listBySectionType({ sectionType: sectionType })
        .$promise
        .then(
          (data) => cb(data, null),
          (error) => cb(null, error)
        );
    },


    listByParentId: function(parentId, callback) {
      let cb = callback || angular.noop;

      return this.init()
        .listByParentId({ parentId: parentId })
        .$promise
        .then(
          (data) => cb(data, null),
          (error) => cb(null, error)
        );
    },


    deleteMultiple: function(suppliersIds, callback) {
      let cb = callback || angular.noop;

      return this.init()
        .deleteMultipleSuppliers({ ids: suppliersIds })
        .$promise
        .then(
          (data) => cb(data, null),
          (error) => cb(null, error)
        );
    }
  };
}
