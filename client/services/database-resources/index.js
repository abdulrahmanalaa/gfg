'use strict';

const angular = require('angular');

import DbRootResource from './dbRootResource.factory';
import DbUserResource from './dbUserResource.factory';
import DbSupplierResource from './dbSupplierResource.factory';
import DbTaxonomyAddressSectionResource from './dbTaxonomyAddressSectionResource.factory';

export default angular.module('gfgApp.databaseResources', [])
  .factory('DbRootResource', DbRootResource)
  .factory('DbUserResource', DbUserResource)
  .factory('DbSupplierResource', DbSupplierResource)
  .factory('DbTaxonomyAddressSectionResource', DbTaxonomyAddressSectionResource)
  .name;
