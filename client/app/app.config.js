'use strict';

const _ = require('lodash');
const angular = require('angular');

export function routeConfig($urlRouterProvider, $locationProvider) {
  'ngInject';
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
}


export function appCachedTemplates($templateCache) {
  $templateCache.put('google-map-search-box.html', require('../templates/google-map-search-box.html'));
}


// config: angular-toastr
export function angularToastrConfig(toastrConfig) {
  'ngInject';

  angular.extend(toastrConfig, {
    autoDismiss: false,
    allowHtml: true,
    closeButton: false,
    timeOut: 10000,
    extendedTimeOut: 1000,
    containerId: 'toast-container',
    maxOpened: 0,
    progressBar: false,
    tapToDismiss: true,
    newestOnTop: true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: false,
    target: 'body',
    toastClass: 'toast',
    titleClass: 'toast-title',
    messageClass: 'toast-message',
    iconClasses: {
      error: 'toast-error',
      info: 'toast-info',
      success: 'toast-success',
      warning: 'toast-warning'
    },
    onHidden: null,
    onShown: null,
    onTap: null
  });
}


// config: angular-xeditable
export function angularXeditableConfig(editableOptions) {
  'ngInject';
  // editableThemes.bs3.inputClass = 'input-sm';
  // editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3'; // or 'bs2', 'default'
  editableOptions.buttons = 'right'; // or 'no' to hide buttons
}


// config: angular-loading-bar
export function angularLoadingBarConfig(cfpLoadingBarProvider) {
  'ngInject';
  cfpLoadingBarProvider.includeBar = true;
  cfpLoadingBarProvider.includeSpinner = true;
  cfpLoadingBarProvider.latencyThreshold = 100;
  //cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
  //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
}


// config: angular-google-maps
export function angularGoogleMapsConfig(uiGmapGoogleMapApiProvider, GOOGLE_MAPS_API_KEYS) {
  'ngInject';
  let selectedApiKey = _.sample(GOOGLE_MAPS_API_KEYS);

  uiGmapGoogleMapApiProvider.configure({
    key: selectedApiKey,
    // v: '3.17', //defaults to latest 3.X
    libraries: 'places'
    // libraries: 'weather,geometry,visualization'
  });
}
