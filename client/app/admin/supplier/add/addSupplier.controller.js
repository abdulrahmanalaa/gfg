'use strict';

const _ = require('lodash');

export default class addSupplierController {

  pageTitle = 'Add Supplier';
  formSubmitted = false;
  mapSelectedLatLng = {
    latitude: null,
    longitude: null
  };
  supplier = {
    status: true,
    contactInfo: [],
    pricingInfo: [],
    address: {
      googleMap: {}
    }
  };
  mapOptions = {
    scrollwheel: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    mapTypeId: 'roadmap'
  };
  mapMarker = {
    id: 1,
    show: true,
    coords: {},
    options: {
      draggable: true,
      labelContent: 'Drag the pin to the desired location',
      labelAnchor: '100 70',
      labelClass: 'google-map-label-style-1'
    },
    events: {}
  };
  mapSearchBox = {
    template: 'google-map-search-box.html',
    position: 'TOP_RIGHT',
    options: {
      bounds: null
    },
    events: {
      places_changed: () => {} // @see setMapSearchBoxEvents().
    }
  };
  dbMaterials = [];
  dbSupplierGroups = [];
  dbDesignations = [];
  addressGovernorates = [];
  addressAreas = [];
  addressBlocks = [];

  /*@ngInject*/
  constructor($log, $state, toastr, DbSupplierResource, GfgModal, GfgHelpers, DbRootResource, DbTaxonomyAddressSectionResource, apiBaseUrl, GOOGLE_MAPS_DEFAULT_CENTER) {
    this.$log = $log;
    this.$state = $state;
    this.toastr = toastr;
    this.DbSupplierResource = DbSupplierResource;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    this.apiBaseUrl = apiBaseUrl;
    this.GOOGLE_MAPS_DEFAULT_CENTER = GOOGLE_MAPS_DEFAULT_CENTER;

    this.GetDbMaterialsResource = this.DbRootResource.list(`${this.apiBaseUrl}/materials`);
    this.GetDbSupplierGroupsResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.supplierGroups`);
    this.GetDbDesignationsResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.designations`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.events.dragend = (marker, eventName, args) => {
      this.mapSelectedLatLng.latitude = marker.getPosition().lat();
      this.mapSelectedLatLng.longitude = marker.getPosition().lng();
    };

    this.setMapSearchBoxEvents();
    this.getDbMaterials();
    this.getDbSupplierGroups();
    this.getDbDesignations();
    this.getAddressGovernorates();
  }


  save(addAnotherItem = false) {
    this.formSubmitted = true;
    let emptyAddressField = { _id: null };

    if(this.form.$valid) {
      if(_.isEqual(this.supplier.address.governorate, emptyAddressField)) {
        this.supplier.address.governorate = null;
      }
      if(_.isEqual(this.supplier.address.area, emptyAddressField)) {
        this.supplier.address.area = null;
      }
      if(_.isEqual(this.supplier.address.block, emptyAddressField)) {
        this.supplier.address.block = null;
      }

      this.DbSupplierResource.create(this.supplier, (res, err) => {
        if(err) {
          this.toastr.error('Error saving supplier');
          this.$log.error(err);
          return;
        }
        this.toastr.success('Successfully created supplier');
        if(addAnotherItem) {
          this.GfgHelpers.refreshPage({}, true);
        }
        else {
          this.$state.go('adminSupplierList');
        }
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  cancel() {
    this.$state.go('adminSupplierList');
  }


  setMapSearchBoxEvents() {
    // set mapSearchBox places_changed here to insure the validity of this.mapSelectedLatLng.
    this.mapSearchBox.events.places_changed = (searchBox) => {
      let places = searchBox.getPlaces();
      if(!_.isEmpty(places)) {
        this.mapSelectedLatLng.latitude = places[0].geometry.location.lat();
        this.mapSelectedLatLng.longitude = places[0].geometry.location.lng();
      }
    };
  }


  getDbMaterials() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbMaterialsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving materials');
        return;
      }
      this.dbMaterials = data;
    });
  }


  getDbSupplierGroups() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbSupplierGroupsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving designations');
        return;
      }
      this.dbSupplierGroups = data;
    });
  }


  getDbDesignations() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbDesignationsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving designations');
        return;
      }
      this.dbDesignations = data;
    });
  }


  getAddressGovernorates() {
    this.DbTaxonomyAddressSectionResource.listBySectionType('governorate', (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving governorates.');
        return;
      }
      this.addressGovernorates = data;
    });
  }


  updateAddressAreas(governorateId) {
    if(governorateId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(governorateId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving areas.');
          return;
        }
        this.addressAreas = data;
      });
    }
  }


  updateAddressBlocks(areaId) {
    if(areaId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(areaId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving blocks.');
          return;
        }
        this.addressBlocks = data;
      });
    }
  }


  /**
   * Helper function to set location lat/lon after dragging the map marker.
   *
   * @param propertyName
   *  latitude or longitude.
   *
   * @param propertyValue
   *  Value to assign to propertyName
   */
  setMapLocationProperty(propertyName, propertyValue) {
    this.supplier.address.googleMap[propertyName] = propertyValue;
  }

  googleMapCapture() {
    this.supplier.address.googleMap.latitude = this.mapSelectedLatLng.latitude;
    this.supplier.address.googleMap.longitude = this.mapSelectedLatLng.longitude;
  }

  googleMapClear() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.supplier.address.googleMap.latitude = null;
    this.supplier.address.googleMap.longitude = null;
  }

  addContactInfo() {
    this.supplier.contactInfo.push({});
  }

  deleteContactInfo(idx) {
    this.supplier.contactInfo.splice(idx, 1);
  }

  addMaterialInfo() {
    this.supplier.pricingInfo.push({});
  }

  deleteMaterialInfo(idx) {
    this.supplier.pricingInfo.splice(idx, 1);
  }

  formClear() {
    this.GfgHelpers.refreshPage({}, false);
  }
}
