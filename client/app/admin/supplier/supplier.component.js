'use strict';
const _ = require('lodash');
const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './supplier.routes';

export class adminSupplierListComponent {

  pageTitle = 'Suppliers';
  suppliers = [];
  selectedSupplierIds = {};
  selectedSupplierIdsFiltered = [];

  /*@ngInject*/
  constructor($state, $stateParams, toastr, GfgModal, GfgHelpers, DbSupplierResource) {
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbSupplierResource = DbSupplierResource;

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  $onInit() {
    this.DbSupplierResource.list({}, (data, err) => {
      if(err) {
        this.toastr.error('Error loading suppliers. try again later.');
        return;
      }

      this.suppliers = data;
    });
  }


  updateSelectedSupplierIdsFiltered() {
    this.selectedSupplierIdsFiltered = [];
    angular.forEach(this.selectedSupplierIds, (_val, _key) => {
      if(_val === true) {
        this.selectedSupplierIdsFiltered.push(_key);
      }
    });

    return this.selectedSupplierIdsFiltered;
  }


  deleteSupplier(supplierId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.DbSupplierResource.delete(supplierId, (data, err) => {
          if(err) {
            this.toastr.error('Error deleting supplier. Try again Later.');
            return;
          }

          this.toastr.success('Successfully deleted supplier.');
          this.GfgHelpers.refreshPage();
        });
      }
    });
  }


  updateMultiple(fieldsToUpdate) {
    this.updateSelectedSupplierIdsFiltered();

    this.DbSupplierResource.updateMultiple(this.selectedSupplierIdsFiltered, fieldsToUpdate, (data, err) => {
      if(err) {
        this.toastr.error('Error updating suppliers. Try again Later.');
        return;
      }

      this.toastr.success('Successfully updated suppliers.');
      this.GfgHelpers.refreshPage();
    });
  }


  deleteMultiple() {
    this.updateSelectedSupplierIdsFiltered();

    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.DbSupplierResource.deleteMultiple(this.selectedSupplierIdsFiltered, (data, err) => {
          if(err) {
            this.toastr.error('Error updating suppliers. Try again Later.');
            return;
          }

          this.toastr.success('Successfully deleted suppliers.');
          this.GfgHelpers.refreshPage();
        });
      }
    });
  }
}

export default angular.module('gfgApp.adminSupplier', [uiRouter])
  .config(routes)
  .component('adminSupplierList', {
    template: require('./supplier.html'),
    controller: adminSupplierListComponent,
    controllerAs: 'vm'
  })
  .name;
