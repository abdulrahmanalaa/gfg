'use strict';

const _ = require('lodash');
const angular = require('angular');

export default class editSupplierController {

  pageTitle = 'Edit Supplier';
  formSubmitted = false;
  mapSelectedLatLng = {
    latitude: null,
    longitude: null
  };
  supplier = {
    pricingInfo: [],
    contactInfo: [],
    address: {
      googleMap: {}
    }
  };
  mapOptions = {
    scrollwheel: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    mapTypeId: 'roadmap'
  };
  mapMarker = {
    id: 1,
    show: true,
    coords: {},
    options: {
      draggable: true,
      labelContent: 'Drag the pin to the desired location',
      labelAnchor: '100 70',
      labelClass: 'google-map-label-style-1'
    },
    events: {}
  };
  mapSearchBox = {
    template: 'google-map-search-box.html',
    position: 'TOP_RIGHT',
    options: {
      bounds: null
    },
    events: {
      places_changed: () => {} // @see setMapSearchBoxEvents().
    }
  };
  dbMaterials = [];
  dbSupplierGroups = [];
  dbDesignations = [];
  addressGovernorates = [];
  addressAreas = [];
  addressBlocks = [];

  /*@ngInject*/
  constructor($log, $state, $stateParams, toastr, apiBaseUrl, GfgHelpers, DbSupplierResource, DbRootResource, DbTaxonomyAddressSectionResource, GOOGLE_MAPS_DEFAULT_CENTER) {
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.DbSupplierResource = DbSupplierResource;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    this.GOOGLE_MAPS_DEFAULT_CENTER = GOOGLE_MAPS_DEFAULT_CENTER;

    this.GetDbMaterialsResource = this.DbRootResource.list(`${this.apiBaseUrl}/materials`);
    this.GetDbSupplierGroupsResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.supplierGroups`);
    this.GetDbDesignationsResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.designations`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    // this.mapMarker.events.dragend = (marker, eventName, args) => {
    //   this.mapSelectedLatLng.latitude = marker.getPosition().lat();
    //   this.mapSelectedLatLng.longitude = marker.getPosition().lng();
    // };

    this.DbSupplierResource.get(this.$stateParams.supplierId, (data, err) => {
      if(err) {
        this.toastr.error('Error loading supplier. Try again later.');
        return;
      }
      this.supplier = data;
      this.$log.info(this.supplier);

      if(_.isEmpty(this.supplier.address.googleMap)) {
        this.supplier.address.googleMap = {};
      }
      if(_.isEmpty(this.supplier.address.googleMap.latitude) || _.isEmpty(this.supplier.address.googleMap.longitude)) {
        this.supplier.address.googleMap = {};
        this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
      }
      else {
        this.mapSelectedLatLng = this.supplier.address.googleMap;
        this.mapMarker.coords = this.supplier.address.googleMap;
      }
      this.fixLoadedSupplierFields();
      this.setMapSearchBoxEvents();
      this.getDbMaterials();
      this.getDbDesignations();
      this.getDbSupplierGroups();
      this.getAddressGovernorates();
      this.updateAddressAreas(this.supplier.address.governorate);
      this.updateAddressBlocks(this.supplier.address.area);
    });
  }


  save() {
    this.formSubmitted = true;
    let emptyAddressField = { _id: null };

    if(this.form.$valid) {
      if(_.isEqual(this.supplier.address.governorate, emptyAddressField)) {
        this.supplier.address.governorate = null;
      }
      if(_.isEqual(this.supplier.address.area, emptyAddressField)) {
        this.supplier.address.area = null;
      }
      if(_.isEqual(this.supplier.address.block, emptyAddressField)) {
        this.supplier.address.block = null;
      }

      this.supplier.changed = new Date();
      this.DbSupplierResource.update(this.$stateParams.supplierId, this.supplier, (data, err) => {
        if(err) {
          this.toastr.error('Error updating supplier. Try again later.');
          return;
        }

        this.toastr.success('Successfully updated supplier.');
        this.$state.go('adminSupplierList');
      });
    }
  }


  cancel() {
    this.$state.go('adminSupplierList');
  }


  fixLoadedSupplierFields() {
    // format date fields to work fine with calendar popup.
    angular.forEach(this.supplier.pricingInfo, (itm) => {
      if(itm.validFrom) {
        itm.validFrom = new Date(itm.validFrom);
      }
      if(itm.validTo) {
        itm.validTo = new Date(itm.validTo);
      }
    });
  }


  setMapSearchBoxEvents() {
    // set mapSearchBox places_changed here to insure the validity of this.mapSelectedLatLng.
    this.mapSearchBox.events.places_changed = (searchBox) => {
      let places = searchBox.getPlaces();
      if(!_.isEmpty(places)) {
        this.mapSelectedLatLng.latitude = places[0].geometry.location.lat();
        this.mapSelectedLatLng.longitude = places[0].geometry.location.lng();
      }
    };
  }


  getDbMaterials() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbMaterialsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving materials');
        return;
      }
      this.dbMaterials = data;
    });
  }


  getDbSupplierGroups() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbSupplierGroupsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving designations');
        return;
      }
      this.dbSupplierGroups = data;
    });
  }


  getDbDesignations() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbDesignationsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving designations');
        return;
      }
      this.dbDesignations = data;
    });
  }


  getAddressGovernorates() {
    this.DbTaxonomyAddressSectionResource.listBySectionType('governorate', (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving governorates.');
        return;
      }
      this.addressGovernorates = data;
    });
  }


  updateAddressAreas(governorateId) {
    if(governorateId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(governorateId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving areas.');
          return;
        }
        this.addressAreas = data;
      });
    }
  }


  updateAddressBlocks(areaId) {
    if(areaId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(areaId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving blocks.');
          return;
        }
        this.addressBlocks = data;
      });
    }
  }


  /**
   * Helper function to set location lat/lon after dragging the map marker.
   *
   * @param propertyName
   *  latitude or longitude.
   *
   * @param propertyValue
   *  Value to assign to propertyName
   */
  setMapLocationProperty(propertyName, propertyValue) {
    this.supplier.address.googleMap[propertyName] = propertyValue;
  }

  googleMapCapture() {
    this.supplier.address.googleMap.latitude = this.mapSelectedLatLng.latitude;
    this.supplier.address.googleMap.longitude = this.mapSelectedLatLng.longitude;
  }

  googleMapClear() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.supplier.address.googleMap.latitude = null;
    this.supplier.address.googleMap.longitude = null;
  }

  addContactInfo() {
    this.supplier.contactInfo.push({});
  }

  deleteContactInfo(idx) {
    this.supplier.contactInfo.splice(idx, 1);
  }

  addMaterialInfo() {
    this.supplier.pricingInfo.push({});
  }

  deleteMaterialInfo(idx) {
    this.supplier.pricingInfo.splice(idx, 1);
  }
}
