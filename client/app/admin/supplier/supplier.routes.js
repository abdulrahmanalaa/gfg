'use strict';

import addSupplierController from './add/addSupplier.controller';
import editSupplierController from './edit/editSupplier.controller';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminSupplierList', {
      url: '/admin/supplier',
      template: '<admin-supplier-list></admin-supplier-list>',
      authenticate: 'admin'
    })
    .state('adminSupplierAdd', {
      url: '/admin/supplier/add',
      template: require('./add/addSupplier.html'),
      controller: addSupplierController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminSupplierEdit', {
      url: '/admin/supplier/{supplierId}',
      template: require('./add/addSupplier.html'),
      controller: editSupplierController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
  ;
}
