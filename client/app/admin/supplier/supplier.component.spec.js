'use strict';

describe('Component: SupplierComponent', function() {
  // load the controller's module
  beforeEach(module('gfgApp.adminSupplier'));

  var SupplierComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    SupplierComponent = $componentController('supplier', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
