'use strict';

export default class editUserController {

  pageTitle = 'Edit User';
  formSubmitted = false;
  user = {};
  availableUserRoles = {
    user: 'User',
    admin: 'Admin'
  };
  passwordConfirm = '';
  dateFormat = 'dd/MM/yyyy';
  datePopupOpened = false;
  datePopupOptions = {
    dateDisabled: false,
    formatYear: 'yyyy',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };
  dbStorageList = [];

  /*@ngInject*/
  constructor($log, $state, $stateParams, toastr, apiBaseUrl, GfgHelpers, DbRootResource, DbUserResource) {
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;
    this.StorageResourceList = this.DbRootResource.list(`${apiBaseUrl}/storages`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getDbStorageList();

    this.DbUserResource.get(this.$stateParams.userId, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving data.');
        this.$state.go('AdminUserList');
        return;
      }
      this.user = data;
      this.$log.info(data);
      // convert mongodb date to javascript date.
      if(this.user.plateExpiration) {
        this.user.plateExpiration = new Date(this.user.plateExpiration);
      }
      if(this.user.civilIdExpiration) {
        this.user.civilIdExpiration = new Date(this.user.civilIdExpiration);
      }
      if(this.user.storageAssignmentStartDate) {
        this.user.storageAssignmentStartDate = new Date(this.user.storageAssignmentStartDate);
      }
    });
  }


  getDbStorageList() {
    let params = {
      fields: 'title',
      sort: 'title'
    };
    this.StorageResourceList(params, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving storage list. <br>Try again later.');
        this.$log.error(error);
        return;
      }
      this.dbStorageList = data;
    });
  }


  save() {
    this.formSubmitted = true;

    if(this.form.$valid) {
      // if role='admin'.
      if(this.user.role === 'admin') {
        this.user.userType = null; // set userType to null.
      }

      this.DbUserResource.update(this.$stateParams.userId, this.user, (data, error) => {
        if(error) {
          this.toastr.error('Error updating user data.');
          this.$state.go('adminUserList');
          this.$log.error(error);
          return;
        }
        this.toastr.success('Successfully updated user data.');
        this.$state.go('adminUserList');
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }
}
