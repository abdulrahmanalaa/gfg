'use strict';

describe('Component: AdminUserComponent', function() {
  // load the controller's module
  beforeEach(module('gfgApp.adminUser'));

  var AdminUserComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminUserComponent = $componentController('adminUser', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
