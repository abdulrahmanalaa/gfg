'use strict';

import addUserController from './add/addUser.controller';
import editUserController from './edit/editUser.controller';
import changePasswordController from './changePassword/changePassword.controller';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminUserList', {
      url: '/admin/user',
      template: '<admin-user-list></admin-user-list>',
      authenticate: 'admin'
    })
    .state('adminUserAdd', {
      url: '/admin/user/add',
      template: require('./add/addUser.html'),
      controller: addUserController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminUserEdit', {
      url: '/admin/user/{userId}',
      template: require('./add/addUser.html'),
      controller: editUserController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminChangeUserPassword', {
      url: '/admin/user/{userId}/password',
      template: require('./changePassword/changePassword.html'),
      controller: changePasswordController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
  ;
}
