'use strict';

export default class addUserController {

  pageTitle = 'Add User';
  user = { status: true, name: '', email: '', role: 'user', permissions: [] };
  formSubmitted = false;
  availableUserRoles = {
    user: 'User',
    admin: 'Admin'
  };
  passwordConfirm = '';
  dateFormat = 'dd/MM/yyyy';
  datePopupOpened = false;
  datePopupOptions = {
    dateDisabled: false,
    formatYear: 'yyyy',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };
  dbStorageList = [];

  /*@ngInject*/
  constructor($log, $state, $stateParams, toastr, apiBaseUrl, GfgHelpers, DbRootResource, DbUserResource) {
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;
    this.StorageResourceList = this.DbRootResource.list(`${apiBaseUrl}/storages`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getDbStorageList();
  }


  getDbStorageList() {
    let params = {
      fields: 'title',
      sort: 'title'
    };
    this.StorageResourceList(params, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving storage list. <br>Try again later.');
        this.$log.error(error);
        return;
      }
      this.dbStorageList = data;
    });
  }


  save(addAnother = false) {
    this.formSubmitted = true;

    if(this.form.$valid) {
      // if role='admin'.
      if(this.user.role === 'admin') {
        this.user.userType = null; // set userType to null.
      }

      this.DbUserResource.create(this.user, (data, error) => {
        if(error) {
          this.toastr.error('Error creating user. Try again later.');
          this.$log.error(error);
          return;
        }
        this.toastr.success('Successfully created user.');
        if(addAnother) {
          this.GfgHelpers.refreshPage({}, true);
        }
        else {
          this.$state.go('adminUserList');
        }
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  formClear() {
    this.GfgHelpers.refreshPage({}, false);
  }
}
