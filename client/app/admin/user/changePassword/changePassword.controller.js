'use strict';

export default class changePasswordController {

  pageTitle = 'Change user password';
  formSubmitted = false;
  newPassword = '';
  confirmPassword = '';

  /*@ngInject*/
  constructor($log, $state, $stateParams, toastr, apiBaseUrl, GfgHelpers, DbRootResource, DbUserResource) {
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;

    this.GfgHelpers.setPageTitle(this.pageTitle);
    this.adminChangeUserPasswordResource = this.DbRootResource.create(`${apiBaseUrl}/users/adminChangePassword`);
  }


  $onInit() {
    this.DbUserResource.get(this.$stateParams.userId, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving data.');
        this.$log.error(error);
        this.$state.go('AdminUserList');
        return;
      }
      this.user = data;
      this.$log.info(data);
    });
  }


  save() {
    this.formSubmitted = true;

    if(this.form.$valid) {
      let postFields = {
        userId: this.$stateParams.userId,
        newPassword: this.newPassword
      };
      this.adminChangeUserPasswordResource(postFields, (data, error) => {
        if(error) {
          this.$log.error(error);
          this.toastr.error('Could not update password. <br>Try again later.');
          this.$state.go('adminUserList');
          return;
        }

        this.toastr.success('Successfully updated password.');
        this.$state.go('adminUserList');
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }
}
