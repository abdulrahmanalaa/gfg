'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');

import routes from './adminUser.routes';

export class AdminUserComponent {

  pageTitle = 'Users';
  users = [];
  queryParams = {};

  /*@ngInject*/
  constructor($state, $stateParams, toastr, GfgModal, GfgHelpers, DbUserResource) {
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbUserResource = DbUserResource;

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    // get users list.
    this.DbUserResource.list(this.queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving data. Try again later.');
        return;
      }
      this.users = data;
    });
  }


  refreshPage() {
    this.$state.transitionTo(this.$state.current, this.$stateParams, {
      reload: true,
      inherit: false,
      notify: true
    });
  }


  deleteUser(userId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.DbUserResource.delete(userId, (data, error) => {
          if(error) {
            this.toastr.error('Could not delete user. Try again later.');
            this.refreshPage();
            return;
          }
          this.toastr.success('Successfully deleted user.');
          this.refreshPage();
        });
      }
    });
  }
}


export default angular.module('gfgApp.adminUser', [uiRouter])
  .config(routes)
  .component('adminUserList', {
    template: require('./adminUser.html'),
    controller: AdminUserComponent,
    controllerAs: 'vm'
  })
  .name;
