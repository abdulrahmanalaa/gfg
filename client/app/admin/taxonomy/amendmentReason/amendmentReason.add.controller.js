'use strict';

export default class adminTaxonomyAmendmentReasonAddController {

  apiEndpoint = '/api/taxonomy.amendmentReasons';
  pageTitle = 'Add Amendment Reason';
  item = { title: '', status: true };

  /*@ngInject*/
  constructor($state, toastr, GfgHelpers, DbRootResource) {
    this.$state = $state;
    this.toastr = toastr;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.addDatabaseItem = this.DbRootResource.create(this.apiEndpoint);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  save(addAnotherItem = false) {
    this.addDatabaseItem(this.item, (data, error) => {
      if(error) {
        this.toastr.error('Could not add item. Try again later.');
        return;
      }

      this.toastr.success(`Successfully added amendment reason: ${this.item.title}`);
      this.item = { title: '', status: false };
    });

    if(!addAnotherItem) {
      this.$state.go('adminTaxonomyAmendmentReasonList');
    }
  }

  cancel() {
    this.$state.go('adminTaxonomyAmendmentReasonList');
  }
}
