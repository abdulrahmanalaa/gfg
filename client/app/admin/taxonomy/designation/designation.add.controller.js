'use strict';

export default class adminTaxonomyDesignationAddController {

  apiEndpoint = '/api/taxonomy.designations';
  pageTitle = 'Add Designation';
  item = { title: '', status: true };

  /*@ngInject*/
  constructor($state, toastr, DbRootResource, GfgHelpers) {
    this.$state = $state;
    this.toastr = toastr;
    this.DbRootResource = DbRootResource;
    this.GfgHelpers = GfgHelpers;
    this.addDatabaseItem = this.DbRootResource.create(this.apiEndpoint);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  save(addAnotherItem = false) {
    this.addDatabaseItem(this.item, (data, error) => {
      if(error) {
        this.toastr.error('Could not add item. Try again later.');
        return;
      }

      this.toastr.success(`Successfully added designation: ${this.item.title}`);
      this.item = { title: '', status: false };
    });

    if(!addAnotherItem) {
      this.$state.go('adminTaxonomyDesignationList');
    }
  }

  cancel() {
    this.$state.go('adminTaxonomyDesignationList');
  }
}
