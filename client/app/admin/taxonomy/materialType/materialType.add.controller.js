'use strict';

export default class adminTaxonomyMaterialTypeAddController {

  apiEndpoint = '/api/taxonomy.materialTypes';
  pageTitle = 'Add Material Type';
  item = { title: '', status: true };

  /*@ngInject*/
  constructor($state, toastr, DbRootResource, GfgHelpers) {
    this.$state = $state;
    this.toastr = toastr;
    this.DbRootResource = DbRootResource;
    this.GfgHelpers = GfgHelpers;
    this.addDatabaseItem = this.DbRootResource.create(this.apiEndpoint);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  save(addAnotherItem = false) {
    this.addDatabaseItem(this.item, (data, error) => {
      if(error) {
        this.toastr.error('Could not add item. Try again later.');
        return;
      }

      this.toastr.success(`Successfully added material type: ${this.item.title}`);
      this.item = { title: '', status: false };
    });

    if(!addAnotherItem) {
      this.$state.go('adminTaxonomyMaterialTypeList');
    }
  }

  cancel() {
    this.$state.go('adminTaxonomyMaterialTypeList');
  }
}
