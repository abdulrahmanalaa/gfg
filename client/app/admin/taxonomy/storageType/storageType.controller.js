'use strict';

export default class adminTaxonomyStorageTypeListController {

  apiEndpoint = '/api/taxonomy.storageTypes';
  pageTitle = 'Storage Types';
  addNewLink = {
    title: 'Add new Storage Type',
    state: 'adminTaxonomyStorageTypeAdd'
  };
  list = [];
  listingTableHeader = [
    'Title',
    'Status',
    'Operations'
  ];


  /*@ngInject*/
  constructor($state, $stateParams, toastr, DbRootResource, GfgModal, GfgHelpers) {
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.DbRootResource = DbRootResource;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;

    this.listDatabaseItems = this.DbRootResource.list(this.apiEndpoint);
    this.updateDatabaseItem = this.DbRootResource.update(this.apiEndpoint);
    this.deleteDatabaseItem = this.DbRootResource.delete(this.apiEndpoint);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  $onInit() {
    this.listDatabaseItems({}, (data, error) => {
      if(error) {
        this.toastr.error('Error loading data. Try again later.');
        return;
      }

      this.list = data;
    });
  }

  refreshPage() {
    this.$state.transitionTo(this.$state.current, this.$stateParams, {
      reload: true,
      inherit: false,
      notify: true
    });
  }

  updateItem(item) {
    this.updateDatabaseItem(item._id, item, (data, error) => {
      if(error) {
        this.toastr.error('Error processing your request. Try again later.');
        return;
      }

      this.toastr.success(`Successfully updated ${item.title}.`);
      this.refreshPage();
    });
  }

  deleteItem(item) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.deleteDatabaseItem(item._id, (data, error) => {
          if(error) {
            this.toastr.error('Error processing your request. Try again later.');
            return;
          }

          this.toastr.success(`Successfully deleted ${item.title}.`);
          this.refreshPage();
        });
      }
    });
  }
}
