'use strict';

export default class adminTaxonomyOrderCancelReasonAddController {

  apiEndpoint = '/api/taxonomy.orderCancelReasons';
  pageTitle = 'Add Order Cancellation Reason';
  item = { title: '', status: true };

  /*@ngInject*/
  constructor($state, toastr, DbRootResource, GfgHelpers) {
    this.$state = $state;
    this.toastr = toastr;
    this.DbRootResource = DbRootResource;
    this.GfgHelpers = GfgHelpers;
    this.addDatabaseItem = this.DbRootResource.create(this.apiEndpoint);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  save(addAnotherItem = false) {
    this.addDatabaseItem(this.item, (data, error) => {
      if(error) {
        this.toastr.error('Could not add item. Try again later.');
        return;
      }

      this.toastr.success(`Successfully added order cancellation reason: ${this.item.title}`);
      this.item = { title: '', status: false };
    });

    if(!addAnotherItem) {
      this.$state.go('adminTaxonomyOrderCancelReasonList');
    }
  }

  cancel() {
    this.$state.go('adminTaxonomyOrderCancelReasonList');
  }
}
