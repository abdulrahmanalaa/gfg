'use strict';

export default class adminTaxonomyCustomerTypeAddController {

  apiEndpoint = '/api/taxonomy.customerTypes';
  pageTitle = 'Add Customer Type';
  item = { title: '', status: true };

  /*@ngInject*/
  constructor($state, toastr, GfgHelpers, DbRootResource) {
    this.$state = $state;
    this.toastr = toastr;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.addDatabaseItem = this.DbRootResource.create(this.apiEndpoint);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  save(addAnotherItem = false) {
    this.addDatabaseItem(this.item, (data, error) => {
      if(error) {
        this.toastr.error('Could not add item. Try again later.');
        return;
      }

      this.toastr.success(`Successfully added customer type: ${this.item.title}`);
      this.item = { title: '', status: false };
    });

    if(!addAnotherItem) {
      this.$state.go('adminTaxonomyCustomerTypeList');
    }
  }

  cancel() {
    this.$state.go('adminTaxonomyCustomerTypeList');
  }
}
