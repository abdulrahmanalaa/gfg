'use strict';

// taxonomy.addressSection
import adminTaxonomyAddressSectionListController from './taxonomy.addressSection/addressSection.list.controller';
import adminTaxonomyAddressSectionAddController from './taxonomy.addressSection/add/addressSection.add.controller';
// taxonomy/storageType
import adminTaxonomyStorageTypeListController from './taxonomy/storageType/storageType.controller';
import adminTaxonomyStorageTypeAddController from './taxonomy/storageType/storageType.add.controller';
// taxonomy/paymentMethod
import adminTaxonomyPaymentMethodListController from './taxonomy/paymentMethod/paymentMethod.controller';
import adminTaxonomyPaymentMethodAddController from './taxonomy/paymentMethod/paymentMethod.add.controller';
// taxonomy/supplierGroup
import adminTaxonomySupplierGroupListController from './taxonomy/supplierGroup/supplierGroup.controller';
import adminTaxonomySupplierGroupAddController from './taxonomy/supplierGroup/supplierGroup.add.controller';
// taxonomy/customerGroup
import adminTaxonomyCustomerGroupListController from './taxonomy/customerGroup/customerGroup.controller';
import adminTaxonomyCustomerGroupAddController from './taxonomy/customerGroup/customerGroup.add.controller';
// taxonomy/customerType
import adminTaxonomyCustomerTypeListController from './taxonomy/customerType/customerType.controller';
import adminTaxonomyCustomerTypeAddController from './taxonomy/customerType/customerType.add.controller';
// taxonomy/orderCancelReason
import adminTaxonomyOrderCancelReasonListController from './taxonomy/orderCancelReason/orderCancelReason.controller';
import adminTaxonomyOrderCancelReasonAddController from './taxonomy/orderCancelReason/orderCancelReason.add.controller';
// taxonomy/amendmentReason
import adminTaxonomyAmendmentReasonListController from './taxonomy/amendmentReason/amendmentReason.controller';
import adminTaxonomyAmendmentReasonAddController from './taxonomy/amendmentReason/amendmentReason.add.controller';
// taxonomy/designation
import adminTaxonomyDesignationListController from './taxonomy/designation/designation.controller';
import adminTaxonomyDesignationAddController from './taxonomy/designation/designation.add.controller';
// taxonomy/materialType
import adminTaxonomyMaterialTypeListController from './taxonomy/materialType/materialType.controller';
import adminTaxonomyMaterialTypeAddController from './taxonomy/materialType/materialType.add.controller';
// taxonomy/materialClass
import adminTaxonomyMaterialClassListController from './taxonomy/materialClass/materialClass.controller';
import adminTaxonomyMaterialClassAddController from './taxonomy/materialClass/materialClass.add.controller';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('admin', {
      url: '/admin',
      template: require('./admin.html'),
      controller: 'AdminController',
      controllerAs: 'admin',
      authenticate: 'admin'
    })
    // taxonomy.addressSection
    .state('adminTaxonomyAddressSectionList', {
      url: '/admin/taxonomy/addressSection?sectionType&parentId',
      template: require('./taxonomy.addressSection/addressSection.html'),
      controller: adminTaxonomyAddressSectionListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyAddressSectionAdd', {
      url: '/admin/taxonomy/addressSection/add?sectionType&parentId',
      template: require('./taxonomy.addressSection/add/addressSection.add.html'),
      controller: adminTaxonomyAddressSectionAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.storageType
    .state('adminTaxonomyStorageTypeList', {
      url: '/admin/taxonomy/storageType',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyStorageTypeListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyStorageTypeAdd', {
      url: '/admin/taxonomy/storageType/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyStorageTypeAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.paymentMethod
    .state('adminTaxonomyPaymentMethodList', {
      url: '/admin/taxonomy/paymentMethod',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyPaymentMethodListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyPaymentMethodAdd', {
      url: '/admin/taxonomy/paymentMethod/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyPaymentMethodAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.supplierGroup
    .state('adminTaxonomySupplierGroupList', {
      url: '/admin/taxonomy/supplierGroup',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomySupplierGroupListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomySupplierGroupAdd', {
      url: '/admin/taxonomy/supplierGroup/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomySupplierGroupAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.customerGroup
    .state('adminTaxonomyCustomerGroupList', {
      url: '/admin/taxonomy/customerGroup',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyCustomerGroupListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyCustomerGroupAdd', {
      url: '/admin/taxonomy/customerGroup/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyCustomerGroupAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.customerType
    .state('adminTaxonomyCustomerTypeList', {
      url: '/admin/taxonomy/customerType',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyCustomerTypeListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyCustomerTypeAdd', {
      url: '/admin/taxonomy/customerType/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyCustomerTypeAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.orderCancelReason
    .state('adminTaxonomyOrderCancelReasonList', {
      url: '/admin/taxonomy/orderCancelReason',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyOrderCancelReasonListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyOrderCancelReasonAdd', {
      url: '/admin/taxonomy/orderCancelReason/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyOrderCancelReasonAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.amendmentReason
    .state('adminTaxonomyAmendmentReasonList', {
      url: '/admin/taxonomy/amendmentReason',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyAmendmentReasonListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyAmendmentReasonAdd', {
      url: '/admin/taxonomy/amendmentReason/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyAmendmentReasonAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.designation
    .state('adminTaxonomyDesignationList', {
      url: '/admin/taxonomy/designation',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyDesignationListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyDesignationAdd', {
      url: '/admin/taxonomy/designation/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyDesignationAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.materialType
    .state('adminTaxonomyMaterialTypeList', {
      url: '/admin/taxonomy/materialType',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyMaterialTypeListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyMaterialTypeAdd', {
      url: '/admin/taxonomy/materialType/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyMaterialTypeAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    // taxonomy.materialClass
    .state('adminTaxonomyMaterialClassList', {
      url: '/admin/taxonomy/materialClass',
      template: require('./taxonomy/taxonomy.list.html'),
      controller: adminTaxonomyMaterialClassListController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminTaxonomyMaterialClassAdd', {
      url: '/admin/taxonomy/materialClass/add',
      template: require('./taxonomy/taxonomy.add.html'),
      controller: adminTaxonomyMaterialClassAddController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
  ;
}
