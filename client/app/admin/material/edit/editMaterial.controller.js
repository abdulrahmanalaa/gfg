'use strict';

export default class editMaterialController {

  pageTitle = 'Edit Material';
  formSubmitted = false;
  material = {};
  materialTypes = [];
  materialClasses = [];
  taxonomyQueryParams = {
    fields: '_id title',
    sort: 'title',
    direction: 'asc',
    queryConditions: {
      status: true
    }
  };

  /*@ngInject*/
  constructor($log, $state, $stateParams, toastr, apiBaseUrl, AWS_S3_REMOTE_BASE_URL, GfgHelpers, DbRootResource) {
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.AWS_S3_REMOTE_BASE_URL = AWS_S3_REMOTE_BASE_URL;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;

    this.getMaterialResource = this.DbRootResource.get(`${this.apiBaseUrl}/materials`);
    this.updateMaterialResource = this.DbRootResource.update(`${this.apiBaseUrl}/materials`);
    this.getMaterialTypes = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.materialTypes`);
    this.getMaterialClasses = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.materialClasses`);
    this.UploadFileResource = this.DbRootResource.create(`${this.apiBaseUrl}/static/file/upload`);
    this.DeleteFileResource = this.DbRootResource.create(`${this.apiBaseUrl}/static/file/delete`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    // get material resource to be edited.
    this.getMaterialResource(this.$stateParams.materialId, (data, error) => {
      if(error) {
        this.toastr.error('Could not retrieve data.');
        this.$state.go('adminMaterialList');
        return;
      }
      this.material = data;
    });
    // get materialTypes
    // this.getMaterialTypes(this.taxonomyQueryParams, (data, error) => {
    //   if(error) {
    //     this.toastr.error('Error requesting material types. try again later.');
    //     this.$state.go('adminMaterialList');
    //     return;
    //   }
    //   this.materialTypes = data;
    // });
    // get materialClasses
    this.getMaterialClasses(this.taxonomyQueryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error requesting material classes. try again later.');
        this.$state.go('adminMaterialList');
        return;
      }
      this.materialClasses = data;
    });
  }


  save() {
    this.formSubmitted = true;

    if(this.form.$valid) {
      this.updateMaterialResource(this.$stateParams.materialId, this.material, (data, error) => {
        if(error) {
          this.toastr.error('Could not update material. Try again later.');
          this.$state.go('adminMaterialList');
          return;
        }
        this.toastr.success(`Successfully update material ${this.material.title}`);
        this.$state.go('adminMaterialList');
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  uploadFile() {
    this.serverCallInProgress = true;
    this.fileUploadStatusMessage = 'Uploading file...';

    this.UploadFileResource(this.uploadedFileObject, (data, error) => {
      this.serverCallInProgress = false;
      this.fileUploadStatusMessage = null;

      if(error) {
        this.toastr.error('Error uploading file.');
        this.$log.error(error);
        return;
      }

      this.material.imageUrl = data.uploadedFileName;
      this.hideFileUploadButton = true;
      this.$log.info(data);
    });
  }


  deleteFile(fileName) {
    this.uploadedFileObject = {};
    this.material.imageUrl = undefined;
    this.hideFileUploadButton = false;

    if(fileName) {
      this.serverCallInProgress = true;
      this.fileUploadStatusMessage = 'Deleting file...';

      // this.DeleteFileResource({ filename: fileName }, (data, error) => {
      //   this.serverCallInProgress = false;
      //   this.fileUploadStatusMessage = null;
      //
      //   if(error) {
      //     this.$log.error(error, 'error deleting file from aws.');
      //     return;
      //   }
      //
      //   this.$log.info(data, 'successfully deleted file from server.');
      // });
    }
  }
}
