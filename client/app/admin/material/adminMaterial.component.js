'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');

import routes from './adminMaterial.routes';

export class AdminMaterialComponent {

  pageTitle = 'Materials';
  materials = [];
  queryParams = {
    sort: 'title',
    direction: 'asc'
  };

  /*@ngInject*/
  constructor($state, $stateParams, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource) {
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.getMaterialsList = this.DbRootResource.list(`${this.apiBaseUrl}/materials`);
    this.deleteMaterialResource = this.DbRootResource.delete(`${this.apiBaseUrl}/materials`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getMaterialsList(this.queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving data. Try again later.');
        return;
      }
      this.materials = data;
    });
  }


  refreshPage() {
    this.$state.transitionTo(this.$state.current, this.$stateParams, {
      reload: true,
      inherit: false,
      notify: true
    });
  }


  deleteMaterial(materialId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.deleteMaterialResource(materialId, (data, err) => {
          if(err) {
            this.toastr.error('Error deleting material. Try again Later.');
            return;
          }

          this.toastr.success('Successfully deleted material.');
          this.refreshPage();
        });
      }
    });
  }
}

export default angular.module('gfgApp.adminMaterial', [uiRouter])
  .config(routes)
  .component('adminMaterialList', {
    template: require('./adminMaterial.html'),
    controller: AdminMaterialComponent,
    controllerAs: 'vm'
  })
  .name;
