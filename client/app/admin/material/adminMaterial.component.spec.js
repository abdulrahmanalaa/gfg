'use strict';

describe('Component: AdminMaterialComponent', function() {
  // load the controller's module
  beforeEach(module('gfgApp.adminMaterial'));

  var AdminMaterialComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminMaterialComponent = $componentController('adminMaterial', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
