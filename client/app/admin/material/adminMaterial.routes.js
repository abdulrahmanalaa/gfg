'use strict';

import addMaterialController from './add/addMaterial.controller';
import editSupplierController from './edit/editMaterial.controller';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminMaterialList', {
      url: '/admin/material',
      template: '<admin-material-list></admin-material-list>',
      authenticate: 'admin'
    })
    .state('adminMaterialAdd', {
      url: '/admin/material/add',
      template: require('./add/addMaterial.html'),
      controller: addMaterialController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminMaterialEdit', {
      url: '/admin/material/{materialId}',
      template: require('./add/addMaterial.html'),
      controller: editSupplierController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
  ;
}
