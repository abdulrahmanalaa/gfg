'use strict';

const _ = require('lodash');
const angular = require('angular');

export default class editPurchaseOrderController {

  purchaseOrder = {};
  pageTitle = 'Edit Purchase Order';
  formSubmitted = false;
  //dbLeaders = [];
  dbMaterials = [];
  dbStorages = [];
  dbStoragesGrouped = [];
  dbSuppliersNames = [];
  dbSelectedSupplier = {};
  supplierAllowedMaterials = {};
  cylinderMaterials = {};
  preSelectedLeader = null;

  purchaseOrderStatuses = {
    OPEN: 'Open',
    ASSIGNED: 'Assigned',
    STARTED: 'Started',
    COMPLETED: 'Completed',
    CANCELLED: 'Cancelled'
  };

  userQueryParams = {
    fields: 'name email status',
    sort: 'name',
    direction: 'asc',
    queryConditions: {
      status: true
    }
  };

  supplierQueryParams = {
    fields: 'name accountingId email address pricingInfo',
    sort: 'name',
    direction: 'asc',
    queryConditions: {
      status: true
    }
  };

  /*@ngInject*/
  constructor($scope, $log, $state, $stateParams, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource, DbUserResource, DbTaxonomyAddressSectionResource) {
    this.$scope = $scope;
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;

    this.DbPurchaseOrderUpdate = this.DbRootResource.update(`${this.apiBaseUrl}/purchaseOrders`);
    this.DbPurchaseOrderGet = this.DbRootResource.get(`${this.apiBaseUrl}/purchaseOrders`);
    this.DbSuppliersList = this.DbRootResource.list(`${this.apiBaseUrl}/suppliers`);
    this.DbSuppliersGet = this.DbRootResource.get(`${this.apiBaseUrl}/suppliers`);
    this.DbStorageList = this.DbRootResource.list(`${this.apiBaseUrl}/storages`);
    this.DbMaterialsList = this.DbRootResource.list(`${this.apiBaseUrl}/materials`);

    this.$scope.$watch(() => this.purchaseOrder.materialInfo, (newValue, oldValue) => {
      this.purchaseOrderMaterialInfoChanged(newValue, oldValue);
    }, true);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }

  $onInit() {
    this.getPurchaseOrderDetails();
    //this.getPreSelectedLeader();
    this.getDbMaterials();
    this.getDbStorages();
    this.getDbSuppliersNamesList();
  }


  save() {
    this.formSubmitted = true;

    if(!this.purchaseOrder.orderCostTotal || parseInt(this.purchaseOrder.orderCostTotal) < .001) {
      this.toastr.warning('Order total value must be bigger than 0');
      return;
    }

    if(!this.validateOrderStatusOnEdit()) {
      return;
    }

    if(this.formIsValid()) {
      // if no leaderIdRef, set orderStatus to OPEN.
      if(!this.purchaseOrder.leaderIdRef) {
        this.purchaseOrder.orderStatus = 'OPEN';
      }

      // if orderStatus is OPEN, set no leaderIdRef
      if(this.purchaseOrder.orderStatus === 'OPEN') {
        this.purchaseOrder.leaderIdRef = null;
      }

      this.DbPurchaseOrderUpdate(this.$stateParams.purchaseOrderId, this.purchaseOrder, (data, error) => {
        if(error) {
          this.toastr.error('Error updating order. <br>Try again later.');
          this.$log.error(error);
          return;
        }
        this.toastr.success('Successfully created purchase order.');
        this.$state.go('adminSalesOrderDashboardDrag');
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  getPurchaseOrderDetails() {
    this.DbPurchaseOrderGet(this.$stateParams.purchaseOrderId, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving order. <br>Try again later.');
        this.$log.error(error);
        this.$state.go('adminSalesOrderDashboardDrag');
        return;
      }
      this.purchaseOrder = data;
      console.log(data, 'data...');
      // get leader info.
      if(this.purchaseOrder.leaderIdRef) {
        this.getPreSelectedLeader(this.purchaseOrder.leaderIdRef);
      }
      // get customer info.
      if(this.purchaseOrder.supplierIdRef) {
        this.getPreSelectedSupplier(this.purchaseOrder.supplierIdRef._id);
      }
    });
  }


  formIsValid() {
    return (
      this.form.$valid
      && this.dbSelectedSupplier._id
      && this.dbSelectedSupplier.pricingInfo.length > 0
      && this.purchaseOrder.materialInfo.length > 0
    );
  }


  getPreSelectedLeader() {
    if(this.$stateParams.leaderUserId) {
      this.DbUserResource.get(this.$stateParams.leaderUserId, (data, error) => {
        if(error) {
          this.toastr.error('Invalid leader id.');
          this.$state.go('adminSalesOrderDashboardDrag');
          return;
        }
        this.preSelectedLeader = data;
        this.purchaseOrder.leaderIdRef = this.preSelectedLeader._id;
      });
    }
  }


  getPreSelectedSupplier(supplierId) {
    this.DbSuppliersGet(supplierId, (data, error) => {
      if(error) {
        console.log(supplierId, 'supplierId');
        this.toastr.error('Error getting supplier information.');
        this.$log.error(error);
        return;
      }
      this.dbSelectedSupplier = data;
      this.runSelectedSupplierOps();
    });
  }


  getDbMaterials() {
    this.DbMaterialsList({}, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving materials.');
        this.$state.go('adminPurchaseOrderList');
        return;
      }
      this.dbMaterials = data;
      angular.forEach(this.dbMaterials, (_item) => {
        _item.materialClass1 = _item.materialClass1._id;
        _item.materialClass2 = _item.materialClass2._id;
        // capture materials of type cylinder. this is used in calculating returned spares.
        if(_item.materialType.toLowerCase() === 'cylinder') {
          this.cylinderMaterials[_item._id] = _item;
        }
      });
    });
  }


  getDbStorages() {
    let params = {
      fields: 'title storageType plateNumber currentLeader currentDispatcher',
      queryConditions: {
        status: true,
        isAvailable: true
      }
    };
    this.DbStorageList(params, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving storage.');
        this.$state.go('adminSalesOrderList');
        return;
      }
      this.dbStorages = data;
      this.dbStoragesGrouped = _.groupBy(data, '_id');
    });
  }


  // getDbLeaders() {
  //   this.DbUserResource.list(this.userQueryParams, (data, error) => {
  //     if(error) {
  //       this.toastr.error('Error retrieving leaders.');
  //       this.$state.go('adminSalesOrderDashboardDrag');
  //       return;
  //     }
  //     this.dbLeaders = data;
  //   });
  // }


  getDbSuppliersNamesList() {
    let params = {
      fields: 'name',
      sort: 'name',
      direction: 'asc',
      queryConditions: {
        status: true
      }
    };

    this.DbSuppliersList(params, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving suppliers names.');
        return;
      }
      this.dbSuppliersNames = data;
    });
  }


  purchaseOrderMaterialInfoChanged(newValue, oldValue) {
    if(!newValue || !oldValue) {
      return;
    }

    this.purchaseOrder.orderCostTotal = 0;
    this.purchaseOrder.materialReturnedSpares = [];

    angular.forEach(newValue, (_item, _index) => {
      // calculate costTotal
      if(_item.quantity && _item.quantity > 0 && _item.cost) {
        //_item.costTotal = _item.quantity * _item.cost;
        _item.costTotal = (_item.quantity * _item.cost) + (_item.quantity * _item.additionalCostPerItem) + _item.additionalLumpSumCost;
        this.purchaseOrder.orderCostTotal += _item.costTotal;
      }

      if(_.isEmpty(this.supplierAllowedMaterials[_item.materialIdRef])) {
        return;
      }

      let materialObject = this.supplierAllowedMaterials[_item.materialIdRef].material;
      let sparesObject = {};

      if(
        materialObject.materialType.toLowerCase() === 'gas' // if materialType is gas.
        && _item.quantity // if there is quantity to compare.
      ) {
        // loop through cylinder materials, till they are added to purchaseOrder.materialInfo.
        // this will be overridden when cylinder material is added.
        angular.forEach(this.cylinderMaterials, (_itemCylinder) => {
          if(
            materialObject.materialClass1 === _itemCylinder.materialClass1
            && materialObject.materialClass2 === _itemCylinder.materialClass2
          ) {
            sparesObject = {
              materialIdRef: _itemCylinder._id,
              returnedSpares: _item.quantity
            };
          }

          //
          angular.forEach(newValue, (_item2, _index2) => {
            if(_item2.materialIdRef && this.supplierAllowedMaterials[_item2.materialIdRef]) {
              let materialObject2 = this.supplierAllowedMaterials[_item2.materialIdRef].material;

              if(
                materialObject2.materialType.toLowerCase() === 'cylinder' // if materialType is cylinder.
                && materialObject.materialClass1 === materialObject2.materialClass1 // if same materialClass1.
                && materialObject.materialClass2 === materialObject2.materialClass2 // if same materialClass2.
                && _item2.quantity // if there is quantity to compare.
              ) {
                let sparesDifference = _item.quantity - _item2.quantity;
                // sparesDifference = (sparesDifference > 0) ? sparesDifference : 0;
                sparesObject = {
                  materialIdRef: materialObject2._id,
                  returnedSpares: sparesDifference > 0 ? sparesDifference : 0
                };
              }
            }
          });
        });

        this.purchaseOrder.materialReturnedSpares.push(sparesObject);
      }

      sparesObject.returnedSparesActual = sparesObject.returnedSpares;

      // if(
      //   materialObject.materialType.toLowerCase() === 'cylinder' // if materialType is cylinder.
      //   && _item.quantity // if there is quantity to compare.
      // ) {
      //   let foundGasMatch = false;
      //
      //   angular.forEach(newValue, (_item2, _index2) => {
      //     if(_item2.materialIdRef && this.customerAllowedMaterials[_item2.materialIdRef]) {
      //       let materialObject2 = this.customerAllowedMaterials[_item2.materialIdRef].material;
      //
      //       if(
      //         materialObject2.materialType.toLowerCase() === 'gas' // if materialType is gas.
      //         && materialObject.materialClass1 === materialObject2.materialClass1 // if same materialClass1.
      //         && materialObject.materialClass2 === materialObject2.materialClass2 // if same materialClass2.
      //       ) {
      //         foundGasMatch = true;
      //       }
      //     }
      //   });
      //
      //   if(!foundGasMatch) {
      //     sparesObject = {
      //       materialIdRef: materialObject._id,
      //       returnedSpares: -1 * Math.abs(_item.quantity)
      //     };
      //
      //     this.salesOrder.materialReturnedSpares.push(sparesObject);
      //   }
      // }
    });
  }


  searchSuppliers(searchQuery) {
    this.supplierQueryParams.queryConditions = { status: true };
    this.supplierQueryParams.queryConditions = angular.extend({}, this.supplierQueryParams.queryConditions, searchQuery);
    this.DbSuppliersList(this.supplierQueryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error searching for suppliers. <br>Try again later.');
        this.dbSelectedSupplier = {};
        return;
      }
      if(_.isEmpty(data)) {
        this.toastr.warning('Supplier not found. <br>Try another search.');
        return;
      }
      console.log(data, 'data');

      // if found 1 result.
      if(data.length === 1) {
        this.dbSelectedSupplier = data[0];
        this.runSelectedSupplierOps();
        this.toastr.success(`Found only one match. <br>Successfully selected ${this.dbSelectedSupplier.name}.`);
      }
      // if more than 1 result, select from modal.
      else if(data.length > 1) {
        this.GfgModal.openCustomerSelectModal({}, data, (userSelection) => {
          if(userSelection) {
            this.dbSelectedSupplier = userSelection;
            this.runSelectedSupplierOps();
            this.toastr.success(`Successfully selected ${this.dbSelectedSupplier.name}.`);
          }
        });
      }
    });
  }


  runSelectedSupplierOps() {
    //this.purchaseOrder.materialInfo = [];
    this.purchaseOrder.supplierIdRef = this.dbSelectedSupplier._id;
    this.setSupplierAllowedMaterials();
    // this.setCustomerBlockedMaterials();
  }


  setSupplierAllowedMaterials() {
    // sort materials by validTo date descending.
    this.dbSelectedSupplier.pricingInfo = this.dbSelectedSupplier.pricingInfo.sort((x, y) => y.validTo > x.validTo);

    angular.forEach(this.dbSelectedSupplier.pricingInfo, (_item) => {
      let materialId = _item.material._id;
      // if material does not exists already in allowedMaterials list.
      if(!_.has(this.supplierAllowedMaterials, materialId)) {
        this.supplierAllowedMaterials[materialId] = _item;
      }
    });

    // show a warning if supplier has no allowedMaterials.
    if(_.isEmpty(this.supplierAllowedMaterials)) {
      let supplierEditUrl = this.$state.href('adminSupplierEdit', { supplierId: this.dbSelectedSupplier._id }, { absolute: true });
      this.toastr.warning(`
        ${this.dbSelectedSupplier.name} has no allowed materials.
        <br>
        <a href="${supplierEditUrl}" target="_blank">Click here to edit supplier details</a>.
      `);
    }
  }


  deleteDbSelectedSupplier() {
    //this.supplierIdRef = null;
    this.purchaseOrder.materialInfo = [];
    this.dbSelectedSupplier = {};
    this.supplierAllowedMaterials = {};
    // this.customerBlockedMaterialIds = [];
  }


  addMaterialInfo() {
    this.purchaseOrder.materialInfo.push({
      additionalCostPerItem: 0,
      additionalLumpSumCost: 0
    });
  }


  deleteMaterialInfo(idx) {
    this.purchaseOrder.materialInfo.splice(idx, 1);
  }


  supplierMaterialSelected(selectedMaterialId, itemIndex) {
    let isValidMaterial = true;
    let materialObject = this.supplierAllowedMaterials[selectedMaterialId].material;

    angular.forEach(this.purchaseOrder.materialInfo, (_item, _idx) => {
      // check if this material is already selected.
      if((_item.materialIdRef === selectedMaterialId) && (itemIndex !== _idx)) {
        this.toastr.warning(`The material "<b>${materialObject.title}</b>" is already selected.<br>Kindly select another material.`);
        isValidMaterial = false;
      }
    });

    if(!isValidMaterial) {
      this.purchaseOrder.materialInfo[itemIndex] = {};
      return;
    }

    this.purchaseOrder.materialInfo[itemIndex].cost = this.supplierAllowedMaterials[selectedMaterialId].price;
    this.purchaseOrder.materialInfo[itemIndex].quantity = 1;
    this.purchaseOrder.materialInfo[itemIndex].returnedSpares = undefined;
  }


  supplierNameSelectedTypeAhead(item, model, label, evt) {
    if(item._id) {
      this.searchSuppliers({ _id: item._id });
    }
  }


  orderStatusChanged() {
    if(this.salesOrder.orderStatus === 'OPEN') {
      this.salesOrder.storageIdRef = null;
    }
  }


  orderStorageChanged() {
    if(this.purchaseOrder.storageIdRef && this.purchaseOrder.orderStatus === 'OPEN') {
      this.purchaseOrder.orderStatus = 'ASSIGNED';
    }

    if(!this.purchaseOrder.storageIdRef && ['ASSIGNED', 'STARTED', 'COMPLETED'].indexOf(this.purchaseOrder.orderStatus) > -1) {
      this.purchaseOrder.orderStatus = 'OPEN';
    }
  }


  validateOrderStatusOnEdit() {
    let isValid = true;
    let orderStatus = this.purchaseOrder.orderStatus;

    if(['ASSIGNED', 'STARTED', 'COMPLETED'].indexOf(orderStatus) > -1 && !this.purchaseOrder.storageIdRef) {
      isValid = false;
      this.toastr.warning('Order can not be created without a leader. <br>Kindly select a leader.');
    }

    return isValid;
  }
}
