'use strict';

const _ = require('lodash');
const angular = require('angular');

export default class listPurchaseOrderController {

  pageTitle = 'Purchase Orders';
  purchaseOrdersList = [];
  queryParams = {
    fields: 'orderNumber orderStatus orderCostTotal created changed storageIdRef supplierIdRef orderCancellationReason',
    sort: 'created',
    direction: 'DESC'
  };

  /*@ngInject*/
  constructor($log, $state, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource) {
    this.$log = $log;
    this.$state = $state;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;

    this.dbPurchaseOrdersList = this.DbRootResource.list(`${this.apiBaseUrl}/purchaseOrders`);
    this.dbPurchaseOrdersDelete = this.DbRootResource.delete(`${this.apiBaseUrl}/purchaseOrders`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.dbPurchaseOrdersList(this.queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving purchase orders. <br>Try again later.');
        this.$log.error(error);
        return;
      }
      this.purchaseOrdersList = data;
      this.$log.info(data);
    });
  }


  deletePurchaseOrder(orderId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.dbPurchaseOrdersDelete(orderId, (data, error) => {
          if(error) {
            this.toastr.error('Error deleting purchase order. <br>Try again later.');
            this.$log.error(error);
            return;
          }
          this.toastr.success('Successfully deleted purchase order.');
          this.GfgHelpers.refreshPage();
        });
      }
    });
  }
}
