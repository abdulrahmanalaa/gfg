'use strict';

const angular = require('angular');
import routes from './admin.routes';
import AdminController from './admin.controller';

import adminUser from './user/adminUser.component';
import adminSupplier from './supplier/supplier.component';
import adminMaterial from './material/adminMaterial.component';
import adminCustomer from './customer/adminCustomer.component';
import adminStorage from './storage/adminStorage.component';
import adminSalesOrder from './salesOrder/salesOrder.component';

export default angular.module('gfgApp.admin', [
  'gfgApp.auth',
  'ui.router',
  adminUser,
  adminSupplier,
  adminMaterial,
  adminCustomer,
  adminStorage,
  adminSalesOrder
])
  .config(routes)
  .controller('AdminController', AdminController)
  .name;
