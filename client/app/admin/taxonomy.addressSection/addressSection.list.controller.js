'use strict';

export default class adminTaxonomyAddressSectionListController {

  pageTitle = 'Goverorates';
  list = [];
  qsSectionType = null;
  qsParentId = null;
  defaultSectionType = 'governorate';

  /*@ngInject*/
  constructor($window, $state, $stateParams, toastr, GfgModal, GfgHelpers, DbTaxonomyAddressSectionResource) {
    this.$window = $window;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    // set values from query string.
    this.qsSectionType = this.$stateParams.sectionType || this.defaultSectionType;
    this.qsParentId = this.$stateParams.parentId || null;

    this.pageTitle = `${this.GfgHelpers.capitalizeFirstLetterOfString(this.qsSectionType)}s`;
    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    if(this.qsSectionType === this.defaultSectionType) {
      this.DbTaxonomyAddressSectionResource.listBySectionType(this.qsSectionType, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving data. Try again later.');
          return;
        }
        this.list = data;
      });
    }
    else {
      this.DbTaxonomyAddressSectionResource.listByParentId(this.qsParentId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving data. Try again later.');
          return;
        }
        this.list = data;
      });
    }
  }


  historyBack() {
    this.$window.history.back();
  }


  refreshPage() {
    this.$state.transitionTo(this.$state.current, this.$stateParams, {
      reload: true,
      inherit: false,
      notify: true
    });
  }


  updateItem(item) {
    this.DbTaxonomyAddressSectionResource.update(item._id, item, (data, error) => {
      if(error) {
        this.toastr.error('Error processing your request. Try again later.');
        return;
      }

      this.toastr.success(`Successfully updated ${this.qsSectionType}: ${item.title}.`);
      this.refreshPage();
    });
  }


  deleteItem(itemId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.DbTaxonomyAddressSectionResource.delete(itemId, (data, error) => {
          if(error) {
            this.toastr.error(`Could not delete this ${this.qsSectionType}. Try again later.`);
            return;
          }
          this.toastr.success(`Successfully deleted ${this.qsSectionType}`);
          this.refreshPage();
        });
      }
    });
  }
}
