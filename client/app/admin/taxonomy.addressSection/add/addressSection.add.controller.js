'use strict';

const _ = require('lodash');

export default class adminTaxonomyAddressSectionAddController {

  pageTitle = '';
  item = { title: '', sectionType: '', parentId: null };
  qsSectionType = null;
  qsParentId = null;
  defaultSectionType = 'governorate';
  allowedSectionTypes = ['governorate', 'area', 'block'];
  allowedParents = [];

  /*@ngInject*/
  constructor($window, $log, $state, $stateParams, toastr, GfgHelpers, DbTaxonomyAddressSectionResource) {
    this.$window = $window;
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.GfgHelpers = GfgHelpers;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    // set values from query string.
    this.qsSectionType = this.$stateParams.sectionType || this.defaultSectionType;
    this.qsParentId = this.$stateParams.parentId || null;

    this.pageTitle = `Add new ${this.GfgHelpers.capitalizeFirstLetterOfString(this.qsSectionType)}`;
    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.setSectionType();
    this.item.parentId = this.qsParentId;
  }


  setSectionType() {
    this.qsSectionType = this.qsSectionType.trim().toLowerCase();
    if(this.allowedSectionTypes.indexOf(this.qsSectionType) === -1) {
      this.qsSectionType = this.defaultSectionType;
    }
    this.item.sectionType = this.qsSectionType;
  }


  historyBack() {
    this.$window.history.back();
  }


  redirectAfterSave() {
    let stateParams = {};

    if(_.isEmpty(this.qsParentId) || _.isEmpty(this.qsSectionType)) {
      stateParams = {};
    }
    else {
      stateParams = {
        parentId: this.qsParentId,
        sectionType: this.qsSectionType
      };
    }

    this.$state.go('adminTaxonomyAddressSectionList', stateParams);
  }


  save(addAnotherItem = false) {
    this.DbTaxonomyAddressSectionResource.create(this.item, (data, error) => {
      if(error) {
        this.toastr.error('Could not process your request. Try again later.');
        this.$log.error(error);
        return;
      }

      this.toastr.success(`Successfully created ${this.qsSectionType}: ${this.item.title}`);
      if(!addAnotherItem) {
        this.redirectAfterSave();
      }
      else {
        this.item.title = '';
      }
    });
  }
}
