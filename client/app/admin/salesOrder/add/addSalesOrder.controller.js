'use strict';

const _ = require('lodash');
const angular = require('angular');

export default class addSalesOrderController {

  salesOrder = {
    status: true,
    orderStatus: 'OPEN',
    orderCostTotal: 0,
    storageIdRef: null,
    customerIdRef: null,
    materialInfo: [],
    materialReturnedSpares: []
  };
  pageTitle = 'Add Sales Order';
  formSubmitted = false;
  dbLeaders = [];
  dbMaterials = [];
  dbStorages = [];
  dbStoragesGrouped = [];
  dbCustomers = [];
  dbCustomerNames = [];
  dbSelectedCustomer = {};
  addressGovernorates = [];
  addressAreas = [];
  addressBlocks = [];
  customerAllowedMaterials = {};
  customerBlockedMaterialIds = [];
  cylinderMaterials = {};
  preSelectedStorage = null;

  salesOrderStatuses = {
    OPEN: 'Open',
    ASSIGNED: 'Assigned',
    STARTED: 'Started',
    COMPLETED: 'Completed',
    CANCELLED: 'Cancelled'
  };

  mapOptions = {
    scrollwheel: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    draggable: false,
    mapTypeId: 'roadmap'
  };

  userQueryParams = {
    fields: 'name email status',
    sort: 'name',
    direction: 'asc',
    queryConditions: {
      status: true
    }
  };

  customerQueryParams = {
    fields: 'accountingId name ownerName customerId email phoneNumber1 phoneNumber2 address pricingInfo customerType blockedMaterials',
    sort: 'name',
    direction: 'asc',
    queryConditions: {
      status: true
    }
  };

  /*@ngInject*/
  constructor($scope, $log, $state, $stateParams, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource, DbUserResource, DbTaxonomyAddressSectionResource) {
    this.$scope = $scope;
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;

    this.DbSalesOrderCreate = this.DbRootResource.create(`${this.apiBaseUrl}/salesOrders`);
    this.DbCustomersList = this.DbRootResource.list(`${this.apiBaseUrl}/customers`);
    this.DbCustomersGet = this.DbRootResource.get(`${this.apiBaseUrl}/customers`);
    this.DbStorageList = this.DbRootResource.list(`${this.apiBaseUrl}/storages`);
    this.DbStorageGet = this.DbRootResource.get(`${this.apiBaseUrl}/storages`);
    this.DbMaterialsList = this.DbRootResource.list(`${this.apiBaseUrl}/materials`);

    this.$scope.$watch(() => this.salesOrder.materialInfo, (newValue, oldValue) => {
      this.salesOrderMaterialInfoChanged(newValue, oldValue);
    }, true);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getPreSelectedStorage();
    this.getDbMaterials();
    this.getDbStorages();
    this.getDbCustomerNamesList();
    // this.getDbLeaders();
    //this.getDbCustomers();
    //this.getAddressGovernorates();
  }


  save() {
    this.formSubmitted = true;

    if(!this.salesOrder.orderCostTotal || parseInt(this.salesOrder.orderCostTotal) < .001) {
      this.toastr.warning('Order total value must be bigger than 0');
      return;
    }

    if(!this.validateOrderStatusOnAdd()) {
      return;
    }

    if(this.formIsValid()) {
      this.DbSalesOrderCreate(this.salesOrder, (data, error) => {
        if(error) {
          this.toastr.error('Error creating sales order. <br>Try again later.');
          this.$log.error(error);
          return;
        }
        this.toastr.success(`Successfully created sales order.<br>Order Number: ${data.orderNumber}`);
        this.$state.go('adminSalesOrderDashboardDrag');
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  formIsValid() {
    return (
      this.form.$valid
      && this.dbSelectedCustomer._id
      && this.salesOrder.materialInfo.length > 0
    );
  }


  getPreSelectedStorage() {
    if(this.$stateParams.storageId) {
      this.salesOrder.orderStatus = 'ASSIGNED';
      this.DbStorageGet(this.$stateParams.storageId, (data, error) => {
        if(error) {
          this.toastr.error('Invalid storage id.');
          this.$state.go('adminSalesOrderDashboardDrag');
          return;
        }
        this.preSelectedStorage = data;
        this.salesOrder.storageIdRef = this.preSelectedStorage._id;
      });
    }
  }


  // getCustomerDetails(customerId) {
  //   this.DbCustomersGet(customerId, (data, error) => {
  //     if(error) {
  //       this.toastr.error('Error getting customer information.');
  //       return;
  //     }
  //     let customer = data; // @TODO; put the data inside usable variable.
  //     // @TODO; bind dependant area and block from selected customer governorate.
  //   });
  // }


  getDbMaterials() {
    this.DbMaterialsList({}, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving materials.');
        this.$state.go('adminSalesOrderList');
        return;
      }
      this.dbMaterials = data;
      angular.forEach(this.dbMaterials, (_item) => {
        _item.materialClass1 = _item.materialClass1._id;
        _item.materialClass2 = _item.materialClass2._id;
        // capture materials of type cylinder. this is used in calculating returned spares.
        if(_item.materialType.toLowerCase() === 'cylinder') {
          this.cylinderMaterials[_item._id] = _item;
        }
      });
    });
  }


  getDbStorages() {
    let params = {
      fields: 'title storageType plateNumber currentLeader currentDispatcher',
      queryConditions: {
        status: true,
        isAvailable: true
      }
    };
    this.DbStorageList(params, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving storage.');
        this.$state.go('adminSalesOrderList');
        return;
      }
      this.dbStorages = data;
      this.dbStoragesGrouped = _.groupBy(data, '_id');
    });
  }


  getDbCustomers() {
    this.DbCustomersList({}, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving customers.');
        return;
      }
      this.dbCustomers = data;
    });
  }


  getDbCustomerNamesList() {
    let params = {
      fields: 'name',
      sort: 'name',
      direction: 'asc',
      queryConditions: {
        status: true
      }
    };

    this.DbCustomersList(params, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving customer names.');
        return;
      }
      this.dbCustomerNames = data;
    });
  }


  salesOrderMaterialInfoChanged(newValue, oldValue) {
    if(!newValue || !oldValue) {
      return;
    }

    this.salesOrder.orderCostTotal = 0;
    this.salesOrder.materialReturnedSpares = [];

    angular.forEach(newValue, (_item, _index) => {
      // calculate costTotal
      if(_item.quantity && _item.quantity > 0 && _item.cost) {
        _item.costTotal = _item.quantity * _item.cost;
        this.salesOrder.orderCostTotal += _item.costTotal;
      }

      if(_.isEmpty(this.customerAllowedMaterials[_item.materialIdRef])) {
        return;
      }

      let materialObject = this.customerAllowedMaterials[_item.materialIdRef].material;
      let sparesObject = {};

      if(
        materialObject.materialType.toLowerCase() === 'gas' // if materialType is gas.
        && _item.quantity // if there is quantity to compare.
      ) {
        // loop through cylinder materials, till they are added to salesOrder.materialInfo.
        // this will be overridden when cylinder material is added.
        angular.forEach(this.cylinderMaterials, (_itemCylinder) => {
          if(
            materialObject.materialClass1 === _itemCylinder.materialClass1
            && materialObject.materialClass2 === _itemCylinder.materialClass2
          ) {
            sparesObject = {
              materialIdRef: _itemCylinder._id,
              returnedSpares: _item.quantity
            };
          }

          //
          angular.forEach(newValue, (_item2, _index2) => {
            if(_item2.materialIdRef && this.customerAllowedMaterials[_item2.materialIdRef]) {
              let materialObject2 = this.customerAllowedMaterials[_item2.materialIdRef].material;

              if(
                materialObject2.materialType.toLowerCase() === 'cylinder' // if materialType is cylinder.
                && materialObject.materialClass1 === materialObject2.materialClass1 // if same materialClass1.
                && materialObject.materialClass2 === materialObject2.materialClass2 // if same materialClass2.
                && _item2.quantity // if there is quantity to compare.
              ) {
                let sparesDifference = _item.quantity - _item2.quantity;
                // sparesDifference = (sparesDifference > 0) ? sparesDifference : 0;
                sparesObject = {
                  materialIdRef: materialObject2._id,
                  returnedSpares: sparesDifference > 0 ? sparesDifference : 0
                };
              }
            }
          });
        });

        this.salesOrder.materialReturnedSpares.push(sparesObject);
      }

      sparesObject.returnedSparesActual = sparesObject.returnedSpares;

      // if(
      //   materialObject.materialType.toLowerCase() === 'cylinder' // if materialType is cylinder.
      //   && _item.quantity // if there is quantity to compare.
      // ) {
      //   let foundGasMatch = false;
      //
      //   angular.forEach(newValue, (_item2, _index2) => {
      //     if(_item2.materialIdRef && this.customerAllowedMaterials[_item2.materialIdRef]) {
      //       let materialObject2 = this.customerAllowedMaterials[_item2.materialIdRef].material;
      //
      //       if(
      //         materialObject2.materialType.toLowerCase() === 'gas' // if materialType is gas.
      //         && materialObject.materialClass1 === materialObject2.materialClass1 // if same materialClass1.
      //         && materialObject.materialClass2 === materialObject2.materialClass2 // if same materialClass2.
      //       ) {
      //         foundGasMatch = true;
      //       }
      //     }
      //   });
      //
      //   if(!foundGasMatch) {
      //     sparesObject = {
      //       materialIdRef: materialObject._id,
      //       returnedSpares: -1 * Math.abs(_item.quantity)
      //     };
      //
      //     this.salesOrder.materialReturnedSpares.push(sparesObject);
      //   }
      // }
    });
  }


  deleteDbSelectedCustomer() {
    this.customerIdRef = null;
    this.salesOrder.materialInfo = [];
    this.dbSelectedCustomer = {};
    this.customerAllowedMaterials = {};
    this.customerBlockedMaterialIds = [];
  }


  searchCustomers(searchQuery) {
    this.deleteDbSelectedCustomer();

    this.customerQueryParams.queryConditions = { status: true };
    this.customerQueryParams.queryConditions = angular.extend({}, this.customerQueryParams.queryConditions, searchQuery);
    this.DbCustomersList(this.customerQueryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error searching for customers. <br>Try again later.');
        this.dbSelectedCustomer = {};
        return;
      }
      if(_.isEmpty(data)) {
        this.toastr.warning('Customer not found. <br>Try another search.');
        return;
      }

      // if found 1 result.
      if(data.length === 1) {
        this.dbSelectedCustomer = data[0];
        this.runSelectedCustomerOps();
        this.toastr.success(`Found only one match. <br>Successfully selected ${this.dbSelectedCustomer.name}.`);
      }
      // if more than 1 result, select from modal.
      else if(data.length > 1) {
        this.GfgModal.openCustomerSelectModal({}, data, (userSelection) => {
          if(userSelection) {
            this.dbSelectedCustomer = userSelection;
            this.runSelectedCustomerOps();
            this.toastr.success(`Successfully selected ${this.dbSelectedCustomer.name}.`);
          }
        });
      }
    });
  }


  runSelectedCustomerOps() {
    this.salesOrder.materialInfo = [];
    this.salesOrder.customerIdRef = this.dbSelectedCustomer._id;
    this.setCustomerTypeCashOrNot();
    this.setCustomerBlockedMaterials();
    this.setCustomerAllowedMaterials();
  }


  // Set whether customerType is cash.
  setCustomerTypeCashOrNot() {
    this.dbSelectedCustomer.isCashCustomer = false;
    if(this.dbSelectedCustomer.customerType && this.dbSelectedCustomer.customerType.title) {
      if(this.dbSelectedCustomer.customerType.title.toLowerCase().trim() === 'cash') {
        this.dbSelectedCustomer.isCashCustomer = true;
      }
    }

    //console.log(this.dbSelectedCustomer, 'this.dbSelectedCustomer');
  }


  setCustomerAllowedMaterials() {
    // sort materials by validTo date descending.
    this.dbSelectedCustomer.pricingInfo = this.dbSelectedCustomer.pricingInfo.sort((x, y) => y.validTo > x.validTo);

    // if cash customer.
    if(this.dbSelectedCustomer.isCashCustomer) {
      angular.forEach(this.dbMaterials, (_itm) => {
        this.customerAllowedMaterials[_itm._id] = {
          price: 0,
          material: {
            _id: _itm._id,
            title: _itm.title,
            materialId: _itm.materialId,
            accountingId: _itm.accountingId,
            materialType: _itm.materialType,
            materialClass1: _itm.materialClass1,
            materialClass2: _itm.materialClass2,
            uid: _itm.uid,
            created: _itm.created,
            changed: _itm.changed,
          }
        };
      });
    }
    // if not cash customer.
    else {
      angular.forEach(this.dbSelectedCustomer.pricingInfo, (_item) => {
        let materialId = _item.material._id;
        // if material does not exists already in allowedMaterials list AND is not one of blocked materials.
        if(!_.has(this.customerAllowedMaterials, materialId) && this.customerBlockedMaterialIds.indexOf(materialId) === -1) {
          this.customerAllowedMaterials[materialId] = _item;
        }
      });

      // not a cash customer? show a warning if customer has no allowedMaterials.
      if(_.isEmpty(this.customerAllowedMaterials)) {
        let customerEditUrl = this.$state.href('adminCustomerEdit', { customerId: this.dbSelectedCustomer._id }, { absolute: true });
        this.toastr.warning(
          `${this.dbSelectedCustomer.name} has no allowed materials.
          <br>
          <a href="${customerEditUrl}" target="_blank">Click here to edit customer details</a>.`
        );
      }
    }
  }


  // sets the array of customerBlockedMaterialIds.
  setCustomerBlockedMaterials() {
    this.customerBlockedMaterialIds = [];
    if(this.dbSelectedCustomer.blockedMaterials && !_.isEmpty(this.dbSelectedCustomer.blockedMaterials)) {
      this.customerBlockedMaterialIds = _.map(this.dbSelectedCustomer.blockedMaterials, _.property('material'));
    }
  }


  addMaterialInfo() {
    this.salesOrder.materialInfo.push({});
  }


  deleteMaterialInfo(idx) {
    this.salesOrder.materialInfo.splice(idx, 1);
  }


  customerMaterialSelected(selectedMaterialId, itemIndex) {
    let isValidMaterial = true;
    let materialObject = this.customerAllowedMaterials[selectedMaterialId].material;

    angular.forEach(this.salesOrder.materialInfo, (_item, _idx) => {
      // check if this material is already selected.
      if((_item.materialIdRef === selectedMaterialId) && (itemIndex !== _idx)) {
        this.toastr.warning(`The material "<b>${materialObject.title}</b>" is already selected.<br>Kindly select another material.`);
        isValidMaterial = false;
      }
    });

    if(!isValidMaterial) {
      this.salesOrder.materialInfo[itemIndex] = {};
      return;
    }

    this.salesOrder.materialInfo[itemIndex].cost = this.customerAllowedMaterials[selectedMaterialId].price;
    this.salesOrder.materialInfo[itemIndex].quantity = 1;
    this.salesOrder.materialInfo[itemIndex].returnedSpares = undefined;
    // this.salesOrder.materialInfo[itemIndex].materialType = this.customerAllowedMaterials[selectedMaterialId].material.materialType;
    // this.salesOrder.materialInfo[itemIndex].materialClass1 = this.customerAllowedMaterials[selectedMaterialId].material.materialClass1;
    // this.salesOrder.materialInfo[itemIndex].materialClass2 = this.customerAllowedMaterials[selectedMaterialId].material.materialClass2;
    // console.log(this.customerAllowedMaterials[selectedMaterialId], 'this.customerAllowedMaterials[selectedMaterialId]');
  }


  customerNameSelectedTypeAhead(item, model, label, evt) {
    if(item._id) {
      this.searchCustomers({ _id: item._id });
    }
  }


  orderStatusChanged() {
    if(this.salesOrder.orderStatus === 'OPEN') {
      this.salesOrder.storageIdRef = null;
    }
  }


  orderStorageChanged() {
    if(this.salesOrder.storageIdRef && this.salesOrder.orderStatus === 'OPEN') {
      this.salesOrder.orderStatus = 'ASSIGNED';
    }

    if(!this.salesOrder.storageIdRef && ['ASSIGNED', 'STARTED', 'COMPLETED'].indexOf(this.salesOrder.orderStatus) > -1) {
      this.salesOrder.orderStatus = 'OPEN';
    }
  }


  validateOrderStatusOnAdd() {
    let isValid = true;
    let orderStatus = this.salesOrder.orderStatus;

    if(orderStatus === 'CANCELLED') {
      isValid = false;
      this.toastr.warning('Order can not be created as Cancelled.');
    }

    if(orderStatus === 'OPEN' && this.salesOrder.storageIdRef) {
      isValid = false;
      this.toastr.warning('Order can not be created with status Open and a storage.');
    }

    if(['ASSIGNED', 'STARTED', 'COMPLETED'].indexOf(orderStatus) > -1 && !this.salesOrder.storageIdRef) {
      isValid = false;
      this.toastr.warning('Order can not be created without a leader. <br>Kindly select a leader.');
    }

    return isValid;
  }
}
