'use strict';

describe('Component: AdminSalesOrderComponent', function() {
  // load the controller's module
  beforeEach(module('gfgApp.adminSalesOrder'));

  var AdminSalesOrderComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminSalesOrderComponent = $componentController('adminSalesOrder', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
