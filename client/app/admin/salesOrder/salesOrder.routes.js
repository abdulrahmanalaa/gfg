'use strict';

import salesOrderDashboardDragController from './dashboard.drag/salesOrderDashboardDrag.controller';
import addSalesOrderController from './add/addSalesOrder.controller';
import editSalesOrderController from './edit/editSalesOrder.controller';
import listPurchaseOrderController from '../purchaseOrder/purchaseOrder.controller';
import addPurchaseOrderController from '../purchaseOrder/add/addPurchaseOrder.controller';
import editPurchaseOrderController from '../purchaseOrder/edit/editPurchaseOrder.controller';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminSalesOrderList', {
      url: '/admin/salesOrder',
      template: '<admin-sales-order-list></admin-sales-order-list>',
      authenticate: true
    })
    .state('adminSalesOrderDashboardDrag', {
      url: '/admin/salesOrder/dashboard',
      template: require('./dashboard.drag/dashboard.drag.html'),
      controller: salesOrderDashboardDragController,
      controllerAs: 'vm',
      authenticate: true
    })
    .state('adminSalesOrderAdd', {
      url: '/admin/salesOrder/add?storageId',
      template: require('./add/addSalesOrder.html'),
      controller: addSalesOrderController,
      controllerAs: 'vm',
      authenticate: true
    })
    .state('adminSalesOrderEdit', {
      url: '/admin/salesOrder/{salesOrderId}',
      template: require('./add/addSalesOrder.html'),
      controller: editSalesOrderController,
      controllerAs: 'vm',
      authenticate: true
    })
    .state('adminPurchaseOrderList', {
      url: '/admin/purchaseOrder',
      template: require('../purchaseOrder/purchaseOrder.html'),
      controller: listPurchaseOrderController,
      controllerAs: 'vm',
      authenticate: true
    })
    .state('adminPurchaseOrderAdd', {
      url: '/admin/purchaseOrder/add?storageId',
      template: require('../purchaseOrder/add/addPurchaseOrder.html'),
      controller: addPurchaseOrderController,
      controllerAs: 'vm',
      authenticate: true
    })
    .state('adminPurchaseOrderEdit', {
      url: '/admin/purchaseOrder/{purchaseOrderId}',
      template: require('../purchaseOrder/add/addPurchaseOrder.html'),
      controller: editPurchaseOrderController,
      controllerAs: 'vm',
      authenticate: true
    })
  ;
}
