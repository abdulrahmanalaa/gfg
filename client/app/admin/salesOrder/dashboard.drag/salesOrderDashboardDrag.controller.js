'use strict';

const _ = require('lodash');
const angular = require('angular');

export default class salesOrderDashboardDragController {

  pageTitle = 'Sales Orders Dispatching Board';
  dbOrders = [];
  dbOrdersFormatted = [];
  dbOrdersAssigned = {};
  dbOrdersUnassigned = [];
  serverCallIsRunning = false;
  usersOperationsButtonGroup = {};

  /*@ngInject*/
  constructor($scope, $log, $state, toastr, apiBaseUrl, GfgModal, DbRootResource, GfgHelpers) {
    this.$scope = $scope;
    this.$log = $log;
    this.$state = $state;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgModal = GfgModal;
    this.DbRootResource = DbRootResource;
    this.GfgHelpers = GfgHelpers;

    this.DbSalesOrderDashboardList = this.DbRootResource.list(`${this.apiBaseUrl}/salesOrders/dashboard`);
    this.DbSalesOrderUpdate = this.DbRootResource.update(`${this.apiBaseUrl}/salesOrders`);
    this.DbPurchaseOrderUpdate = this.DbRootResource.update(`${this.apiBaseUrl}/purchaseOrders`);
    this.DbUsersUpdate = this.DbRootResource.update(`${this.apiBaseUrl}/users`);
    this.DbStorageList = this.DbRootResource.list(`${this.apiBaseUrl}/storages`);
    this.DbStorageUpdate = this.DbRootResource.update(`${this.apiBaseUrl}/storages`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getDbOrders();
  }


  getDbOrders() {
    this.DbSalesOrderDashboardList({}, (data, error) => {
      if(error) {
        this.toastr.error('Error loading orders. <br>Try again later.');
        this.$log.error(error);
        return;
      }
      this.dbOrders = data;
      this.formatDbOrders();
      this.attachStorageWithNoOrdersToDashboard();
    });
  }


  formatDbOrders() {
    angular.forEach(this.dbOrders, (itm) => {
      // order has customerIdRef? it is sales order.
      if(itm.customerIdRef) {
        itm.collectionName = 'SO';
      }
      // order has supplierIdRef? it is purchase order.
      if(itm.supplierIdRef) {
        itm.collectionName = 'PO';
      }

      if(!itm.storageIdRef) {
        this.dbOrdersUnassigned.push(itm);
      }
      else {
        if(!this.dbOrdersAssigned[itm.storageIdRef._id]) {
          this.dbOrdersAssigned[itm.storageIdRef._id] = {
            storageDetails: itm.storageIdRef,
            orders: []
          };
        }
        this.dbOrdersAssigned[itm.storageIdRef._id].orders.push(itm);
        // this.dbOrdersAssigned[itm.leaderIdRef._id].push(itm);
      }
    });

    // add unassigned orders at the top.
    let newGroup = {
      label: 'Unassigned Orders',
      storageId: undefined,
      storageDetails: {},
      orders: this.dbOrdersUnassigned
    };
    this.dbOrdersFormatted.push(newGroup);

    angular.forEach(this.dbOrdersAssigned, (itm, key) => {
      newGroup = {
        label: itm.storageDetails.title,
        storageId: itm.storageDetails._id,
        storageDetails: itm.storageDetails,
        orders: itm.orders
      };
      this.dbOrdersFormatted.push(newGroup);
    });

    // console.log(this.dbOrdersAssigned, 'dbOrdersAssigned');
    // console.log(this.dbOrdersUnassigned, 'dbOrdersUnassigned');
    console.log(this.dbOrdersFormatted, 'dbOrdersFormatted');
  }


  attachStorageWithNoOrdersToDashboard() {
    let params = {
      fields: 'title plateNumber status isAvailable currentLeader currentDispatcher',
      queryConditions: {
        status: true
      }
    };

    this.DbStorageList(params, (data, error) => {
      if(error) {
        this.$log.error(error);
        return;
      }
      let storageListGrouped = _.groupBy(data, '_id');
      let storageIdsAll = _.map(data, '_id');
      let storageIdsWithOrders = _.map(this.dbOrdersFormatted, 'storageId');
      let storageIdsWithoutOrders = _.difference(storageIdsAll, storageIdsWithOrders);

      _.forEach(storageIdsWithoutOrders, (_sid) => {
        let _storage = storageListGrouped[_sid][0];
        let newGroup = {
          label: _storage.title,
          storageId: _storage._id,
          storageDetails: _storage,
          orders: []
        };

        this.dbOrdersFormatted.push(newGroup);
      });
    });
  }


  getStorageIdFromOrderId(orderId) {
    let storageId = null;
    angular.forEach(this.dbOrdersFormatted, (itm1) => {
      angular.forEach(itm1.orders, (itm2) => {
        if(orderId === itm2._id) {
          storageId = itm1.storageId;
        }
      });
    });

    return storageId || null;
  }


  draggableOrderMoved2(order, lst, index) {
    let newStorageId = this.getStorageIdFromOrderId(order._id);
    let oldStorageId = order.storageIdRef;
    let newOrderStatus = 'ASSIGNED';
    // let newOrderStatus = order.orderStatus;

    // check if newStorageId (coming from ui) is changed from storageId attached to the order (coming from database).
    let storageIdChanged = false;
    if(!order.storageIdRef && !newStorageId) {
      storageIdChanged = false;
    }
    else if(!order.storageIdRef && newStorageId) {
      storageIdChanged = true;
    }
    else if(order.storageIdRef._id !== newStorageId) {
      storageIdChanged = true;
    }

    // storageIdChanged? update the backend.
    if(storageIdChanged === true) {
      // if newStorageId is null (order became unassigned), set orderStatus to STARTED.
      if(!newStorageId) {
        newOrderStatus = 'OPEN';
      }
      // if oldStorageId was null (order became from unassigned to assigned), set orderStatus to ASSIGNED.
      if(!oldStorageId) {
        newOrderStatus = 'ASSIGNED';
      }

      this.serverCallIsRunning = true;
      if(order.collectionName === 'SO') {
        this.DbSalesOrderUpdate(order._id, { storageIdRef: newStorageId, orderStatus: newOrderStatus }, (data, error) => {
          this.serverCallIsRunning = false;
          if(error) {
            this.toastr.error('Could not update the order. <br>Try again later.');
            this.$log.error(error);
            return;
          }
          this.toastr.success('Successfully updated order.');
          this.$log.info(data);
          this.GfgHelpers.refreshPage({}, false);
        });
      }
      else if(order.collectionName === 'PO') {
        this.DbPurchaseOrderUpdate(order._id, { storageIdRef: newStorageId, orderStatus: newOrderStatus }, (data, error) => {
          this.serverCallIsRunning = false;
          if(error) {
            this.toastr.error('Could not update the order. <br>Try again later.');
            this.$log.error(error);
            return;
          }
          this.toastr.success('Successfully updated order.');
          this.$log.info(data);
          this.GfgHelpers.refreshPage({}, false);
        });
      }
    }
  }


  updateDbStorage(storageId, conditions) {
    this.DbStorageUpdate(storageId, conditions, (data, error) => {
      if(error) {
        this.toastr.error('Error updating storage. <br>Try again later.');
        this.$log.error(error);
        return;
      }
      this.toastr.success('Successfully updated storage.');
      this.GfgHelpers.refreshPage();
    });
  }


  setOrderToStartedOrCompleted(order, newStatus) {
    this.GfgModal.openSalesOrderCompleteModal({}, (userSelection) => {
      if(userSelection) {
        if(order.collectionName === 'SO') {
          this.DbSalesOrderUpdate(order._id, { orderStatus: newStatus }, (data, error) => {
            if(error) {
              this.toastr.error('Error updating order.');
              this.$log.error(error);
              return;
            }
            this.toastr.success('Successfully updated order.');
            this.GfgHelpers.refreshPage();
          });
        }
        else if(order.collectionName === 'PO') {
          this.DbPurchaseOrderUpdate(order._id, { orderStatus: newStatus }, (data, error) => {
            if(error) {
              this.toastr.error('Error updating order.');
              this.$log.error(error);
              return;
            }
            this.toastr.success('Successfully updated order.');
            this.GfgHelpers.refreshPage();
          });
        }
      }
    });
  }


  setOrderToCancelled(order) {
    this.GfgModal.openSalesOrderCancelModal({}, (userSelection) => {
      if(userSelection) {
        if(order.collectionName === 'SO') {
          this.DbSalesOrderUpdate(order._id, { orderStatus: 'CANCELLED', orderCancellationReason: userSelection }, (data, error) => {
            if(error) {
              this.toastr.error('Error updating order.');
              this.$log.error(error);
              return;
            }
            this.toastr.success('Successfully updated order.');
            this.GfgHelpers.refreshPage();
          });
        }
        else if(order.collectionName === 'PO') {
          this.DbPurchaseOrderUpdate(order._id, { orderStatus: 'CANCELLED', orderCancellationReason: userSelection }, (data, error) => {
            if(error) {
              this.toastr.error('Error updating order.');
              this.$log.error(error);
              return;
            }
            this.toastr.success('Successfully updated order.');
            this.GfgHelpers.refreshPage();
          });
        }
      }
    });
  }
}
