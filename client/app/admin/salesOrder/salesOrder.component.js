'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');

import routes from './salesOrder.routes';

export class AdminSalesOrderComponent {

  pageTitle = 'Sales Orders';
  salesOrdersList = [];
  queryParams = {
    fields: 'orderNumber orderStatus orderCostTotal created changed storageIdRef customerIdRef orderCancellationReason',
    sort: 'created',
    direction: 'DESC'
  };

  /*@ngInject*/
  constructor($log, $state, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource) {
    this.$log = $log;
    this.$state = $state;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;

    this.dbSalesOrdersList = this.DbRootResource.list(`${this.apiBaseUrl}/salesOrders`);
    this.dbSalesOrdersDelete = this.DbRootResource.delete(`${this.apiBaseUrl}/salesOrders`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.dbSalesOrdersList(this.queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving sales orders. <br>Try again later.');
        this.$log.error(error);
        return;
      }
      this.salesOrdersList = data;
      this.$log.info(data);
    });
  }


  deleteSalesOrder(salesOrderId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.dbSalesOrdersDelete(salesOrderId, (data, error) => {
          if(error) {
            this.toastr.error('Error deleting sales order. <br>Try again later.');
            this.$log.error(error);
            return;
          }
          this.toastr.success('Successfully deleted sales order.');
          this.GfgHelpers.refreshPage();
        });
      }
    });
  }
}


export default angular.module('gfgApp.adminSalesOrder', [uiRouter])
  .config(routes)
  .component('adminSalesOrderList', {
    template: require('./salesOrder.html'),
    controller: AdminSalesOrderComponent,
    controllerAs: 'vm'
  })
  .name;
