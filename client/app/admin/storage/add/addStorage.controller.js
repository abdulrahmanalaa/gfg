'use strict';

const _ = require('lodash');

export default class addStorageController {

  storage = {
    status: true,
    isAvailable: true,
    assignmentHistory: [],
    address: {
      googleMap: {}
    }
  };
  pageTitle = 'Add Storage';
  formSubmitted = false;
  mapSelectedLatLng = {
    latitude: null,
    longitude: null
  };
  dateFormat = 'dd/MM/yyyy';
  datePopupOptions = {
    dateDisabled: false,
    formatYear: 'yyyy',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };
  timePickerHourStep = 1;
  timePickerMinuteStep = 1;
  timePickerShowMeridian = true;
  mapOptions = {
    scrollwheel: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    mapTypeId: 'roadmap'
  };
  mapMarker = {
    id: 1,
    show: true,
    coords: {},
    options: {
      draggable: true,
      labelContent: 'Drag the pin to the desired location',
      labelAnchor: '100 70',
      labelClass: 'google-map-label-style-1'
    },
    events: {}
  };
  mapSearchBox = {
    template: 'google-map-search-box.html',
    position: 'TOP_RIGHT',
    options: {
      bounds: null
    },
    events: {
      places_changed: () => {} // @see setMapSearchBoxEvents().
    }
  };
  dbUsers = {
    leader: [],
    dispatcher: []
  };
  dbStorageTypes = [];
  addressGovernorates = [];
  addressAreas = [];
  addressBlocks = [];

  /*@ngInject*/
  constructor($scope, $log, $window, $state, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource, DbUserResource, DbTaxonomyAddressSectionResource, GOOGLE_MAPS_DEFAULT_CENTER) {
    this.$scope = $scope;
    this.$log = $log;
    this.$window = $window;
    this.$state = $state;
    this.toastr = toastr;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    this.apiBaseUrl = apiBaseUrl;
    this.GOOGLE_MAPS_DEFAULT_CENTER = GOOGLE_MAPS_DEFAULT_CENTER;

    this.addStorageResource = this.DbRootResource.create(`${this.apiBaseUrl}/storages`);
    this.GetDbStorageTypeResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.storageTypes`);

    this.GfgHelpers.setPageTitle(this.pageTitle);

    // this.$scope.$on('$stateChangeStart', (evt) => {
    //   if(this.form.$dirty && !$window.confirm('Are you sure you want to leave this page?')) {
    //     evt.preventDefault();
    //   }
    // });
  }


  $onInit() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.events.dragend = (marker, eventName, args) => {
      this.mapSelectedLatLng.latitude = marker.getPosition().lat();
      this.mapSelectedLatLng.longitude = marker.getPosition().lng();
      // this.setMapLocationProperty('latitude', marker.getPosition().lat());
      // this.setMapLocationProperty('longitude', marker.getPosition().lng());
    };

    this.getDbUsers();
    this.setMapSearchBoxEvents();
    this.getDbStorageTypes();
    this.getAddressGovernorates();
  }


  save(addAnotherItem = false) {
    this.formSubmitted = true;
    let emptyAddressField = { _id: null };

    if(_.isEmpty(this.storage.assignmentHistory)) {
      this.toastr.error('Assignment history can not be empty.');
      return;
    }

    if(this.form.$valid) {
      if(_.isEqual(this.storage.address.governorate, emptyAddressField)) {
        this.storage.address.governorate = null;
      }
      if(_.isEqual(this.storage.address.area, emptyAddressField)) {
        this.storage.address.area = null;
      }
      if(_.isEqual(this.storage.address.block, emptyAddressField)) {
        this.storage.address.block = null;
      }


      this.addStorageResource(this.storage, (res, err) => {
        if(err) {
          this.toastr.error('Error saving storage');
          this.$log.error(err);
          this.$state.go('adminStorageList');
          return;
        }
        this.toastr.success('Successfully created storage.');
        this.$log.info(res);
        if(addAnotherItem) {
          this.GfgHelpers.refreshPage({}, true);
        }
        else {
          this.$state.go('adminStorageList');
        }
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  cancel() {
    this.$state.go('adminStorageList');
  }


  setMapSearchBoxEvents() {
    // set mapSearchBox places_changed here to insure the validity of this.mapSelectedLatLng.
    this.mapSearchBox.events.places_changed = (searchBox) => {
      let places = searchBox.getPlaces();
      if(!_.isEmpty(places)) {
        this.mapSelectedLatLng.latitude = places[0].geometry.location.lat();
        this.mapSelectedLatLng.longitude = places[0].geometry.location.lng();
      }
    };
  }


  getDbUsers() {
    let queryParams = {
      sort: 'name',
      fields: 'name username userType email plateNumber',
      queryConditions: {
        status: 1,
        role: 'user'
      }
    };
    this.DbUserResource.list(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Could not retrieve users. <br>Try again later.');
        this.$log.error(error);
        this.$state.go('adminStorageList');
        return;
      }
      this.$log.info(data);
      this.dbUsers.leader = _.filter(data, { userType: 'leader' });
      this.dbUsers.dispatcher = _.filter(data, { userType: 'dispatcher' });
    });
  }


  getDbStorageTypes() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbStorageTypeResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving storage types');
        return;
      }
      this.dbStorageTypes = data;
    });
  }


  getAddressGovernorates() {
    this.DbTaxonomyAddressSectionResource.listBySectionType('governorate', (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving governorates.');
        return;
      }
      this.addressGovernorates = data;
    });
  }


  updateAddressAreas(governorateId) {
    if(governorateId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(governorateId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving areas.');
          return;
        }
        this.addressAreas = data;
      });
    }
  }


  updateAddressBlocks(areaId) {
    if(areaId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(areaId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving blocks.');
          return;
        }
        this.addressBlocks = data;
      });
    }
  }




  /**
   * Helper function to set location lat/lon after dragging the map marker.
   *
   * @param propertyName
   *  latitude or longitude.
   *
   * @param propertyValue
   *  Value to assign to propertyName
   */
  setMapLocationProperty(propertyName, propertyValue) {
    this.storage.address.googleMap[propertyName] = propertyValue;
  }

  googleMapCapture() {
    this.storage.address.googleMap.latitude = this.mapSelectedLatLng.latitude;
    this.storage.address.googleMap.longitude = this.mapSelectedLatLng.longitude;
  }


  googleMapClear() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.storage.address.googleMap.latitude = null;
    this.storage.address.googleMap.longitude = null;
  }

  addAssignmentHistory() {
    this.storage.assignmentHistory.push({});
  }

  deleteAssignmentHistory(idx) {
    this.storage.assignmentHistory.splice(idx, 1);
  }

  formClear() {
    this.GfgHelpers.refreshPage({}, false);
  }
}
