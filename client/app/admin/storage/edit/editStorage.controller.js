'use strict';

const _ = require('lodash');
const angular = require('angular');

export default class editStorageController {

  storage = {
    status: true,
    assignmentHistory: [],
    address: {
      googleMap: {}
    }
  };
  pageTitle = 'Edit Storage';
  formSubmitted = false;
  mapSelectedLatLng = {
    latitude: null,
    longitude: null
  };
  dateFormat = 'dd/MM/yyyy';
  datePopupOptions = {
    dateDisabled: false,
    formatYear: 'yyyy',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };
  timePickerHourStep = 1;
  timePickerMinuteStep = 1;
  timePickerShowMeridian = true;
  mapOptions = {
    scrollwheel: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    mapTypeId: 'roadmap'
  };
  mapMarker = {
    id: 1,
    show: true,
    coords: {},
    options: {
      draggable: true,
      labelContent: 'Drag the pin to the desired location',
      labelAnchor: '100 70',
      labelClass: 'google-map-label-style-1'
    },
    events: {}
  };
  mapSearchBox = {
    template: 'google-map-search-box.html',
    position: 'TOP_RIGHT',
    options: {
      bounds: null
    },
    events: {
      places_changed: () => {} // @see setMapSearchBoxEvents().
    }
  };
  dbUsers = {
    leader: [],
    dispatcher: []
  };
  dbStorageTypes = [];
  addressGovernorates = [];
  addressAreas = [];
  addressBlocks = [];


  /*@ngInject*/
  constructor($scope, $window, $log, $state, $stateParams, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource, DbUserResource, DbTaxonomyAddressSectionResource, GOOGLE_MAPS_DEFAULT_CENTER) {
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.toastr = toastr;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbUserResource = DbUserResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    this.apiBaseUrl = apiBaseUrl;
    this.GOOGLE_MAPS_DEFAULT_CENTER = GOOGLE_MAPS_DEFAULT_CENTER;

    this.getStorageResource = this.DbRootResource.get(`${this.apiBaseUrl}/storages`);
    this.updateStorageResource = this.DbRootResource.update(`${this.apiBaseUrl}/storages`);
    this.GetDbStorageTypeResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.storageTypes`);

    this.GfgHelpers.setPageTitle(this.pageTitle);

    // this.$scope.$on('$stateChangeStart', (evt) => {
    //   if(this.form.$dirty && !$window.confirm('Are you sure you want to leave this page?')) {
    //     evt.preventDefault();
    //   }
    // });
  }


  $onInit() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.events.dragend = (marker, eventName, args) => {
      this.mapSelectedLatLng.latitude = marker.getPosition().lat();
      this.mapSelectedLatLng.longitude = marker.getPosition().lng();
    };

    this.getStorageResource(this.$stateParams.storageId, (data, error) => {
      if(error) {
        this.toastr.error('Error loading storage. <br>Try again later.');
        this.$log.error(error);
        this.$state.go('adminStorageList');
        return;
      }
      this.$log.info(data);
      this.storage = data;
      this.mapMarker.coords = this.storage.address.googleMap;
      this.fixLoadedStorageFields();
      this.setMapSearchBoxEvents();
      this.getDbUsers();
      this.getDbStorageTypes();
      this.getAddressGovernorates();
      this.updateAddressAreas(this.storage.address.governorate);
      this.updateAddressBlocks(this.storage.address.area);
    });
  }


  save() {
    if(_.isEmpty(this.storage.assignmentHistory)) {
      this.toastr.error('Assignment history can not be empty.');
      return;
    }

    this.updateStorageResource(this.$stateParams.storageId, this.storage, (data, error) => {
      if(error) {
        this.toastr.error('Error saving storage. Try again later.');
        this.$log.error(error);
        this.$state.go('adminStorageList');
        return;
      }
      this.toastr.success('Successfully updated storage.');
      this.$state.go('adminStorageList');
    });
  }


  fixLoadedStorageFields() {
    if(_.isEmpty(this.storage.address.googleMap)) {
      this.storage.address.googleMap = {};
    }
    if(_.isEmpty(this.storage.address.googleMap.latitude) || _.isEmpty(this.storage.address.googleMap.longitude)) {
      this.storage.address.googleMap = {};
      this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    }
    else {
      this.mapSelectedLatLng = this.storage.address.googleMap;
      this.mapMarker.coords = this.storage.address.googleMap;
    }

    // format date fields to work fine with calendar popup.
    if(this.storage.registrationBookExpiration) {
      this.storage.registrationBookExpiration = new Date(this.storage.registrationBookExpiration);
    }
    angular.forEach(this.storage.assignmentHistory, (itm) => {
      if(itm.fromDate) {
        itm.fromDate = new Date(itm.fromDate);
      }
      if(itm.fromTime) {
        itm.fromTime = new Date(itm.fromTime);
      }
      if(itm.toDate) {
        itm.toDate = new Date(itm.toDate);
      }
    });
  }


  setMapSearchBoxEvents() {
    // set mapSearchBox places_changed here to insure the validity of this.mapSelectedLatLng.
    this.mapSearchBox.events.places_changed = (searchBox) => {
      let places = searchBox.getPlaces();
      if(!_.isEmpty(places)) {
        this.mapSelectedLatLng.latitude = places[0].geometry.location.lat();
        this.mapSelectedLatLng.longitude = places[0].geometry.location.lng();
      }
    };
  }


  getDbUsers() {
    let queryParams = {
      sort: 'name',
      fields: 'name username userType email plateNumber',
      queryConditions: {
        status: 1,
        role: 'user'
      }
    };
    this.DbUserResource.list(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Could not retrieve users. <br>Try again later.');
        this.$log.error(error);
        this.$state.go('adminStorageList');
        return;
      }
      this.$log.info(data);
      this.dbUsers.leader = _.filter(data, { userType: 'leader' });
      this.dbUsers.dispatcher = _.filter(data, { userType: 'dispatcher' });
    });
  }


  getDbStorageTypes() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbStorageTypeResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving storage types');
        return;
      }
      this.dbStorageTypes = data;
    });
  }


  getAddressGovernorates() {
    this.DbTaxonomyAddressSectionResource.listBySectionType('governorate', (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving governorates.');
        return;
      }
      this.addressGovernorates = data;
    });
  }


  updateAddressAreas(governorateId) {
    if(governorateId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(governorateId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving areas.');
          return;
        }
        this.addressAreas = data;
      });
    }
  }


  updateAddressBlocks(areaId) {
    if(areaId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(areaId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving blocks.');
          return;
        }
        this.addressBlocks = data;
      });
    }
  }



  /**
   * Helper function to set location lat/lon after dragging the map marker.
   *
   * @param propertyName
   *  latitude or longitude.
   *
   * @param propertyValue
   *  Value to assign to propertyName
   */
  setMapLocationProperty(propertyName, propertyValue) {
    this.storage.address.googleMap[propertyName] = propertyValue;
  }

  addAssignmentHistory() {
    this.storage.assignmentHistory.push({});
  }

  deleteAssignmentHistory(idx) {
    this.storage.assignmentHistory.splice(idx, 1);
  }
}
