'use strict';

import addStorageController from './add/addStorage.controller';
import editStorageController from './edit/editStorage.controller';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminStorageList', {
      url: '/admin/storage',
      template: '<admin-storage-list></admin-storage-list>',
      authenticate: 'admin'
    })
    .state('adminStorageAdd', {
      url: '/admin/storage/add',
      template: require('./add/addStorage.html'),
      controller: addStorageController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminStorageEdit', {
      url: '/admin/storage/{storageId}',
      template: require('./add/addStorage.html'),
      controller: editStorageController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
  ;
}
