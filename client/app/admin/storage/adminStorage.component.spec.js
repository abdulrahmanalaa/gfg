'use strict';

describe('Component: AdminStorageComponent', function() {
  // load the controller's module
  beforeEach(module('gfgApp.adminStorage'));

  var AdminStorageComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminStorageComponent = $componentController('adminStorage', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
