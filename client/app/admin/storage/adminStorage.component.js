'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './adminStorage.routes';

export class AdminStorageComponent {

  pageTitle = 'Storage';
  storageList = [];
  queryParams = {
    fields: 'storageId title plateNumber status isAvailable currentLeader currentDispatcher',
    sort: 'title',
    direction: 'asc'
  };

  /*@ngInject*/
  constructor($log, $state, toastr, apiBaseUrl, DbRootResource, GfgModal, GfgHelpers) {
    this.$log = $log;
    this.$state = $state;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.DbRootResource = DbRootResource;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;

    this.getStorageResource = this.DbRootResource.list(`${this.apiBaseUrl}/storages`);
    this.deleteStorageResource = this.DbRootResource.delete(`${this.apiBaseUrl}/storages`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getStorageResource(this.queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error loading storage list.');
        this.$log.error(error);
        return;
      }
      this.storageList = data;
      this.$log.info(data);
    });
  }


  deleteStorage(storageId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.deleteStorageResource(storageId, (data, err) => {
          if(err) {
            this.toastr.error('Error deleting storage. Try again Later.');
            return;
          }
          this.toastr.success('Successfully deleted storage.');
          this.GfgHelpers.refreshPage();
        });
      }
    });
  }
}


export default angular.module('gfgApp.adminStorage', [uiRouter])
  .config(routes)
  .component('adminStorageList', {
    template: require('./adminStorage.html'),
    controller: AdminStorageComponent,
    controllerAs: 'vm'
  })
  .name;
