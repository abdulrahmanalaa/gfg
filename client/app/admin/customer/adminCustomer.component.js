'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');

import routes from './adminCustomer.routes';

export class AdminCustomerComponent {

  pageTitle = 'Customers';
  customers = [];
  queryParams = {
    fields: 'accountingId name ownerName status email phoneNumber1',
    sort: 'title',
    direction: 'asc'
  };

  /*@ngInject*/
  constructor($state, toastr, apiBaseUrl, DbRootResource, GfgModal, GfgHelpers) {
    this.$state = $state;
    this.toastr = toastr;
    this.apiBaseUrl = apiBaseUrl;
    this.DbRootResource = DbRootResource;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;

    this.getCustomersResource = this.DbRootResource.list(`${this.apiBaseUrl}/customers`);
    this.deleteCustomersResource = this.DbRootResource.delete(`${this.apiBaseUrl}/customers`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.getCustomersResource(this.queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error loading customers.');
        return;
      }
      this.customers = data;
    });
  }


  deleteCustomer(customerId) {
    this.GfgModal.openDeleteModal({}, (userSelection) => {
      if(userSelection) {
        this.deleteCustomersResource(customerId, (data, err) => {
          if(err) {
            this.toastr.error('Error deleting customer. Try again Later.');
            return;
          }
          this.toastr.success('Successfully deleted customer.');
          this.GfgHelpers.refreshPage();
        });
      }
    });
  }
}


export default angular.module('gfgApp.adminCustomer', [uiRouter])
  .config(routes)
  .component('adminCustomerList', {
    template: require('./adminCustomer.html'),
    controller: AdminCustomerComponent,
    controllerAs: 'vm'
  })
  .name;
