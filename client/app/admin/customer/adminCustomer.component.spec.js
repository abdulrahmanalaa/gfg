'use strict';

describe('Component: AdminCustomerComponent', function() {
  // load the controller's module
  beforeEach(module('gfgApp.adminCustomer'));

  var AdminCustomerComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminCustomerComponent = $componentController('adminCustomer', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
