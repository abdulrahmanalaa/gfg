'use strict';

const _ = require('lodash');

export default class addCustomerController {

  customer = {
    status: true,
    contactInfo: [],
    pricingInfo: [],
    blockedMaterials: [],
    address: {
      governorate: null,
      area: null,
      block: null,
      googleMap: {}
    }
  };
  pageTitle = 'Add Customer';
  formSubmitted = false;
  mapSelectedLatLng = {
    latitude: null,
    longitude: null
  };
  mapOptions = {
    scrollwheel: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    mapTypeId: 'roadmap'
  };
  mapMarker = {
    id: 1,
    show: true,
    coords: {},
    options: {
      draggable: true,
      labelContent: 'Drag the pin to the desired location',
      labelAnchor: '100 70',
      labelClass: 'google-map-label-style-1'
    },
    events: {}
  };
  mapSearchBox = {
    template: 'google-map-search-box.html',
    position: 'TOP_RIGHT',
    options: {
      bounds: null
    },
    events: {
      places_changed: () => {} // @see setMapSearchBoxEvents().
    }
  };
  dbCustomerTypes = [];
  dbCustomerGroups = [];
  dbMaterials = [];
  dbDesignations = [];
  addressGovernorates = [];
  addressAreas = [];
  addressBlocks = [];

  /*@ngInject*/
  constructor($log, $state, toastr, apiBaseUrl, GfgModal, GfgHelpers, DbRootResource, DbTaxonomyAddressSectionResource, GOOGLE_MAPS_DEFAULT_CENTER) {
    this.$log = $log;
    this.$state = $state;
    this.toastr = toastr;
    this.GfgModal = GfgModal;
    this.GfgHelpers = GfgHelpers;
    this.DbRootResource = DbRootResource;
    this.DbTaxonomyAddressSectionResource = DbTaxonomyAddressSectionResource;
    this.apiBaseUrl = apiBaseUrl;
    this.GOOGLE_MAPS_DEFAULT_CENTER = GOOGLE_MAPS_DEFAULT_CENTER;

    this.addCustomerResource = this.DbRootResource.create(`${this.apiBaseUrl}/customers`);
    this.GetDbCustomerGroupsResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.customerGroups`);
    this.GetDbCustomerTypesResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.customerTypes`);
    this.GetDbMaterialsResource = this.DbRootResource.list(`${this.apiBaseUrl}/materials`);
    this.GetDbDesignationsResource = this.DbRootResource.list(`${this.apiBaseUrl}/taxonomy.designations`);

    this.GfgHelpers.setPageTitle(this.pageTitle);
  }


  $onInit() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.events.dragend = (marker, eventName, args) => {
      this.mapSelectedLatLng.latitude = marker.getPosition().lat();
      this.mapSelectedLatLng.longitude = marker.getPosition().lng();
      //this.setMapLocationProperty('latitude', marker.getPosition().lat());
      //this.setMapLocationProperty('longitude', marker.getPosition().lng());
    };

    this.setMapSearchBoxEvents();
    this.getDbCustomerTypes();
    this.getDbCustomerGroups();
    this.getDbMaterials();
    this.getDbDesignations();
    this.getAddressGovernorates();
  }


  save(addAnotherItem = false) {
    this.formSubmitted = true;
    let emptyAddressField = { _id: null };


    if(this.form.$valid) {
      if(_.isEqual(this.customer.address.governorate, emptyAddressField)) {
        this.customer.address.governorate = null;
      }
      if(_.isEqual(this.customer.address.area, emptyAddressField)) {
        this.customer.address.area = null;
      }
      if(_.isEqual(this.customer.address.block, emptyAddressField)) {
        this.customer.address.block = null;
      }

      this.addCustomerResource(this.customer, (res, err) => {
        if(err) {
          this.toastr.error('Error saving customer');
          this.$log.error(err);
          return;
        }
        this.toastr.success('Successfully created customer');
        this.$log.info(res);
        if(addAnotherItem) {
          this.GfgHelpers.refreshPage({}, true);
        }
        else {
          this.$state.go('adminCustomerList');
        }
      });
    }
    else {
      this.toastr.warning('Correct form errors before submitting.');
    }
  }


  cancel() {
    this.$state.go('adminCustomerList');
  }


  setMapSearchBoxEvents() {
    // set mapSearchBox places_changed here to insure the validity of this.mapSelectedLatLng.
    this.mapSearchBox.events.places_changed = (searchBox) => {
      let places = searchBox.getPlaces();
      if(!_.isEmpty(places)) {
        this.mapSelectedLatLng.latitude = places[0].geometry.location.lat();
        this.mapSelectedLatLng.longitude = places[0].geometry.location.lng();
      }
    };
  }


  getDbCustomerTypes() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbCustomerTypesResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving customer types');
        return;
      }
      this.dbCustomerTypes = data;
    });
  }


  getDbCustomerGroups() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbCustomerGroupsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving customer groups');
        return;
      }
      this.dbCustomerGroups = data;
    });
  }


  getDbMaterials() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbMaterialsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving materials');
        return;
      }
      this.dbMaterials = data;
    });
  }


  getDbDesignations() {
    let queryParams = { fields: 'title', sort: 'title' };
    this.GetDbDesignationsResource(queryParams, (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving designations');
        return;
      }
      this.dbDesignations = data;
    });
  }


  getAddressGovernorates() {
    this.DbTaxonomyAddressSectionResource.listBySectionType('governorate', (data, error) => {
      if(error) {
        this.toastr.error('Error retrieving governorates.');
        return;
      }
      this.addressGovernorates = data;
    });
  }


  updateAddressAreas(governorateId) {
    if(governorateId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(governorateId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving areas.');
          return;
        }
        this.addressAreas = data;
      });
    }
  }


  updateAddressBlocks(areaId) {
    if(areaId) {
      this.DbTaxonomyAddressSectionResource.listByParentId(areaId, (data, error) => {
        if(error) {
          this.toastr.error('Error retrieving blocks.');
          return;
        }
        this.addressBlocks = data;
      });
    }
  }



  /**
   * Helper function to set location lat/lon after dragging the map marker.
   *
   * @param propertyName
   *  latitude or longitude.
   *
   * @param propertyValue
   *  Value to assign to propertyName
   */
  setMapLocationProperty(propertyName, propertyValue) {
    this.customer.address.googleMap[propertyName] = propertyValue;
  }


  googleMapCapture() {
    this.customer.address.googleMap.latitude = this.mapSelectedLatLng.latitude;
    this.customer.address.googleMap.longitude = this.mapSelectedLatLng.longitude;
  }


  googleMapClear() {
    this.mapSelectedLatLng = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.mapMarker.coords = this.GOOGLE_MAPS_DEFAULT_CENTER;
    this.customer.address.googleMap.latitude = null;
    this.customer.address.googleMap.longitude = null;
  }


  addContactInfo() {
    this.customer.contactInfo.push({});
  }

  deleteContactInfo(idx) {
    this.customer.contactInfo.splice(idx, 1);
  }

  addMaterialInfo() {
    this.customer.pricingInfo.push({});
  }

  deleteMaterialInfo(idx) {
    this.customer.pricingInfo.splice(idx, 1);
  }

  addBlockedMaterial() {
    this.customer.blockedMaterials.push({});
  }

  deleteBlockedMaterial(idx) {
    this.customer.blockedMaterials.splice(idx, 1);
  }

  formClear() {
    this.GfgHelpers.refreshPage({}, false);
  }
}
