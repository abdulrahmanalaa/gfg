'use strict';

import addCustomerController from './add/addCustomer.controller';
import edirCustomerController from './edit/editCustomer.controller';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminCustomerList', {
      url: '/admin/customer',
      template: '<admin-customer-list></admin-customer-list>',
      authenticate: 'admin'
    })
    .state('adminCustomerAdd', {
      url: '/admin/customer/add',
      template: require('./add/addCustomer.html'),
      controller: addCustomerController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
    .state('adminCustomerEdit', {
      url: '/admin/customer/{customerId}',
      template: require('./add/addCustomer.html'),
      controller: edirCustomerController,
      controllerAs: 'vm',
      authenticate: 'admin'
    })
  ;
}
