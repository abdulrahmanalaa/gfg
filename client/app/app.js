'use strict';

import 'chosen-js';
import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import ngMessages from 'angular-messages';

import 'angular-socket-io';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import 'angular-validation-match';

// 3rd parties
import uiValidate from 'angular-ui-validate';
import toastr from 'angular-toastr';
import 'angular-loading-bar';
import 'angular-drag-and-drop-lists';
import 'angular-xeditable';
import 'angular-simple-logger'; // required for angular-google-maps
import 'angular-google-maps';
import 'angular-chosen-localytics';
import 'angular-base64-upload';
import 'angular-input-masks';

import {
  routeConfig,
  appCachedTemplates,
  angularToastrConfig,
  angularLoadingBarConfig,
  angularXeditableConfig,
  angularGoogleMapsConfig
} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';

// apps
import gfgDirectives from '../directives';
import databaseResources from '../services/database-resources';
import gfgHelpers from '../services/gfgHelpers';

// services
import GfgModal from '../services/gfgModal/gfgModal.factory';

import './app.scss';

angular.module('gfgApp', [
  ngCookies,
  ngResource,
  ngSanitize,
  ngMessages,
  'btford.socket-io',
  uiRouter,
  uiBootstrap,
  _Auth,
  account,
  admin,
  'validation.match',
  navbar,
  footer,
  main,
  constants,
  socket,
  util,
  // 3rd parties
  uiValidate,
  toastr,
  'angular-loading-bar',
  'dndLists',
  'xeditable',
  'uiGmapgoogle-maps',
  'localytics.directives',
  'naif.base64',
  'ui.utils.masks',
  // apps
  gfgDirectives,
  databaseResources,
  gfgHelpers
])

  // services
  .factory('GfgModal', GfgModal)

  // config
  .config(routeConfig)
  .config(angularToastrConfig)
  .config(angularLoadingBarConfig)
  .config(angularGoogleMapsConfig)

  // run
  .run(angularXeditableConfig)
  .run(appCachedTemplates)
  .run(function($rootScope, $location, Auth, GfgHelpers) {
    'ngInject';

    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function(event, next) {
      GfgHelpers.setPageTitle(null);

      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['gfgApp'], {
      strictDi: false
    });
  });
