'use strict';

export default class LoginController {
  user = {
    name: '',
    email: '',
    password: ''
  };
  errors = {
    login: undefined
  };
  submitted = false;


  /*@ngInject*/
  constructor($log, $state, GfgHelpers, Auth) {
    this.$log = $log;
    this.$state = $state;
    this.GfgHelpers = GfgHelpers;
    this.Auth = Auth;

    this.GfgHelpers.setPageTitle('Login');
  }

  login(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.login({
        email: this.user.email,
        password: this.user.password
      })
        .then((data) => {
          // Logged in, redirect to home
          this.$log.info(data);
          if(data.role === 'admin') {
            this.$state.go('main');
          }
          else if(data.role === 'user') {
            this.$state.go('adminSalesOrderDashboardDrag');
          }
          else {
            this.$state.go('main');
          }
        })
        .catch(err => {
          this.errors.login = err.message;
        });
    }
  }


  reset() {
    this.user = {
      name: '',
      email: '',
      password: ''
    };
  }
}
