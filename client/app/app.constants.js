'use strict';

import angular from 'angular';

export default angular.module('gfgApp.constants', [])
  .constant('appConfig', require('../../server/config/environment/shared'))
  .constant('APP_VERSION', '0.2.14')
  .constant('apiBaseUrl', '/api')
  .constant('GOOGLE_MAPS_API_KEYS', [
    // test keys
    'AIzaSyBV2cYw4IPxXdVJT3jpW_rBYB7DQKi3COY'
    // production keys
  ])

  .constant('AWS_S3_REMOTE_BASE_URL', 'https://bucketeer-d9bd964c-6902-410e-becf-438c5e673b37.s3.amazonaws.com/')

  .constant('GOOGLE_MAPS_DEFAULT_CENTER', {
    latitude: 29.310512,
    longitude: 47.766858
  })

  .name;
