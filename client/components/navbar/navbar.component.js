'use strict';
/* eslint no-sync: 0 */

const angular = require('angular');

export class NavbarComponent {
  isCollapsed = true;

  menu = [
    {
      title: 'Home',
      state: 'main'
    },
    {
      title: 'Dispatching Board',
      state: 'adminSalesOrderDashboardDrag'
    }
  ];

  masterDataMenu = [
    {
      title: 'Amendment Reasons',
      state: 'adminTaxonomyAmendmentReasonList',
      stateParams: {}
    },
    {
      title: 'Customer Groups',
      state: 'adminTaxonomyCustomerGroupList',
      stateParams: {}
    },
    {
      title: 'Customer Types',
      state: 'adminTaxonomyCustomerTypeList',
      stateParams: {}
    },
    {
      title: 'Customers',
      state: 'adminCustomerList',
      stateParams: {}
    },
    {
      title: 'Designations',
      state: 'adminTaxonomyDesignationList',
      stateParams: {}
    },
    {
      title: 'Governorates, Areas and Blocks',
      state: 'adminTaxonomyAddressSectionList',
      stateParams: {}
    },
    {
      title: 'Material Class',
      state: 'adminTaxonomyMaterialClassList',
      stateParams: {}
    },
    // {
    //   title: 'Material Types',
    //   state: 'adminTaxonomyMaterialTypeList',
    //   stateParams: {}
    // },
    {
      title: 'Materials',
      state: 'adminMaterialList',
      stateParams: {}
    },
    {
      title: 'Order Cancellation Reasons',
      state: 'adminTaxonomyOrderCancelReasonList',
      stateParams: {}
    },
    {
      title: 'Payment Methods',
      state: 'adminTaxonomyPaymentMethodList',
      stateParams: {}
    },
    {
      title: 'Storage Types',
      state: 'adminTaxonomyStorageTypeList',
      stateParams: {}
    },
    {
      title: 'Storage',
      state: 'adminStorageList',
      stateParams: {}
    },
    {
      title: 'Supplier Groups',
      state: 'adminTaxonomySupplierGroupList',
      stateParams: {}
    },
    {
      title: 'Suppliers',
      state: 'adminSupplierList',
      stateParams: {}
    },
    {
      title: 'Users',
      state: 'adminUserList',
      stateParams: {}
    }
  ];

  constructor(Auth) {
    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

}

export default angular.module('directives.navbar', [])
  .component('navbar', {
    template: require('./navbar.html'),
    controller: NavbarComponent,
    controllerAs: 'vm'
  })
  .name;
