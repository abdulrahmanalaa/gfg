import angular from 'angular';

export class FooterComponent {

  constructor(APP_VERSION) {
    this.APP_VERSION = APP_VERSION;
  }
}

export default angular.module('directives.footer', [])
  .component('footer', {
    template: require('./footer.html'),
    controller: FooterComponent,
    controllerAs: 'vm'
  })
  .name;
