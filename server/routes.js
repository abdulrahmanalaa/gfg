/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  app.use('/api/purchaseOrders', require('./api/purchaseOrder'));
  app.use('/api/salesOrders', require('./api/salesOrder'));
  app.use('/api/customers', require('./api/customer'));
  app.use('/api/storages', require('./api/storage'));
  app.use('/api/materials', require('./api/material'));
  app.use('/api/taxonomy.addressSections', require('./api/taxonomy.addressSection'));
  app.use('/api/taxonomy.materialClasses', require('./api/taxonomy.materialClass'));
  app.use('/api/taxonomy.materialTypes', require('./api/taxonomy.materialType'));
  app.use('/api/taxonomy.amendmentReasons', require('./api/taxonomy.amendmentReason'));
  app.use('/api/taxonomy.designations', require('./api/taxonomy.designation'));
  app.use('/api/taxonomy.customerTypes', require('./api/taxonomy.customerType'));
  app.use('/api/taxonomy.customerGroups', require('./api/taxonomy.customerGroup'));
  app.use('/api/taxonomy.orderCancelReasons', require('./api/taxonomy.orderCancelReason'));
  app.use('/api/taxonomy.supplierGroups', require('./api/taxonomy.supplierGroup'));
  app.use('/api/taxonomy.paymentMethods', require('./api/taxonomy.paymentMethod'));
  app.use('/api/taxonomy.storageTypes', require('./api/taxonomy.storageType'));
  app.use('/api/suppliers', require('./api/supplier'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth').default);
  app.use('/api/static', require('./api/static'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
