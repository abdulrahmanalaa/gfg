/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/customers              ->  index
 * POST    /api/customers              ->  create
 * GET     /api/customers/:id          ->  show
 * PUT     /api/customers/:id          ->  upsert
 * PATCH   /api/customers/:id          ->  patch
 * DELETE  /api/customers/:id          ->  destroy
 */

'use strict';

const _ = require('lodash');
import jsonpatch from 'fast-json-patch';
import Customer from './customer.model';

import * as customUtils from '../../utils';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Customers
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return Customer
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    // populate customerType
    .populate({ path: 'customerType', select: 'title' })
    // populate address fields
    .populate({ path: 'address.governorate', select: 'title' })
    .populate({ path: 'address.area', select: 'title' })
    .populate({ path: 'address.block', select: 'title' })
    // populate contactInfo fields
    .populate({ path: 'contactInfo.designation', select: 'title' })
    // populate pricingInfo fields
    .populate({ path: 'pricingInfo.material' })
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Customer from the DB
export function show(req, res) {
  return Customer
    .findById(req.params.id)
    // populate customerType
    .populate({ path: 'customerType', select: 'title' })
    // populate address fields
    .populate({ path: 'address.governorate', select: 'title' })
    .populate({ path: 'address.area', select: 'title' })
    .populate({ path: 'address.block', select: 'title' })
    // populate contactInfo fields
    .populate({ path: 'contactInfo.designation', select: 'title' })
    // populate pricingInfo fields
    .populate({ path: 'pricingInfo.material' })
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Customer in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  // sort by assignmentHistory date before saving.
  if(req.body.pricingInfo && !_.isEmpty(req.body.pricingInfo)) {
    req.body.pricingInfo = req.body.pricingInfo.sort((x, y) => y.validTo > x.validTo);
  }

  return Customer.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Customer in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  // sort by assignmentHistory date before saving.
  if(req.body.pricingInfo && !_.isEmpty(req.body.pricingInfo)) {
    req.body.pricingInfo = req.body.pricingInfo.sort((x, y) => y.validTo > x.validTo);
  }

  return Customer.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Customer in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return Customer.findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Customer from the DB
export function destroy(req, res) {
  return Customer.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


/**
 *
 *
 * @param req
 *  expects req.params to contain
 *    - fields: (string) fields to select separated by spaces.
 *    - sort: (string) field to sort by.
 *        default search direction is ascending, add "-" before the field to sort descending.
 *    - search: (object) search fields and conditions.
 *        each search object should consist of
 *          {
 *            fieldToSearch: {
 *              value: the value to evaluate "fieldToSearch" against.
 *              operator: available options [ =, gt, lt, in, regex ]. defaults to =.
 *            },
 *            fieldToSearch2: {},
 *            fieldToSearch3: {}
 *          }
 * @param res
 * @returns {Promise.<TResult>|Promise}
 */
export function search(req, res) {

  let queryConditions = {};
  let querySearch = req.body.search || {};
  let queryFields = req.body.select || '';
  let querySort = req.body.sort || '-created';
  let validSearchOperators = [
    '=', 'regex', 'gt', 'lt', 'in'
  ];

  _.forEach(querySearch, (item, key) => {
    if(_.isEmpty(item.operator)) {
      item.operator = '=';
    }
    if(validSearchOperators.indexOf(item.operator) === -1) {
      item.operator = '=';
    }

    if(item.operator === '=') {
      // queryConditions[key] = item.value;
      queryConditions[key] = {
        $regex: item.value,
        //$options: ''
      };
    }
    if(item.operator === 'regex') {
      queryConditions[key] = {
        $regex: item.value,
        $options: 'i'
      };
    }
    else {
      queryConditions[key] = {};
      queryConditions[key][`$${item.operator}`] = item.value;
    }
  });

  console.log(queryConditions, 'queryConditions');

  return Customer
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    // populate customerType
    .populate({ path: 'customerType', select: 'title' })
    // populate address fields
    .populate({ path: 'address.governorate', select: 'title' })
    .populate({ path: 'address.area', select: 'title' })
    .populate({ path: 'address.block', select: 'title' })
    // populate contactInfo fields
    .populate({ path: 'contactInfo.designation', select: 'title' })
    // populate pricingInfo fields
    .populate({ path: 'pricingInfo.material' })
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
