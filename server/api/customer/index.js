'use strict';

let express = require('express');
let controller = require('./customer.controller');
import * as auth from '../../auth/auth.service';

let router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/search', auth.isAuthenticated(), controller.search);
// router.post('/search', controller.search);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.upsert);
router.patch('/:id', auth.isAuthenticated(), controller.patch);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);

module.exports = router;
