'use strict';

import mongoose from 'mongoose';
let mongooseAutoIncrement = require('mongoose-auto-increment');
import {registerEvents} from './customer.events';

let CustomerSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  status: { type: Boolean, required: true, Default: false },
  accountingId: { type: String, required: false, trim: true },
  customerId: { type: Number },
  name: { type: String, required: true, trim: true },
  ownerName: { type: String, trim: true },
  email: { type: String, trim: true },
  phoneNumber1: { type: Number, trim: true },
  phoneNumber2: { type: Number, trim: true },
  blockingReason: { type: String, trim: true },
  customerType: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyCustomerType' },
  customerGroup: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyCustomerGroup' },
  // address
  address: {
    // @TODO; some address fields to be replaced by references.
    governorate: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    area: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    block: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    street: { type: String, trim: true },
    building: { type: String, trim: true },
    paciNo: { type: String, trim: true },
    notes: { type: String, trim: true },
    googleMap: {
      _id: false,
      latitude: { type: String, trim: true },
      longitude: { type: String, trim: true }
    }
  },
  // contact info
  contactInfo: [
    {
      _id: { type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId() },
      name: { type: String, trim: true },
      designation: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyDesignation' },
      email: { type: String, trim: true },
      phoneNumber1: { type: String, trim: true },
      phoneNumber2: { type: String, trim: true }
    }
  ],
  // pricing info
  pricingInfo: [
    {
      _id: { type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId() },
      material: { type: mongoose.Schema.Types.ObjectId, ref: 'Material' },
      price: { type: String, trim: true },
      validFrom: { type: Date },
      validTo: { type: Date }
    }
  ],
  // blocked materials
  blockedMaterials: [
    {
      material: { type: mongoose.Schema.Types.ObjectId, ref: 'Material' }
    }
  ]
});

CustomerSchema.plugin(mongooseAutoIncrement.plugin, {
  model: 'Customer',
  field: 'customerId',
  startAt: 1000,
  incrementBy: 1
});

registerEvents(CustomerSchema);
export default mongoose.model('Customer', CustomerSchema);
