/**
 * TaxonomyOrderCancelReason model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyOrderCancelReasonEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyOrderCancelReasonEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyOrderCancelReason) {
  for(var e in events) {
    let event = events[e];
    TaxonomyOrderCancelReason.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyOrderCancelReasonEvents.emit(event + ':' + doc._id, doc);
    TaxonomyOrderCancelReasonEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyOrderCancelReasonEvents;
