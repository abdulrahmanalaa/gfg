'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyOrderCancelReason;

describe('TaxonomyOrderCancelReason API:', function() {
  describe('GET /api/taxonomy.orderCancelReasons', function() {
    var taxonomy.orderCancelReasons;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.orderCancelReasons')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.orderCancelReasons = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.orderCancelReasons).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.orderCancelReasons', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.orderCancelReasons')
        .send({
          name: 'New TaxonomyOrderCancelReason',
          info: 'This is the brand new taxonomy.orderCancelReason!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyOrderCancelReason = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.orderCancelReason', function() {
      expect(newTaxonomyOrderCancelReason.name).to.equal('New TaxonomyOrderCancelReason');
      expect(newTaxonomyOrderCancelReason.info).to.equal('This is the brand new taxonomy.orderCancelReason!!!');
    });
  });

  describe('GET /api/taxonomy.orderCancelReasons/:id', function() {
    var taxonomy.orderCancelReason;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.orderCancelReasons/${newTaxonomyOrderCancelReason._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.orderCancelReason = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.orderCancelReason = {};
    });

    it('should respond with the requested taxonomy.orderCancelReason', function() {
      expect(taxonomy.orderCancelReason.name).to.equal('New TaxonomyOrderCancelReason');
      expect(taxonomy.orderCancelReason.info).to.equal('This is the brand new taxonomy.orderCancelReason!!!');
    });
  });

  describe('PUT /api/taxonomy.orderCancelReasons/:id', function() {
    var updatedTaxonomyOrderCancelReason;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.orderCancelReasons/${newTaxonomyOrderCancelReason._id}`)
        .send({
          name: 'Updated TaxonomyOrderCancelReason',
          info: 'This is the updated taxonomy.orderCancelReason!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyOrderCancelReason = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyOrderCancelReason = {};
    });

    it('should respond with the updated taxonomy.orderCancelReason', function() {
      expect(updatedTaxonomyOrderCancelReason.name).to.equal('Updated TaxonomyOrderCancelReason');
      expect(updatedTaxonomyOrderCancelReason.info).to.equal('This is the updated taxonomy.orderCancelReason!!!');
    });

    it('should respond with the updated taxonomy.orderCancelReason on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.orderCancelReasons/${newTaxonomyOrderCancelReason._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.orderCancelReason = res.body;

          expect(taxonomy.orderCancelReason.name).to.equal('Updated TaxonomyOrderCancelReason');
          expect(taxonomy.orderCancelReason.info).to.equal('This is the updated taxonomy.orderCancelReason!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.orderCancelReasons/:id', function() {
    var patchedTaxonomyOrderCancelReason;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.orderCancelReasons/${newTaxonomyOrderCancelReason._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyOrderCancelReason' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.orderCancelReason!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyOrderCancelReason = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyOrderCancelReason = {};
    });

    it('should respond with the patched taxonomy.orderCancelReason', function() {
      expect(patchedTaxonomyOrderCancelReason.name).to.equal('Patched TaxonomyOrderCancelReason');
      expect(patchedTaxonomyOrderCancelReason.info).to.equal('This is the patched taxonomy.orderCancelReason!!!');
    });
  });

  describe('DELETE /api/taxonomy.orderCancelReasons/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.orderCancelReasons/${newTaxonomyOrderCancelReason._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.orderCancelReason does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.orderCancelReasons/${newTaxonomyOrderCancelReason._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
