'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.orderCancelReasonCtrlStub = {
  index: 'taxonomy.orderCancelReasonCtrl.index',
  show: 'taxonomy.orderCancelReasonCtrl.show',
  create: 'taxonomy.orderCancelReasonCtrl.create',
  upsert: 'taxonomy.orderCancelReasonCtrl.upsert',
  patch: 'taxonomy.orderCancelReasonCtrl.patch',
  destroy: 'taxonomy.orderCancelReasonCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.orderCancelReasonIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.orderCancelReason.controller': taxonomy.orderCancelReasonCtrlStub
});

describe('TaxonomyOrderCancelReason API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.orderCancelReasonIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.orderCancelReasons', function() {
    it('should route to taxonomy.orderCancelReason.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.orderCancelReasonCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.orderCancelReasons/:id', function() {
    it('should route to taxonomy.orderCancelReason.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.orderCancelReasonCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.orderCancelReasons', function() {
    it('should route to taxonomy.orderCancelReason.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.orderCancelReasonCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.orderCancelReasons/:id', function() {
    it('should route to taxonomy.orderCancelReason.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.orderCancelReasonCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.orderCancelReasons/:id', function() {
    it('should route to taxonomy.orderCancelReason.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.orderCancelReasonCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.orderCancelReasons/:id', function() {
    it('should route to taxonomy.orderCancelReason.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.orderCancelReasonCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
