/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/taxonomy.orderCancelReasons              ->  index
 * POST    /api/taxonomy.orderCancelReasons              ->  create
 * GET     /api/taxonomy.orderCancelReasons/:id          ->  show
 * PUT     /api/taxonomy.orderCancelReasons/:id          ->  upsert
 * PATCH   /api/taxonomy.orderCancelReasons/:id          ->  patch
 * DELETE  /api/taxonomy.orderCancelReasons/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import TaxonomyOrderCancelReason from './taxonomy.orderCancelReason.model';

import * as customUtils from '../../utils';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of TaxonomyOrderCancelReasons
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return TaxonomyOrderCancelReason
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single TaxonomyOrderCancelReason from the DB
export function show(req, res) {
  return TaxonomyOrderCancelReason.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new TaxonomyOrderCancelReason in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  return TaxonomyOrderCancelReason.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given TaxonomyOrderCancelReason in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return TaxonomyOrderCancelReason.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing TaxonomyOrderCancelReason in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return TaxonomyOrderCancelReason.findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a TaxonomyOrderCancelReason from the DB
export function destroy(req, res) {
  return TaxonomyOrderCancelReason.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
