'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.supplierGroupCtrlStub = {
  index: 'taxonomy.supplierGroupCtrl.index',
  show: 'taxonomy.supplierGroupCtrl.show',
  create: 'taxonomy.supplierGroupCtrl.create',
  upsert: 'taxonomy.supplierGroupCtrl.upsert',
  patch: 'taxonomy.supplierGroupCtrl.patch',
  destroy: 'taxonomy.supplierGroupCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.supplierGroupIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.supplierGroup.controller': taxonomy.supplierGroupCtrlStub
});

describe('TaxonomySupplierGroup API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.supplierGroupIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.supplierGroups', function() {
    it('should route to taxonomy.supplierGroup.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.supplierGroupCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.supplierGroups/:id', function() {
    it('should route to taxonomy.supplierGroup.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.supplierGroupCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.supplierGroups', function() {
    it('should route to taxonomy.supplierGroup.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.supplierGroupCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.supplierGroups/:id', function() {
    it('should route to taxonomy.supplierGroup.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.supplierGroupCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.supplierGroups/:id', function() {
    it('should route to taxonomy.supplierGroup.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.supplierGroupCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.supplierGroups/:id', function() {
    it('should route to taxonomy.supplierGroup.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.supplierGroupCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
