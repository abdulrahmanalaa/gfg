'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomySupplierGroup;

describe('TaxonomySupplierGroup API:', function() {
  describe('GET /api/taxonomy.supplierGroups', function() {
    var taxonomy.supplierGroups;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.supplierGroups')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.supplierGroups = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.supplierGroups).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.supplierGroups', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.supplierGroups')
        .send({
          name: 'New TaxonomySupplierGroup',
          info: 'This is the brand new taxonomy.supplierGroup!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomySupplierGroup = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.supplierGroup', function() {
      expect(newTaxonomySupplierGroup.name).to.equal('New TaxonomySupplierGroup');
      expect(newTaxonomySupplierGroup.info).to.equal('This is the brand new taxonomy.supplierGroup!!!');
    });
  });

  describe('GET /api/taxonomy.supplierGroups/:id', function() {
    var taxonomy.supplierGroup;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.supplierGroups/${newTaxonomySupplierGroup._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.supplierGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.supplierGroup = {};
    });

    it('should respond with the requested taxonomy.supplierGroup', function() {
      expect(taxonomy.supplierGroup.name).to.equal('New TaxonomySupplierGroup');
      expect(taxonomy.supplierGroup.info).to.equal('This is the brand new taxonomy.supplierGroup!!!');
    });
  });

  describe('PUT /api/taxonomy.supplierGroups/:id', function() {
    var updatedTaxonomySupplierGroup;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.supplierGroups/${newTaxonomySupplierGroup._id}`)
        .send({
          name: 'Updated TaxonomySupplierGroup',
          info: 'This is the updated taxonomy.supplierGroup!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomySupplierGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomySupplierGroup = {};
    });

    it('should respond with the updated taxonomy.supplierGroup', function() {
      expect(updatedTaxonomySupplierGroup.name).to.equal('Updated TaxonomySupplierGroup');
      expect(updatedTaxonomySupplierGroup.info).to.equal('This is the updated taxonomy.supplierGroup!!!');
    });

    it('should respond with the updated taxonomy.supplierGroup on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.supplierGroups/${newTaxonomySupplierGroup._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.supplierGroup = res.body;

          expect(taxonomy.supplierGroup.name).to.equal('Updated TaxonomySupplierGroup');
          expect(taxonomy.supplierGroup.info).to.equal('This is the updated taxonomy.supplierGroup!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.supplierGroups/:id', function() {
    var patchedTaxonomySupplierGroup;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.supplierGroups/${newTaxonomySupplierGroup._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomySupplierGroup' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.supplierGroup!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomySupplierGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomySupplierGroup = {};
    });

    it('should respond with the patched taxonomy.supplierGroup', function() {
      expect(patchedTaxonomySupplierGroup.name).to.equal('Patched TaxonomySupplierGroup');
      expect(patchedTaxonomySupplierGroup.info).to.equal('This is the patched taxonomy.supplierGroup!!!');
    });
  });

  describe('DELETE /api/taxonomy.supplierGroups/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.supplierGroups/${newTaxonomySupplierGroup._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.supplierGroup does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.supplierGroups/${newTaxonomySupplierGroup._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
