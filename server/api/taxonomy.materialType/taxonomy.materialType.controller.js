/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/taxonomy.materialTypes              ->  index
 * POST    /api/taxonomy.materialTypes              ->  create
 * GET     /api/taxonomy.materialTypes/:id          ->  show
 * PUT     /api/taxonomy.materialTypes/:id          ->  upsert
 * PATCH   /api/taxonomy.materialTypes/:id          ->  patch
 * DELETE  /api/taxonomy.materialTypes/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import TaxonomyMaterialType from './taxonomy.materialType.model';

import * as customUtils from '../../utils';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of TaxonomyMaterialTypes
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return TaxonomyMaterialType
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single TaxonomyMaterialType from the DB
export function show(req, res) {
  return TaxonomyMaterialType.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new TaxonomyMaterialType in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  return TaxonomyMaterialType.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given TaxonomyMaterialType in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return TaxonomyMaterialType.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing TaxonomyMaterialType in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return TaxonomyMaterialType.findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a TaxonomyMaterialType from the DB
export function destroy(req, res) {
  return TaxonomyMaterialType.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
