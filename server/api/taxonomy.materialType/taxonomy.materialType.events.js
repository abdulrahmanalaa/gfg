/**
 * TaxonomyMaterialType model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyMaterialTypeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyMaterialTypeEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyMaterialType) {
  for(var e in events) {
    let event = events[e];
    TaxonomyMaterialType.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyMaterialTypeEvents.emit(event + ':' + doc._id, doc);
    TaxonomyMaterialTypeEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyMaterialTypeEvents;
