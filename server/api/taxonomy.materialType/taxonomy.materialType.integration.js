'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyMaterialType;

describe('TaxonomyMaterialType API:', function() {
  describe('GET /api/taxonomy.materialTypes', function() {
    var taxonomy.materialTypes;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.materialTypes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.materialTypes = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.materialTypes).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.materialTypes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.materialTypes')
        .send({
          name: 'New TaxonomyMaterialType',
          info: 'This is the brand new taxonomy.materialType!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyMaterialType = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.materialType', function() {
      expect(newTaxonomyMaterialType.name).to.equal('New TaxonomyMaterialType');
      expect(newTaxonomyMaterialType.info).to.equal('This is the brand new taxonomy.materialType!!!');
    });
  });

  describe('GET /api/taxonomy.materialTypes/:id', function() {
    var taxonomy.materialType;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.materialTypes/${newTaxonomyMaterialType._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.materialType = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.materialType = {};
    });

    it('should respond with the requested taxonomy.materialType', function() {
      expect(taxonomy.materialType.name).to.equal('New TaxonomyMaterialType');
      expect(taxonomy.materialType.info).to.equal('This is the brand new taxonomy.materialType!!!');
    });
  });

  describe('PUT /api/taxonomy.materialTypes/:id', function() {
    var updatedTaxonomyMaterialType;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.materialTypes/${newTaxonomyMaterialType._id}`)
        .send({
          name: 'Updated TaxonomyMaterialType',
          info: 'This is the updated taxonomy.materialType!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyMaterialType = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyMaterialType = {};
    });

    it('should respond with the updated taxonomy.materialType', function() {
      expect(updatedTaxonomyMaterialType.name).to.equal('Updated TaxonomyMaterialType');
      expect(updatedTaxonomyMaterialType.info).to.equal('This is the updated taxonomy.materialType!!!');
    });

    it('should respond with the updated taxonomy.materialType on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.materialTypes/${newTaxonomyMaterialType._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.materialType = res.body;

          expect(taxonomy.materialType.name).to.equal('Updated TaxonomyMaterialType');
          expect(taxonomy.materialType.info).to.equal('This is the updated taxonomy.materialType!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.materialTypes/:id', function() {
    var patchedTaxonomyMaterialType;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.materialTypes/${newTaxonomyMaterialType._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyMaterialType' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.materialType!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyMaterialType = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyMaterialType = {};
    });

    it('should respond with the patched taxonomy.materialType', function() {
      expect(patchedTaxonomyMaterialType.name).to.equal('Patched TaxonomyMaterialType');
      expect(patchedTaxonomyMaterialType.info).to.equal('This is the patched taxonomy.materialType!!!');
    });
  });

  describe('DELETE /api/taxonomy.materialTypes/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.materialTypes/${newTaxonomyMaterialType._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.materialType does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.materialTypes/${newTaxonomyMaterialType._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
