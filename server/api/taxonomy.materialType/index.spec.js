'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.materialTypeCtrlStub = {
  index: 'taxonomy.materialTypeCtrl.index',
  show: 'taxonomy.materialTypeCtrl.show',
  create: 'taxonomy.materialTypeCtrl.create',
  upsert: 'taxonomy.materialTypeCtrl.upsert',
  patch: 'taxonomy.materialTypeCtrl.patch',
  destroy: 'taxonomy.materialTypeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.materialTypeIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.materialType.controller': taxonomy.materialTypeCtrlStub
});

describe('TaxonomyMaterialType API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.materialTypeIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.materialTypes', function() {
    it('should route to taxonomy.materialType.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.materialTypeCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.materialTypes/:id', function() {
    it('should route to taxonomy.materialType.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.materialTypeCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.materialTypes', function() {
    it('should route to taxonomy.materialType.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.materialTypeCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.materialTypes/:id', function() {
    it('should route to taxonomy.materialType.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.materialTypeCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.materialTypes/:id', function() {
    it('should route to taxonomy.materialType.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.materialTypeCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.materialTypes/:id', function() {
    it('should route to taxonomy.materialType.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.materialTypeCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
