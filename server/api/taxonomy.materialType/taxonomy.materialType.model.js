'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './taxonomy.materialType.events';

let TaxonomyMaterialTypeSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },
  status: { type: Boolean, required: true, Default: false },
  title: { type: String, required: true, unique: true, trim: true }
});

registerEvents(TaxonomyMaterialTypeSchema);
export default mongoose.model('TaxonomyMaterialType', TaxonomyMaterialTypeSchema);
