'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.designationCtrlStub = {
  index: 'taxonomy.designationCtrl.index',
  show: 'taxonomy.designationCtrl.show',
  create: 'taxonomy.designationCtrl.create',
  upsert: 'taxonomy.designationCtrl.upsert',
  patch: 'taxonomy.designationCtrl.patch',
  destroy: 'taxonomy.designationCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.designationIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.designation.controller': taxonomy.designationCtrlStub
});

describe('TaxonomyDesignation API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.designationIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.designations', function() {
    it('should route to taxonomy.designation.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.designationCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.designations/:id', function() {
    it('should route to taxonomy.designation.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.designationCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.designations', function() {
    it('should route to taxonomy.designation.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.designationCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.designations/:id', function() {
    it('should route to taxonomy.designation.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.designationCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.designations/:id', function() {
    it('should route to taxonomy.designation.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.designationCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.designations/:id', function() {
    it('should route to taxonomy.designation.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.designationCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
