'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyDesignation;

describe('TaxonomyDesignation API:', function() {
  describe('GET /api/taxonomy.designations', function() {
    var taxonomy.designations;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.designations')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.designations = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.designations).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.designations', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.designations')
        .send({
          name: 'New TaxonomyDesignation',
          info: 'This is the brand new taxonomy.designation!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyDesignation = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.designation', function() {
      expect(newTaxonomyDesignation.name).to.equal('New TaxonomyDesignation');
      expect(newTaxonomyDesignation.info).to.equal('This is the brand new taxonomy.designation!!!');
    });
  });

  describe('GET /api/taxonomy.designations/:id', function() {
    var taxonomy.designation;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.designations/${newTaxonomyDesignation._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.designation = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.designation = {};
    });

    it('should respond with the requested taxonomy.designation', function() {
      expect(taxonomy.designation.name).to.equal('New TaxonomyDesignation');
      expect(taxonomy.designation.info).to.equal('This is the brand new taxonomy.designation!!!');
    });
  });

  describe('PUT /api/taxonomy.designations/:id', function() {
    var updatedTaxonomyDesignation;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.designations/${newTaxonomyDesignation._id}`)
        .send({
          name: 'Updated TaxonomyDesignation',
          info: 'This is the updated taxonomy.designation!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyDesignation = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyDesignation = {};
    });

    it('should respond with the updated taxonomy.designation', function() {
      expect(updatedTaxonomyDesignation.name).to.equal('Updated TaxonomyDesignation');
      expect(updatedTaxonomyDesignation.info).to.equal('This is the updated taxonomy.designation!!!');
    });

    it('should respond with the updated taxonomy.designation on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.designations/${newTaxonomyDesignation._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.designation = res.body;

          expect(taxonomy.designation.name).to.equal('Updated TaxonomyDesignation');
          expect(taxonomy.designation.info).to.equal('This is the updated taxonomy.designation!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.designations/:id', function() {
    var patchedTaxonomyDesignation;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.designations/${newTaxonomyDesignation._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyDesignation' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.designation!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyDesignation = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyDesignation = {};
    });

    it('should respond with the patched taxonomy.designation', function() {
      expect(patchedTaxonomyDesignation.name).to.equal('Patched TaxonomyDesignation');
      expect(patchedTaxonomyDesignation.info).to.equal('This is the patched taxonomy.designation!!!');
    });
  });

  describe('DELETE /api/taxonomy.designations/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.designations/${newTaxonomyDesignation._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.designation does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.designations/${newTaxonomyDesignation._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
