/**
 * TaxonomyDesignation model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyDesignationEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyDesignationEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyDesignation) {
  for(var e in events) {
    let event = events[e];
    TaxonomyDesignation.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyDesignationEvents.emit(event + ':' + doc._id, doc);
    TaxonomyDesignationEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyDesignationEvents;
