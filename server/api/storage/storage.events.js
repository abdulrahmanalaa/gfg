/**
 * Storage model events
 */

'use strict';

import {EventEmitter} from 'events';
var StorageEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
StorageEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Storage) {
  for(var e in events) {
    let event = events[e];
    Storage.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    StorageEvents.emit(event + ':' + doc._id, doc);
    StorageEvents.emit(event, doc);
  };
}

export {registerEvents};
export default StorageEvents;
