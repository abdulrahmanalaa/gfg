/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/storages              ->  index
 * POST    /api/storages              ->  create
 * GET     /api/storages/:id          ->  show
 * PUT     /api/storages/:id          ->  upsert
 * PATCH   /api/storages/:id          ->  patch
 * DELETE  /api/storages/:id          ->  destroy
 */

'use strict';

const _ = require('lodash');
import jsonpatch from 'fast-json-patch';
import Storage from './storage.model';

import * as customUtils from '../../utils';

const leaderDispatcherFieldsToPopulate = 'name email phoneNumber1';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Storages
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return Storage
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    .populate({ path: 'storageType', select: 'title' })
    .populate({ path: 'currentLeader', select: leaderDispatcherFieldsToPopulate })
    .populate({ path: 'currentDispatcher', select: leaderDispatcherFieldsToPopulate })
    .populate({ path: 'assignmentHistory.leader', select: leaderDispatcherFieldsToPopulate })
    .populate({ path: 'assignmentHistory.dispatcher', select: leaderDispatcherFieldsToPopulate })
    .exec()
    .then(respondWithResult(res))
    // .then((data) => {
    //   // sort assignmentHistory by fromDate descending.
    //   _.forEach(data, (dataItem) => {
    //     // if there are assignmentHistory items.
    //     if(!_.isEmpty(dataItem.assignmentHistory)) {
    //       // let assignmentHistoryOrdered = _.sortBy(dataItem.assignmentHistory, 'fromDate').reverse();
    //       // dataItem.assignmentHistory = assignmentHistoryOrdered;
    //       dataItem.assignmentHistory = dataItem.assignmentHistory.sort((x, y) => y.fromDate > x.fromDate);
    //     }
    //     return res.status(200).json(data);
    //   });
    // })
    .catch(handleError(res));
}

// Gets a single Storage from the DB
export function show(req, res) {
  return Storage
    .findById(req.params.id)
    .populate({ path: 'storageType', select: 'title' })
    .populate({ path: 'currentLeader', select: leaderDispatcherFieldsToPopulate })
    .populate({ path: 'currentDispatcher', select: leaderDispatcherFieldsToPopulate })
    .populate({ path: 'assignmentHistory.leader', select: leaderDispatcherFieldsToPopulate })
    .populate({ path: 'assignmentHistory.dispatcher', select: leaderDispatcherFieldsToPopulate })
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    // .then((data) => {
    //   // sort assignmentHistory by fromDate descending.
    //   let assignmentHistoryOrdered = _.sortBy(data.assignmentHistory, 'fromDate').reverse();
    //   data.assignmentHistory = assignmentHistoryOrdered;
    //   return res.status(200).json(data);
    // })
    .catch(handleError(res));
}

// Creates a new Storage in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  // sort by assignmentHistory date before saving.
  if(req.body.assignmentHistory && !_.isEmpty(req.body.assignmentHistory)) {
    req.body.assignmentHistory = req.body.assignmentHistory.sort((x, y) => y.fromDate > x.fromDate);
    req.body.currentLeader = req.body.assignmentHistory[0].leader;
    req.body.currentDispatcher = req.body.assignmentHistory[0].dispatcher;
  }

  return Storage.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Storage in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  // sort by assignmentHistory date before saving.
  if(req.body.assignmentHistory && !_.isEmpty(req.body.assignmentHistory)) {
    req.body.assignmentHistory = req.body.assignmentHistory.sort((x, y) => y.fromDate > x.fromDate);
    req.body.currentLeader = req.body.assignmentHistory[0].leader;
    req.body.currentDispatcher = req.body.assignmentHistory[0].dispatcher;
  }

  return Storage
    .findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Storage in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return Storage
    .findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Storage from the DB
export function destroy(req, res) {
  return Storage.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
