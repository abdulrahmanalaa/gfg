'use strict';

import mongoose from 'mongoose';
let mongooseAutoIncrement = require('mongoose-auto-increment');
import {registerEvents} from './storage.events';

let StorageSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  status: { type: Boolean, required: true, Default: false },
  isAvailable: { type: Boolean, required: true, Default: true },
  storageId: { type: Number },
  title: { type: String, required: true, trim: true },
  storageType: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyStorageType', required: false },
  plateNumber: { type: String, trim: true },
  registrationBookExpiration: { type: Date },
  blockingReason: { type: String, trim: true },
  currentLeader: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
  currentDispatcher: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },

  // address
  address: {
    governorate: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    area: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    block: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    street: { type: String, trim: true },
    building: { type: String, trim: true },
    paciNo: { type: String, trim: true },
    notes: { type: String, trim: true },
    googleMap: {
      _id: false,
      latitude: { type: String, trim: true },
      longitude: { type: String, trim: true }
    }
  },

  // assignmentHistory
  assignmentHistory: [
    {
      _id: { type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId() },
      fromDate: { type: Date },
      fromTime: { type: Date },
      toDate: { type: Date },
      toTime: { type: Date },
      leader: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      dispatcher: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }
  ]
});

StorageSchema.plugin(mongooseAutoIncrement.plugin, {
  model: 'Storage',
  field: 'storageId',
  startAt: 1000,
  incrementBy: 1
});

registerEvents(StorageSchema);
export default mongoose.model('Storage', StorageSchema);
