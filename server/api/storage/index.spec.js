'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var storageCtrlStub = {
  index: 'storageCtrl.index',
  show: 'storageCtrl.show',
  create: 'storageCtrl.create',
  upsert: 'storageCtrl.upsert',
  patch: 'storageCtrl.patch',
  destroy: 'storageCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var storageIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './storage.controller': storageCtrlStub
});

describe('Storage API Router:', function() {
  it('should return an express router instance', function() {
    expect(storageIndex).to.equal(routerStub);
  });

  describe('GET /api/storages', function() {
    it('should route to storage.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'storageCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/storages/:id', function() {
    it('should route to storage.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'storageCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/storages', function() {
    it('should route to storage.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'storageCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/storages/:id', function() {
    it('should route to storage.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'storageCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/storages/:id', function() {
    it('should route to storage.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'storageCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/storages/:id', function() {
    it('should route to storage.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'storageCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
