'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newStorage;

describe('Storage API:', function() {
  describe('GET /api/storages', function() {
    var storages;

    beforeEach(function(done) {
      request(app)
        .get('/api/storages')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          storages = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(storages).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/storages', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/storages')
        .send({
          name: 'New Storage',
          info: 'This is the brand new storage!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newStorage = res.body;
          done();
        });
    });

    it('should respond with the newly created storage', function() {
      expect(newStorage.name).to.equal('New Storage');
      expect(newStorage.info).to.equal('This is the brand new storage!!!');
    });
  });

  describe('GET /api/storages/:id', function() {
    var storage;

    beforeEach(function(done) {
      request(app)
        .get(`/api/storages/${newStorage._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          storage = res.body;
          done();
        });
    });

    afterEach(function() {
      storage = {};
    });

    it('should respond with the requested storage', function() {
      expect(storage.name).to.equal('New Storage');
      expect(storage.info).to.equal('This is the brand new storage!!!');
    });
  });

  describe('PUT /api/storages/:id', function() {
    var updatedStorage;

    beforeEach(function(done) {
      request(app)
        .put(`/api/storages/${newStorage._id}`)
        .send({
          name: 'Updated Storage',
          info: 'This is the updated storage!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedStorage = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedStorage = {};
    });

    it('should respond with the updated storage', function() {
      expect(updatedStorage.name).to.equal('Updated Storage');
      expect(updatedStorage.info).to.equal('This is the updated storage!!!');
    });

    it('should respond with the updated storage on a subsequent GET', function(done) {
      request(app)
        .get(`/api/storages/${newStorage._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let storage = res.body;

          expect(storage.name).to.equal('Updated Storage');
          expect(storage.info).to.equal('This is the updated storage!!!');

          done();
        });
    });
  });

  describe('PATCH /api/storages/:id', function() {
    var patchedStorage;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/storages/${newStorage._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Storage' },
          { op: 'replace', path: '/info', value: 'This is the patched storage!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedStorage = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedStorage = {};
    });

    it('should respond with the patched storage', function() {
      expect(patchedStorage.name).to.equal('Patched Storage');
      expect(patchedStorage.info).to.equal('This is the patched storage!!!');
    });
  });

  describe('DELETE /api/storages/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/storages/${newStorage._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when storage does not exist', function(done) {
      request(app)
        .delete(`/api/storages/${newStorage._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
