'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var salesOrderCtrlStub = {
  index: 'salesOrderCtrl.index',
  show: 'salesOrderCtrl.show',
  create: 'salesOrderCtrl.create',
  upsert: 'salesOrderCtrl.upsert',
  patch: 'salesOrderCtrl.patch',
  destroy: 'salesOrderCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var salesOrderIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './salesOrder.controller': salesOrderCtrlStub
});

describe('SalesOrder API Router:', function() {
  it('should return an express router instance', function() {
    expect(salesOrderIndex).to.equal(routerStub);
  });

  describe('GET /api/salesOrders', function() {
    it('should route to salesOrder.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'salesOrderCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/salesOrders/:id', function() {
    it('should route to salesOrder.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'salesOrderCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/salesOrders', function() {
    it('should route to salesOrder.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'salesOrderCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/salesOrders/:id', function() {
    it('should route to salesOrder.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'salesOrderCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/salesOrders/:id', function() {
    it('should route to salesOrder.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'salesOrderCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/salesOrders/:id', function() {
    it('should route to salesOrder.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'salesOrderCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
