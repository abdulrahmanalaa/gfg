'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newSalesOrder;

describe('SalesOrder API:', function() {
  describe('GET /api/salesOrders', function() {
    var salesOrders;

    beforeEach(function(done) {
      request(app)
        .get('/api/salesOrders')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          salesOrders = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(salesOrders).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/salesOrders', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/salesOrders')
        .send({
          name: 'New SalesOrder',
          info: 'This is the brand new salesOrder!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newSalesOrder = res.body;
          done();
        });
    });

    it('should respond with the newly created salesOrder', function() {
      expect(newSalesOrder.name).to.equal('New SalesOrder');
      expect(newSalesOrder.info).to.equal('This is the brand new salesOrder!!!');
    });
  });

  describe('GET /api/salesOrders/:id', function() {
    var salesOrder;

    beforeEach(function(done) {
      request(app)
        .get(`/api/salesOrders/${newSalesOrder._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          salesOrder = res.body;
          done();
        });
    });

    afterEach(function() {
      salesOrder = {};
    });

    it('should respond with the requested salesOrder', function() {
      expect(salesOrder.name).to.equal('New SalesOrder');
      expect(salesOrder.info).to.equal('This is the brand new salesOrder!!!');
    });
  });

  describe('PUT /api/salesOrders/:id', function() {
    var updatedSalesOrder;

    beforeEach(function(done) {
      request(app)
        .put(`/api/salesOrders/${newSalesOrder._id}`)
        .send({
          name: 'Updated SalesOrder',
          info: 'This is the updated salesOrder!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedSalesOrder = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSalesOrder = {};
    });

    it('should respond with the updated salesOrder', function() {
      expect(updatedSalesOrder.name).to.equal('Updated SalesOrder');
      expect(updatedSalesOrder.info).to.equal('This is the updated salesOrder!!!');
    });

    it('should respond with the updated salesOrder on a subsequent GET', function(done) {
      request(app)
        .get(`/api/salesOrders/${newSalesOrder._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let salesOrder = res.body;

          expect(salesOrder.name).to.equal('Updated SalesOrder');
          expect(salesOrder.info).to.equal('This is the updated salesOrder!!!');

          done();
        });
    });
  });

  describe('PATCH /api/salesOrders/:id', function() {
    var patchedSalesOrder;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/salesOrders/${newSalesOrder._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched SalesOrder' },
          { op: 'replace', path: '/info', value: 'This is the patched salesOrder!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedSalesOrder = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedSalesOrder = {};
    });

    it('should respond with the patched salesOrder', function() {
      expect(patchedSalesOrder.name).to.equal('Patched SalesOrder');
      expect(patchedSalesOrder.info).to.equal('This is the patched salesOrder!!!');
    });
  });

  describe('DELETE /api/salesOrders/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/salesOrders/${newSalesOrder._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when salesOrder does not exist', function(done) {
      request(app)
        .delete(`/api/salesOrders/${newSalesOrder._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
