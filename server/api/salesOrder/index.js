'use strict';

let express = require('express');
let controller = require('./salesOrder.controller');
import * as auth from '../../auth/auth.service';

let router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/resetOrderNumberCount', auth.hasRole('admin'), controller.resetOrderNumberCount);
router.get('/me', auth.isAuthenticated(), controller.myOrders);
router.get('/dashboard', auth.isAuthenticated(), controller.indexDashboard);
router.get('/groupedByField/:field', auth.isAuthenticated(), controller.indexGroupedByField);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.upsert);
router.patch('/:id', auth.isAuthenticated(), controller.patch);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
