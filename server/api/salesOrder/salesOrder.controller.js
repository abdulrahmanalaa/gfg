/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/salesOrders              ->  index
 * POST    /api/salesOrders              ->  create
 * GET     /api/salesOrders/:id          ->  show
 * PUT     /api/salesOrders/:id          ->  upsert
 * PATCH   /api/salesOrders/:id          ->  patch
 * DELETE  /api/salesOrders/:id          ->  destroy
 */

'use strict';

const _ = require('lodash');

import jsonpatch from 'fast-json-patch';
import SalesOrder from './salesOrder.model';
import PurchaseOrder from '../purchaseOrder/purchaseOrder.model';
import Storage from '../storage/storage.model';

import * as customUtils from '../../utils';

const uidFieldsToPopulate = 'name email';
const storageFieldsToPopulate = 'title plateNumber status isAvailable currentLeader currentDispatcher';
const leaderFieldsToPopulate = 'status name email plateNumber phoneNumber1';
const customerFieldsToPopulate = 'status accountingId name ownerName email phoneNumber1 phoneNumber2 address pricingInfo customerType blockedMaterials';
const supplierFieldsToPopulate = 'accountingId name email phoneNumber1 phoneNumber2 address supplierGroup';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of SalesOrders
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return SalesOrder
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    //.populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
    .populate({ path: 'storageIdRef', select: storageFieldsToPopulate })
    .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
    .populate({ path: 'orderCancellationReason', select: 'title' })
    .deepPopulate(''
      + ' storageIdRef.currentLeader'
      + ' storageIdRef.currentDispatcher'
    )
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of SalesOrders for dashboard.
export function indexDashboard(req, res) {
  let output = [];

  return SalesOrder
    .find({
      orderStatus: { $nin: ['COMPLETED', 'CANCELLED'] }
    })
    .populate({ path: 'uid', select: uidFieldsToPopulate })
    .populate({ path: 'storageIdRef', select: storageFieldsToPopulate, match: { status: true } })
    // .populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
    .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
    .populate({ path: 'orderCancellationReason', select: 'title' })
    .deepPopulate(''
      + ' storageIdRef.currentLeader'
      + ' storageIdRef.currentDispatcher'
      + ' customerIdRef.customerType'
      + ' customerIdRef.address.governorate'
      + ' customerIdRef.address.area'
      + ' customerIdRef.address.block'
    )
    .exec()
    .then(function(data) {
      PurchaseOrder
        .find({
          orderStatus: { $nin: ['COMPLETED', 'CANCELLED'] }
        })
        .populate({ path: 'uid', select: uidFieldsToPopulate })
        .populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
        .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
        .populate({ path: 'orderCancellationReason', select: 'title' })
        .deepPopulate(''
          + ' storageIdRef.currentLeader'
          + ' storageIdRef.currentDispatcher'
          + ' supplierIdRef.supplierGroup'
          + ' supplierIdRef.address.governorate'
          + ' supplierIdRef.address.area'
          + ' supplierIdRef.address.block'
        )
        .exec()
        .then(function(data2) {
          _.forEach(data2, (item2) => {
            output.push(item2);
          });

          output = output.sort((x, y) => x.created < y.created);

          return res.status(200).json(output);
        });

      _.forEach(data, (item) => {
        output.push(item);
      });

      output = output.sort((x, y) => x.created < y.created);
    })
    .catch(handleError(res));

  // return SalesOrder
  //   .find({
  //     orderStatus: { $nin: ['COMPLETED', 'CANCELLED'] }
  //   })
  //   .populate({ path: 'uid', select: uidFieldsToPopulate })
  //   .populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
  //   .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
  //   .populate({ path: 'orderCancellationReason', select: 'title' })
  //   .deepPopulate(''
  //     + ' customerIdRef.customerType'
  //     + ' customerIdRef.address.governorate'
  //     + ' customerIdRef.address.area'
  //     + ' customerIdRef.address.block'
  //   )
  //   .sort('-created')
  //   .exec()
  //   .then(respondWithResult(res))
  //   .catch(handleError(res));
}

export function indexGroupedByField(req, res) {
  let leaderUid = req.user._id;

  Storage
    .find({ currentLeader: leaderUid })
    .select('_id')
    .exec()
    .then(function(data) {
      let storageIds = _.map(data, '_id');
      if(storageIds.length === 0) {
        res
          .status(403)
          .json({ error: 'User has no assigned storage.' })
          .end();
      }

      SalesOrder
        .find({
          storageIdRef: { $in: storageIds },
        })
        .populate({ path: 'uid', select: uidFieldsToPopulate })
        .populate({ path: 'storageIdRef', select: storageFieldsToPopulate })
        // .populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
        .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
        .populate({ path: 'orderCancellationReason', select: 'title' })
        .deepPopulate(''
          + ' customerIdRef.customerType'
          + ' customerIdRef.address.governorate'
          + ' customerIdRef.address.area'
          + ' customerIdRef.address.block'
        )
        //.sort('-created')
        .exec()
        .then(function(data) {
          let output = {};
          let field = req.params.field;

          _.forEach(data, (itm, idx) => {
            if(!output[itm[field]]) {
              output[itm[field]] = [];
            }
            output[itm[field]].push(itm);
          });

          res
            .status(200)
            .json(output)
            .end();
        })
        .catch(handleError(res));
    })
    .catch(handleError(res));

  // return SalesOrder
  //   .find({
  //     leaderIdRef: uid
  //   })
  //   .populate({ path: 'uid', select: uidFieldsToPopulate })
  //   .populate({ path: 'storageIdRef', select: storageFieldsToPopulate })
  //   // .populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
  //   .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
  //   .populate({ path: 'orderCancellationReason', select: 'title' })
  //   .deepPopulate(''
  //     + ' customerIdRef.customerType'
  //     + ' customerIdRef.address.governorate'
  //     + ' customerIdRef.address.area'
  //     + ' customerIdRef.address.block'
  //   )
  //   //.sort('-created')
  //   .exec()
  //   .then(function(data) {
  //     let output = {};
  //     let field = req.params.field;
  //
  //     _.forEach(data, (itm, idx) => {
  //       if(!output[itm[field]]) {
  //         output[itm[field]] = [];
  //       }
  //       output[itm[field]].push(itm);
  //     });
  //
  //     res
  //       .status(200)
  //       .json(output)
  //       .end();
  //   })
  //   .catch(handleError(res));
}

// Gets a single SalesOrder from the DB
export function show(req, res) {
  return SalesOrder
    .findById(req.params.id)
    //.populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
    .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
    .populate({ path: 'orderCancellationReason', select: 'title' })
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new SalesOrder in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  return SalesOrder.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given SalesOrder in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return SalesOrder.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing SalesOrder in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return SalesOrder.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a SalesOrder from the DB
export function destroy(req, res) {
  return SalesOrder.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function myOrders(req, res) {
  let leaderUid = req.user._id;

  Storage
    .find({ currentLeader: leaderUid })
    .select('_id')
    .exec()
    .then(function(data) {
      let storageIds = _.map(data, '_id');
      if(storageIds.length === 0) {
        res
          .status(403)
          .json({ error: 'User has no assigned storage.' })
          .end();
      }

      SalesOrder
        .find({
          storageIdRef: { $in: storageIds },
          orderStatus: { $nin: ['COMPLETED', 'CANCELLED'] }
        })
        .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
        .sort('-created')
        .exec()
        .then(respondWithResult(res))
        .catch(handleError(res));
    })
    .catch(handleError(res));

  // let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);
  //
  // queryConditions = queryConditions || {};
  // queryConditions.leaderIdRef = leaderUid;
  //
  // return SalesOrder
  //   .find(queryConditions)
  //   .sort(querySort)
  //   .limit(queryLimit)
  //   .skip(querySkip)
  //   //.populate({ path: 'uid', select: uidFieldsToPopulate })
  //   //.populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
  //   .populate({ path: 'customerIdRef', select: customerFieldsToPopulate })
  //   .exec()
  //   .then(respondWithResult(res))
  //   .catch(handleError(res));
}

export function resetOrderNumberCount(req, res) {
  return SalesOrder
    .findOne({})
    .exec()
    .then(handleEntityNotFound(res))
    .then(function(data) {
      data.nextCount(function(error, count) {
        data.resetCount(function(err, nextCount) {
          if(err) {
            return res.status(500).json({ status: false, error: err });
          }
          else {
            return res.status(200).json({ status: true, nextCount });
          }
        });
      });
    })
    .catch(handleError(res));
}
