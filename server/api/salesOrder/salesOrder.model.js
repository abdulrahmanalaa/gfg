'use strict';

import mongoose from 'mongoose';
let mongooseAutoIncrement = require('mongoose-auto-increment');
let mongooseDeepPopulate = require('mongoose-deep-populate')(mongoose);
import {registerEvents} from './salesOrder.events';

let SalesOrderSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  status: { type: Boolean, required: true, Default: true },
  orderNumber: { type: Number },
  orderStatus: {
    type: String,
    Default: null,
    enum: ['OPEN', 'ASSIGNED', 'STARTED', 'COMPLETED', 'CANCELLED']
  },
  orderCancellationReason: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyOrderCancelReason', default: null, required: false },
  // completed: { type: Boolean, required: true, Default: false },
  notes: { type: String, trim: true },
  // leaderIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null, required: false },
  storageIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Storage', default: null, required: false },
  customerIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer', required: true },
  orderCostTotal: { type: String, required: false },
  materialInfo: [
    {
      materialIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Material', required: false },
      cost: { type: String, required: false },
      costTotal: { type: String, required: false },
      quantity: { type: Number, required: false },
      // quantityActual: { type: Number, required: false },
    }
  ],
  materialReturnedSpares: [
    {
      materialIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Material', required: false },
      returnedSpares: { type: Number, required: false },
      returnedSparesActual: { type: Number, required: false }
    }
  ]
});

SalesOrderSchema.plugin(mongooseAutoIncrement.plugin, {
  model: 'SalesOrder',
  field: 'orderNumber',
  startAt: 10000000,
  incrementBy: 1
});

SalesOrderSchema.plugin(mongooseDeepPopulate, {
  populate: {
    'storageIdRef.currentLeader': {
      select: 'name plateNumber'
    },
    'storageIdRef.currentDispatcher': {
      select: 'name plateNumber'
    },
    'customerIdRef.customerType': {
      select: 'title'
    },
    'customerIdRef.address.governorate': {
      select: 'title'
    },
    'customerIdRef.address.area': {
      select: 'title'
    },
    'customerIdRef.address.block': {
      select: 'title'
    },
  }
});

registerEvents(SalesOrderSchema);
export default mongoose.model('SalesOrder', SalesOrderSchema);
