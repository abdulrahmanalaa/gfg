'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './supplier.events';

let SupplierSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  status: { type: Boolean, required: true, Default: true },
  accountingId: { type: String, required: false, trim: true },
  name: { type: String, required: true, trim: true },
  // type: { type: String, required: true },
  email: { type: String, trim: true },
  phoneNumber1: { type: Number, trim: true },
  phoneNumber2: { type: Number, trim: true },
  blockingReason: { type: String, trim: true },
  supplierGroup: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomySupplierGroup' },
  // address
  address: {
    governorate: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    area: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    block: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection' },
    street: { type: String, trim: true },
    jaddah: { type: String, trim: true },
    building: { type: String, trim: true },
    paciNo: { type: String, trim: true },
    notes: { type: String, trim: true },
    googleMap: {
      _id: false,
      latitude: { type: String, trim: true },
      longitude: { type: String, trim: true }
    }
  },
  // material info
  // materialInfo: [
  //   {
  //     _id: { type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId() },
  //     accountingId: { type: String, trim: true },
  //     cost: { type: String, trim: true }
  //   }
  // ],
  // contact info
  contactInfo: [
    {
      _id: { type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId() },
      name: { type: String, trim: true },
      designation: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyDesignation' },
      email: { type: String, trim: true },
      phoneNumber1: { type: String, trim: true },
      phoneNumber2: { type: String, trim: true }
    }
  ],
  // pricing info
  pricingInfo: [
    {
      _id: { type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId() },
      material: { type: mongoose.Schema.Types.ObjectId, ref: 'Material' },
      price: { type: String, trim: true },
      validFrom: { type: Date },
      validTo: { type: Date }
    }
  ]
});


registerEvents(SupplierSchema);
export default mongoose.model('Supplier', SupplierSchema);
