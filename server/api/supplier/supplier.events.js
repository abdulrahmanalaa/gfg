/**
 * Supplier model events
 */

'use strict';

import {EventEmitter} from 'events';
var SupplierEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
SupplierEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Supplier) {
  for(var e in events) {
    let event = events[e];
    Supplier.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    SupplierEvents.emit(event + ':' + doc._id, doc);
    SupplierEvents.emit(event, doc);
  };
}

export {registerEvents};
export default SupplierEvents;
