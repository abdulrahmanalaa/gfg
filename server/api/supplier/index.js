'use strict';

let express = require('express');
let controller = require('./supplier.controller');
import * as auth from '../../auth/auth.service';

let router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
router.put('/:id', auth.hasRole('admin'), controller.upsert);
router.patch('/:id', auth.hasRole('admin'), controller.patch);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);

router.post('/updateMultiple', auth.hasRole('admin'), controller.updateMultiple);
router.post('/deleteMultiple', auth.hasRole('admin'), controller.destroyMultiple);

module.exports = router;
