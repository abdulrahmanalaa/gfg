/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/suppliers              ->  index
 * POST    /api/suppliers              ->  create
 * GET     /api/suppliers/:id          ->  show
 * PUT     /api/suppliers/:id          ->  upsert
 * PATCH   /api/suppliers/:id          ->  patch
 * DELETE  /api/suppliers/:id          ->  destroy
 */

'use strict';

const _ = require('lodash');
import jsonpatch from 'fast-json-patch';
import Supplier from './supplier.model';

import * as customUtils from '../../utils';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Suppliers
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return Supplier
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    // populate supplierGroup
    .populate({ path: 'supplierGroup', select: 'title' })
    // populate address fields
    .populate({ path: 'address.governorate', select: 'title' })
    .populate({ path: 'address.area', select: 'title' })
    .populate({ path: 'address.block', select: 'title' })
    // populate contactInfo fields
    .populate({ path: 'contactInfo.designation', select: 'title' })
    // populate pricingInfo fields
    .populate({ path: 'pricingInfo.material' })
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Supplier from the DB
export function show(req, res) {
  return Supplier
    .findById(req.params.id)
    // populate supplierGroup
    .populate({ path: 'supplierGroup', select: 'title' })
    // populate address fields
    .populate({ path: 'address.governorate', select: 'title' })
    .populate({ path: 'address.area', select: 'title' })
    .populate({ path: 'address.block', select: 'title' })
    // populate contactInfo fields
    .populate({ path: 'contactInfo.designation', select: 'title' })
    // populate pricingInfo fields
    .populate({ path: 'pricingInfo.material' })
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Supplier in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  return Supplier.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Supplier in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return Supplier.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Supplier in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return Supplier.findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Supplier from the DB
export function destroy(req, res) {
  return Supplier.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function updateMultiple(req, res) {
  if(!req.body.ids || !_.isArray(req.body.ids)) {
    return res.status(404)
      .json({ error: 'Missing suppliers ids.' })
      .end();
  }

  if(!req.body.fields || !_.isPlainObject(req.body.fields)) {
    return res.status(404)
      .json({ error: 'Missing suppliers fields.' })
      .end();
  }

  return Supplier.update(
    { _id: { $in: req.body.ids } },
    { $set: req.body.fields },
    { multi: true }
  )
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


export function destroyMultiple(req, res) {
  if(!req.body.ids || !_.isArray(req.body.ids)) {
    return res.status(404)
      .json({ error: 'Missing suppliers ids.' })
      .end();
  }

  return Supplier.remove({ _id: { $in: req.body.ids } })
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
