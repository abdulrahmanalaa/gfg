/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/taxonomy.addressSections              ->  index
 * POST    /api/taxonomy.addressSections              ->  create
 * GET     /api/taxonomy.addressSections/:id          ->  show
 * PUT     /api/taxonomy.addressSections/:id          ->  upsert
 * PATCH   /api/taxonomy.addressSections/:id          ->  patch
 * DELETE  /api/taxonomy.addressSections/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import TaxonomyAddressSection from './taxonomy.addressSection.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of TaxonomyAddressSections
export function index(req, res) {
  return TaxonomyAddressSection.find()
    .populate({ path: 'parentId', select: 'title' })
    .sort('title')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single TaxonomyAddressSection from the DB
export function show(req, res) {
  return TaxonomyAddressSection.findById(req.params.id)
    .populate({ path: 'parentId', select: 'title' })
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new TaxonomyAddressSection in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  return TaxonomyAddressSection.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given TaxonomyAddressSection in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return TaxonomyAddressSection.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing TaxonomyAddressSection in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return TaxonomyAddressSection.findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a TaxonomyAddressSection from the DB
export function destroy(req, res) {
  return TaxonomyAddressSection.findById(req.params.id)
    .exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

export function indexByType(req, res) {
  return TaxonomyAddressSection.find({ sectionType: req.params.sectionType })
    .populate({ path: 'parentId', select: 'title' })
    .sort('title')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function indexByParentId(req, res) {
  return TaxonomyAddressSection.find({ parentId: req.params.parentId })
    .populate({ path: 'parentId', select: 'title' })
    .sort('title')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
