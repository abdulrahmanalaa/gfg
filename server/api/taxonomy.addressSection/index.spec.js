'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.addressSectionCtrlStub = {
  index: 'taxonomy.addressSectionCtrl.index',
  show: 'taxonomy.addressSectionCtrl.show',
  create: 'taxonomy.addressSectionCtrl.create',
  upsert: 'taxonomy.addressSectionCtrl.upsert',
  patch: 'taxonomy.addressSectionCtrl.patch',
  destroy: 'taxonomy.addressSectionCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.addressSectionIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.addressSection.controller': taxonomy.addressSectionCtrlStub
});

describe('TaxonomyAddressSection API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.addressSectionIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.addressSections', function() {
    it('should route to taxonomy.addressSection.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.addressSectionCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.addressSections/:id', function() {
    it('should route to taxonomy.addressSection.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.addressSectionCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.addressSections', function() {
    it('should route to taxonomy.addressSection.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.addressSectionCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.addressSections/:id', function() {
    it('should route to taxonomy.addressSection.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.addressSectionCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.addressSections/:id', function() {
    it('should route to taxonomy.addressSection.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.addressSectionCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.addressSections/:id', function() {
    it('should route to taxonomy.addressSection.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.addressSectionCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
