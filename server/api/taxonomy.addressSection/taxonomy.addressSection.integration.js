'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyAddressSection;

describe('TaxonomyAddressSection API:', function() {
  describe('GET /api/taxonomy.addressSections', function() {
    var taxonomy.addressSections;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.addressSections')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.addressSections = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.addressSections).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.addressSections', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.addressSections')
        .send({
          name: 'New TaxonomyAddressSection',
          info: 'This is the brand new taxonomy.addressSection!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyAddressSection = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.addressSection', function() {
      expect(newTaxonomyAddressSection.name).to.equal('New TaxonomyAddressSection');
      expect(newTaxonomyAddressSection.info).to.equal('This is the brand new taxonomy.addressSection!!!');
    });
  });

  describe('GET /api/taxonomy.addressSections/:id', function() {
    var taxonomy.addressSection;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.addressSections/${newTaxonomyAddressSection._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.addressSection = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.addressSection = {};
    });

    it('should respond with the requested taxonomy.addressSection', function() {
      expect(taxonomy.addressSection.name).to.equal('New TaxonomyAddressSection');
      expect(taxonomy.addressSection.info).to.equal('This is the brand new taxonomy.addressSection!!!');
    });
  });

  describe('PUT /api/taxonomy.addressSections/:id', function() {
    var updatedTaxonomyAddressSection;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.addressSections/${newTaxonomyAddressSection._id}`)
        .send({
          name: 'Updated TaxonomyAddressSection',
          info: 'This is the updated taxonomy.addressSection!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyAddressSection = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyAddressSection = {};
    });

    it('should respond with the updated taxonomy.addressSection', function() {
      expect(updatedTaxonomyAddressSection.name).to.equal('Updated TaxonomyAddressSection');
      expect(updatedTaxonomyAddressSection.info).to.equal('This is the updated taxonomy.addressSection!!!');
    });

    it('should respond with the updated taxonomy.addressSection on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.addressSections/${newTaxonomyAddressSection._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.addressSection = res.body;

          expect(taxonomy.addressSection.name).to.equal('Updated TaxonomyAddressSection');
          expect(taxonomy.addressSection.info).to.equal('This is the updated taxonomy.addressSection!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.addressSections/:id', function() {
    var patchedTaxonomyAddressSection;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.addressSections/${newTaxonomyAddressSection._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyAddressSection' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.addressSection!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyAddressSection = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyAddressSection = {};
    });

    it('should respond with the patched taxonomy.addressSection', function() {
      expect(patchedTaxonomyAddressSection.name).to.equal('Patched TaxonomyAddressSection');
      expect(patchedTaxonomyAddressSection.info).to.equal('This is the patched taxonomy.addressSection!!!');
    });
  });

  describe('DELETE /api/taxonomy.addressSections/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.addressSections/${newTaxonomyAddressSection._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.addressSection does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.addressSections/${newTaxonomyAddressSection._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
