'use strict';

let express = require('express');
let controller = require('./taxonomy.addressSection.controller');
import * as auth from '../../auth/auth.service';

let router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.get('/:id', auth.hasRole('admin'), controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
router.put('/:id', auth.hasRole('admin'), controller.upsert);
router.patch('/:id', auth.hasRole('admin'), controller.patch);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);

router.get('/listBySectionType/:sectionType', auth.hasRole('admin'), controller.indexByType);
router.get('/listByParentId/:parentId', auth.hasRole('admin'), controller.indexByParentId);

module.exports = router;
