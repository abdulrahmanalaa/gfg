'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './taxonomy.addressSection.events';

let TaxonomyAddressSectionSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  title: { type: String, required: true, unique: false, trim: true },
  parentId: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyAddressSection', required: false, default: null },
  sectionType: { type: String,
    required: [true, 'Section Type is required'],
    trim: true,
    enum: ['governorate', 'area', 'block'],
    default: 'governorate'
  }
});

registerEvents(TaxonomyAddressSectionSchema);
export default mongoose.model('TaxonomyAddressSection', TaxonomyAddressSectionSchema);
