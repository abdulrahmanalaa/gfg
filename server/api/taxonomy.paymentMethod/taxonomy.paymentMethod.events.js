/**
 * TaxonomyPaymentMethod model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyPaymentMethodEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyPaymentMethodEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyPaymentMethod) {
  for(var e in events) {
    let event = events[e];
    TaxonomyPaymentMethod.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyPaymentMethodEvents.emit(event + ':' + doc._id, doc);
    TaxonomyPaymentMethodEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyPaymentMethodEvents;
