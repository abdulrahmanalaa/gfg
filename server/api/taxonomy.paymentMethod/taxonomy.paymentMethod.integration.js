'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyPaymentMethod;

describe('TaxonomyPaymentMethod API:', function() {
  describe('GET /api/taxonomy.paymentMethods', function() {
    var taxonomy.paymentMethods;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.paymentMethods')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.paymentMethods = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.paymentMethods).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.paymentMethods', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.paymentMethods')
        .send({
          name: 'New TaxonomyPaymentMethod',
          info: 'This is the brand new taxonomy.paymentMethod!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyPaymentMethod = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.paymentMethod', function() {
      expect(newTaxonomyPaymentMethod.name).to.equal('New TaxonomyPaymentMethod');
      expect(newTaxonomyPaymentMethod.info).to.equal('This is the brand new taxonomy.paymentMethod!!!');
    });
  });

  describe('GET /api/taxonomy.paymentMethods/:id', function() {
    var taxonomy.paymentMethod;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.paymentMethods/${newTaxonomyPaymentMethod._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.paymentMethod = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.paymentMethod = {};
    });

    it('should respond with the requested taxonomy.paymentMethod', function() {
      expect(taxonomy.paymentMethod.name).to.equal('New TaxonomyPaymentMethod');
      expect(taxonomy.paymentMethod.info).to.equal('This is the brand new taxonomy.paymentMethod!!!');
    });
  });

  describe('PUT /api/taxonomy.paymentMethods/:id', function() {
    var updatedTaxonomyPaymentMethod;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.paymentMethods/${newTaxonomyPaymentMethod._id}`)
        .send({
          name: 'Updated TaxonomyPaymentMethod',
          info: 'This is the updated taxonomy.paymentMethod!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyPaymentMethod = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyPaymentMethod = {};
    });

    it('should respond with the updated taxonomy.paymentMethod', function() {
      expect(updatedTaxonomyPaymentMethod.name).to.equal('Updated TaxonomyPaymentMethod');
      expect(updatedTaxonomyPaymentMethod.info).to.equal('This is the updated taxonomy.paymentMethod!!!');
    });

    it('should respond with the updated taxonomy.paymentMethod on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.paymentMethods/${newTaxonomyPaymentMethod._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.paymentMethod = res.body;

          expect(taxonomy.paymentMethod.name).to.equal('Updated TaxonomyPaymentMethod');
          expect(taxonomy.paymentMethod.info).to.equal('This is the updated taxonomy.paymentMethod!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.paymentMethods/:id', function() {
    var patchedTaxonomyPaymentMethod;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.paymentMethods/${newTaxonomyPaymentMethod._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyPaymentMethod' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.paymentMethod!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyPaymentMethod = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyPaymentMethod = {};
    });

    it('should respond with the patched taxonomy.paymentMethod', function() {
      expect(patchedTaxonomyPaymentMethod.name).to.equal('Patched TaxonomyPaymentMethod');
      expect(patchedTaxonomyPaymentMethod.info).to.equal('This is the patched taxonomy.paymentMethod!!!');
    });
  });

  describe('DELETE /api/taxonomy.paymentMethods/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.paymentMethods/${newTaxonomyPaymentMethod._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.paymentMethod does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.paymentMethods/${newTaxonomyPaymentMethod._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
