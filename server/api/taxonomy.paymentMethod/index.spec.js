'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.paymentMethodCtrlStub = {
  index: 'taxonomy.paymentMethodCtrl.index',
  show: 'taxonomy.paymentMethodCtrl.show',
  create: 'taxonomy.paymentMethodCtrl.create',
  upsert: 'taxonomy.paymentMethodCtrl.upsert',
  patch: 'taxonomy.paymentMethodCtrl.patch',
  destroy: 'taxonomy.paymentMethodCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.paymentMethodIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.paymentMethod.controller': taxonomy.paymentMethodCtrlStub
});

describe('TaxonomyPaymentMethod API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.paymentMethodIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.paymentMethods', function() {
    it('should route to taxonomy.paymentMethod.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.paymentMethodCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.paymentMethods/:id', function() {
    it('should route to taxonomy.paymentMethod.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.paymentMethodCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.paymentMethods', function() {
    it('should route to taxonomy.paymentMethod.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.paymentMethodCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.paymentMethods/:id', function() {
    it('should route to taxonomy.paymentMethod.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.paymentMethodCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.paymentMethods/:id', function() {
    it('should route to taxonomy.paymentMethod.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.paymentMethodCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.paymentMethods/:id', function() {
    it('should route to taxonomy.paymentMethod.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.paymentMethodCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
