'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyAmendmentReason;

describe('TaxonomyAmendmentReason API:', function() {
  describe('GET /api/taxonomy.amendmentReasons', function() {
    var taxonomy.amendmentReasons;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.amendmentReasons')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.amendmentReasons = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.amendmentReasons).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.amendmentReasons', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.amendmentReasons')
        .send({
          name: 'New TaxonomyAmendmentReason',
          info: 'This is the brand new taxonomy.amendmentReason!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyAmendmentReason = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.amendmentReason', function() {
      expect(newTaxonomyAmendmentReason.name).to.equal('New TaxonomyAmendmentReason');
      expect(newTaxonomyAmendmentReason.info).to.equal('This is the brand new taxonomy.amendmentReason!!!');
    });
  });

  describe('GET /api/taxonomy.amendmentReasons/:id', function() {
    var taxonomy.amendmentReason;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.amendmentReasons/${newTaxonomyAmendmentReason._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.amendmentReason = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.amendmentReason = {};
    });

    it('should respond with the requested taxonomy.amendmentReason', function() {
      expect(taxonomy.amendmentReason.name).to.equal('New TaxonomyAmendmentReason');
      expect(taxonomy.amendmentReason.info).to.equal('This is the brand new taxonomy.amendmentReason!!!');
    });
  });

  describe('PUT /api/taxonomy.amendmentReasons/:id', function() {
    var updatedTaxonomyAmendmentReason;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.amendmentReasons/${newTaxonomyAmendmentReason._id}`)
        .send({
          name: 'Updated TaxonomyAmendmentReason',
          info: 'This is the updated taxonomy.amendmentReason!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyAmendmentReason = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyAmendmentReason = {};
    });

    it('should respond with the updated taxonomy.amendmentReason', function() {
      expect(updatedTaxonomyAmendmentReason.name).to.equal('Updated TaxonomyAmendmentReason');
      expect(updatedTaxonomyAmendmentReason.info).to.equal('This is the updated taxonomy.amendmentReason!!!');
    });

    it('should respond with the updated taxonomy.amendmentReason on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.amendmentReasons/${newTaxonomyAmendmentReason._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.amendmentReason = res.body;

          expect(taxonomy.amendmentReason.name).to.equal('Updated TaxonomyAmendmentReason');
          expect(taxonomy.amendmentReason.info).to.equal('This is the updated taxonomy.amendmentReason!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.amendmentReasons/:id', function() {
    var patchedTaxonomyAmendmentReason;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.amendmentReasons/${newTaxonomyAmendmentReason._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyAmendmentReason' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.amendmentReason!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyAmendmentReason = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyAmendmentReason = {};
    });

    it('should respond with the patched taxonomy.amendmentReason', function() {
      expect(patchedTaxonomyAmendmentReason.name).to.equal('Patched TaxonomyAmendmentReason');
      expect(patchedTaxonomyAmendmentReason.info).to.equal('This is the patched taxonomy.amendmentReason!!!');
    });
  });

  describe('DELETE /api/taxonomy.amendmentReasons/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.amendmentReasons/${newTaxonomyAmendmentReason._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.amendmentReason does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.amendmentReasons/${newTaxonomyAmendmentReason._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
