'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.amendmentReasonCtrlStub = {
  index: 'taxonomy.amendmentReasonCtrl.index',
  show: 'taxonomy.amendmentReasonCtrl.show',
  create: 'taxonomy.amendmentReasonCtrl.create',
  upsert: 'taxonomy.amendmentReasonCtrl.upsert',
  patch: 'taxonomy.amendmentReasonCtrl.patch',
  destroy: 'taxonomy.amendmentReasonCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.amendmentReasonIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.amendmentReason.controller': taxonomy.amendmentReasonCtrlStub
});

describe('TaxonomyAmendmentReason API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.amendmentReasonIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.amendmentReasons', function() {
    it('should route to taxonomy.amendmentReason.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.amendmentReasonCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.amendmentReasons/:id', function() {
    it('should route to taxonomy.amendmentReason.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.amendmentReasonCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.amendmentReasons', function() {
    it('should route to taxonomy.amendmentReason.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.amendmentReasonCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.amendmentReasons/:id', function() {
    it('should route to taxonomy.amendmentReason.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.amendmentReasonCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.amendmentReasons/:id', function() {
    it('should route to taxonomy.amendmentReason.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.amendmentReasonCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.amendmentReasons/:id', function() {
    it('should route to taxonomy.amendmentReason.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.amendmentReasonCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
