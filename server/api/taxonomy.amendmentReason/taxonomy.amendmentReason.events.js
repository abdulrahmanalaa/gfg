/**
 * TaxonomyAmendmentReason model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyAmendmentReasonEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyAmendmentReasonEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyAmendmentReason) {
  for(var e in events) {
    let event = events[e];
    TaxonomyAmendmentReason.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyAmendmentReasonEvents.emit(event + ':' + doc._id, doc);
    TaxonomyAmendmentReasonEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyAmendmentReasonEvents;
