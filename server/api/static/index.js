'use strict';

let express = require('express');
let controller = require('./static.controller');
import * as auth from '../../auth/auth.service';

let router = express.Router();

router.post('/file/upload', auth.hasRole('admin'), controller.fileUpload);
router.post('/file/delete', auth.hasRole('admin'), controller.fileDelete);

module.exports = router;
