'use strict';

const _ = require('lodash');
let crypto = require('crypto');
import {
  uploadFileToAws,
  deleteFileFromAws
} from '../../config/s3';


export function fileUpload(req, res) {

  let originalFileName = req.body.filename;
  let fileExt = originalFileName.substring(originalFileName.lastIndexOf('.'));
  let genFileName = crypto.randomBytes(12).toString('hex');
  let fileName = `${genFileName}${fileExt}`;
  let imageData = req.body.base64;

  uploadFileToAws(fileName, imageData, (data, error) => {
    if(data) {
      return res.status(200).json({ status: 1, uploadedFileName: fileName });
    }
    else {
      res.status(500).json({ status: 0, error });
    }
  });
}


export function fileDelete(req, res) {

  let fileName = req.body.filename;

  deleteFileFromAws(fileName, (data, error) => {
    if(data) {
      return res.status(200).json({ status: 1 });
    }
    else {
      res.status(500).json({ status: 0, error });
    }
  });
}
