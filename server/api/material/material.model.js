'use strict';

import mongoose from 'mongoose';
let mongooseAutoIncrement = require('mongoose-auto-increment');
import {registerEvents} from './material.events';

let MaterialSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  accountingId: { type: String, required: true, trim: true },
  materialId: { type: Number },
  title: { type: String, required: true, trim: true },
  imageUrl: { type: String, required: false, trim: true },
  // materialType: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyMaterialType', required: false },
  materialType: {
    type: String,
    default: undefined,
    required: false,
    enum: ['gas', 'cylinder']
  },
  materialClass1: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyMaterialClass', required: false },
  materialClass2: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyMaterialClass', required: false }
});

MaterialSchema.plugin(mongooseAutoIncrement.plugin, {
  model: 'Material',
  field: 'materialId',
  startAt: 1000,
  incrementBy: 1
});

registerEvents(MaterialSchema);
export default mongoose.model('Material', MaterialSchema);
