/**
 * TaxonomyCustomerGroup model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyCustomerGroupEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyCustomerGroupEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyCustomerGroup) {
  for(var e in events) {
    let event = events[e];
    TaxonomyCustomerGroup.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyCustomerGroupEvents.emit(event + ':' + doc._id, doc);
    TaxonomyCustomerGroupEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyCustomerGroupEvents;
