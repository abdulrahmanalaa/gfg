'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyCustomerGroup;

describe('TaxonomyCustomerGroup API:', function() {
  describe('GET /api/taxonomy.customerGroups', function() {
    var taxonomy.customerGroups;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.customerGroups')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.customerGroups = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.customerGroups).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.customerGroups', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.customerGroups')
        .send({
          name: 'New TaxonomyCustomerGroup',
          info: 'This is the brand new taxonomy.customerGroup!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyCustomerGroup = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.customerGroup', function() {
      expect(newTaxonomyCustomerGroup.name).to.equal('New TaxonomyCustomerGroup');
      expect(newTaxonomyCustomerGroup.info).to.equal('This is the brand new taxonomy.customerGroup!!!');
    });
  });

  describe('GET /api/taxonomy.customerGroups/:id', function() {
    var taxonomy.customerGroup;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.customerGroups/${newTaxonomyCustomerGroup._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.customerGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.customerGroup = {};
    });

    it('should respond with the requested taxonomy.customerGroup', function() {
      expect(taxonomy.customerGroup.name).to.equal('New TaxonomyCustomerGroup');
      expect(taxonomy.customerGroup.info).to.equal('This is the brand new taxonomy.customerGroup!!!');
    });
  });

  describe('PUT /api/taxonomy.customerGroups/:id', function() {
    var updatedTaxonomyCustomerGroup;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.customerGroups/${newTaxonomyCustomerGroup._id}`)
        .send({
          name: 'Updated TaxonomyCustomerGroup',
          info: 'This is the updated taxonomy.customerGroup!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyCustomerGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyCustomerGroup = {};
    });

    it('should respond with the updated taxonomy.customerGroup', function() {
      expect(updatedTaxonomyCustomerGroup.name).to.equal('Updated TaxonomyCustomerGroup');
      expect(updatedTaxonomyCustomerGroup.info).to.equal('This is the updated taxonomy.customerGroup!!!');
    });

    it('should respond with the updated taxonomy.customerGroup on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.customerGroups/${newTaxonomyCustomerGroup._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.customerGroup = res.body;

          expect(taxonomy.customerGroup.name).to.equal('Updated TaxonomyCustomerGroup');
          expect(taxonomy.customerGroup.info).to.equal('This is the updated taxonomy.customerGroup!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.customerGroups/:id', function() {
    var patchedTaxonomyCustomerGroup;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.customerGroups/${newTaxonomyCustomerGroup._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyCustomerGroup' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.customerGroup!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyCustomerGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyCustomerGroup = {};
    });

    it('should respond with the patched taxonomy.customerGroup', function() {
      expect(patchedTaxonomyCustomerGroup.name).to.equal('Patched TaxonomyCustomerGroup');
      expect(patchedTaxonomyCustomerGroup.info).to.equal('This is the patched taxonomy.customerGroup!!!');
    });
  });

  describe('DELETE /api/taxonomy.customerGroups/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.customerGroups/${newTaxonomyCustomerGroup._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.customerGroup does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.customerGroups/${newTaxonomyCustomerGroup._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
