'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.customerGroupCtrlStub = {
  index: 'taxonomy.customerGroupCtrl.index',
  show: 'taxonomy.customerGroupCtrl.show',
  create: 'taxonomy.customerGroupCtrl.create',
  upsert: 'taxonomy.customerGroupCtrl.upsert',
  patch: 'taxonomy.customerGroupCtrl.patch',
  destroy: 'taxonomy.customerGroupCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.customerGroupIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.customerGroup.controller': taxonomy.customerGroupCtrlStub
});

describe('TaxonomyCustomerGroup API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.customerGroupIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.customerGroups', function() {
    it('should route to taxonomy.customerGroup.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.customerGroupCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.customerGroups/:id', function() {
    it('should route to taxonomy.customerGroup.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.customerGroupCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.customerGroups', function() {
    it('should route to taxonomy.customerGroup.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.customerGroupCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.customerGroups/:id', function() {
    it('should route to taxonomy.customerGroup.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.customerGroupCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.customerGroups/:id', function() {
    it('should route to taxonomy.customerGroup.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.customerGroupCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.customerGroups/:id', function() {
    it('should route to taxonomy.customerGroup.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.customerGroupCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
