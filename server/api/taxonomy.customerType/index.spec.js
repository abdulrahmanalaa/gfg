'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.customerTypeCtrlStub = {
  index: 'taxonomy.customerTypeCtrl.index',
  show: 'taxonomy.customerTypeCtrl.show',
  create: 'taxonomy.customerTypeCtrl.create',
  upsert: 'taxonomy.customerTypeCtrl.upsert',
  patch: 'taxonomy.customerTypeCtrl.patch',
  destroy: 'taxonomy.customerTypeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.customerTypeIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.customerType.controller': taxonomy.customerTypeCtrlStub
});

describe('TaxonomyCustomerType API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.customerTypeIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.customerTypes', function() {
    it('should route to taxonomy.customerType.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.customerTypeCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.customerTypes/:id', function() {
    it('should route to taxonomy.customerType.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.customerTypeCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.customerTypes', function() {
    it('should route to taxonomy.customerType.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.customerTypeCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.customerTypes/:id', function() {
    it('should route to taxonomy.customerType.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.customerTypeCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.customerTypes/:id', function() {
    it('should route to taxonomy.customerType.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.customerTypeCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.customerTypes/:id', function() {
    it('should route to taxonomy.customerType.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.customerTypeCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
