'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyCustomerType;

describe('TaxonomyCustomerType API:', function() {
  describe('GET /api/taxonomy.customerTypes', function() {
    var taxonomy.customerTypes;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.customerTypes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.customerTypes = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.customerTypes).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.customerTypes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.customerTypes')
        .send({
          name: 'New TaxonomyCustomerType',
          info: 'This is the brand new taxonomy.customerType!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyCustomerType = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.customerType', function() {
      expect(newTaxonomyCustomerType.name).to.equal('New TaxonomyCustomerType');
      expect(newTaxonomyCustomerType.info).to.equal('This is the brand new taxonomy.customerType!!!');
    });
  });

  describe('GET /api/taxonomy.customerTypes/:id', function() {
    var taxonomy.customerType;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.customerTypes/${newTaxonomyCustomerType._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.customerType = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.customerType = {};
    });

    it('should respond with the requested taxonomy.customerType', function() {
      expect(taxonomy.customerType.name).to.equal('New TaxonomyCustomerType');
      expect(taxonomy.customerType.info).to.equal('This is the brand new taxonomy.customerType!!!');
    });
  });

  describe('PUT /api/taxonomy.customerTypes/:id', function() {
    var updatedTaxonomyCustomerType;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.customerTypes/${newTaxonomyCustomerType._id}`)
        .send({
          name: 'Updated TaxonomyCustomerType',
          info: 'This is the updated taxonomy.customerType!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyCustomerType = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyCustomerType = {};
    });

    it('should respond with the updated taxonomy.customerType', function() {
      expect(updatedTaxonomyCustomerType.name).to.equal('Updated TaxonomyCustomerType');
      expect(updatedTaxonomyCustomerType.info).to.equal('This is the updated taxonomy.customerType!!!');
    });

    it('should respond with the updated taxonomy.customerType on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.customerTypes/${newTaxonomyCustomerType._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.customerType = res.body;

          expect(taxonomy.customerType.name).to.equal('Updated TaxonomyCustomerType');
          expect(taxonomy.customerType.info).to.equal('This is the updated taxonomy.customerType!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.customerTypes/:id', function() {
    var patchedTaxonomyCustomerType;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.customerTypes/${newTaxonomyCustomerType._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyCustomerType' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.customerType!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyCustomerType = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyCustomerType = {};
    });

    it('should respond with the patched taxonomy.customerType', function() {
      expect(patchedTaxonomyCustomerType.name).to.equal('Patched TaxonomyCustomerType');
      expect(patchedTaxonomyCustomerType.info).to.equal('This is the patched taxonomy.customerType!!!');
    });
  });

  describe('DELETE /api/taxonomy.customerTypes/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.customerTypes/${newTaxonomyCustomerType._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.customerType does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.customerTypes/${newTaxonomyCustomerType._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
