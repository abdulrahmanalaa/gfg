/**
 * TaxonomyCustomerType model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyCustomerTypeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyCustomerTypeEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyCustomerType) {
  for(var e in events) {
    let event = events[e];
    TaxonomyCustomerType.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyCustomerTypeEvents.emit(event + ':' + doc._id, doc);
    TaxonomyCustomerTypeEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyCustomerTypeEvents;
