'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var purchaseOrderCtrlStub = {
  index: 'purchaseOrderCtrl.index',
  show: 'purchaseOrderCtrl.show',
  create: 'purchaseOrderCtrl.create',
  upsert: 'purchaseOrderCtrl.upsert',
  patch: 'purchaseOrderCtrl.patch',
  destroy: 'purchaseOrderCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var purchaseOrderIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './purchaseOrder.controller': purchaseOrderCtrlStub
});

describe('PurchaseOrder API Router:', function() {
  it('should return an express router instance', function() {
    expect(purchaseOrderIndex).to.equal(routerStub);
  });

  describe('GET /api/purchaseOrders', function() {
    it('should route to purchaseOrder.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'purchaseOrderCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/purchaseOrders/:id', function() {
    it('should route to purchaseOrder.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'purchaseOrderCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/purchaseOrders', function() {
    it('should route to purchaseOrder.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'purchaseOrderCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/purchaseOrders/:id', function() {
    it('should route to purchaseOrder.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'purchaseOrderCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/purchaseOrders/:id', function() {
    it('should route to purchaseOrder.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'purchaseOrderCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/purchaseOrders/:id', function() {
    it('should route to purchaseOrder.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'purchaseOrderCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
