/**
 * PurchaseOrder model events
 */

'use strict';

import {EventEmitter} from 'events';
var PurchaseOrderEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PurchaseOrderEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(PurchaseOrder) {
  for(var e in events) {
    let event = events[e];
    PurchaseOrder.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    PurchaseOrderEvents.emit(event + ':' + doc._id, doc);
    PurchaseOrderEvents.emit(event, doc);
  };
}

export {registerEvents};
export default PurchaseOrderEvents;
