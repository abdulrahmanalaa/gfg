'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newPurchaseOrder;

describe('PurchaseOrder API:', function() {
  describe('GET /api/purchaseOrders', function() {
    var purchaseOrders;

    beforeEach(function(done) {
      request(app)
        .get('/api/purchaseOrders')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          purchaseOrders = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(purchaseOrders).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/purchaseOrders', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/purchaseOrders')
        .send({
          name: 'New PurchaseOrder',
          info: 'This is the brand new purchaseOrder!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newPurchaseOrder = res.body;
          done();
        });
    });

    it('should respond with the newly created purchaseOrder', function() {
      expect(newPurchaseOrder.name).to.equal('New PurchaseOrder');
      expect(newPurchaseOrder.info).to.equal('This is the brand new purchaseOrder!!!');
    });
  });

  describe('GET /api/purchaseOrders/:id', function() {
    var purchaseOrder;

    beforeEach(function(done) {
      request(app)
        .get(`/api/purchaseOrders/${newPurchaseOrder._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          purchaseOrder = res.body;
          done();
        });
    });

    afterEach(function() {
      purchaseOrder = {};
    });

    it('should respond with the requested purchaseOrder', function() {
      expect(purchaseOrder.name).to.equal('New PurchaseOrder');
      expect(purchaseOrder.info).to.equal('This is the brand new purchaseOrder!!!');
    });
  });

  describe('PUT /api/purchaseOrders/:id', function() {
    var updatedPurchaseOrder;

    beforeEach(function(done) {
      request(app)
        .put(`/api/purchaseOrders/${newPurchaseOrder._id}`)
        .send({
          name: 'Updated PurchaseOrder',
          info: 'This is the updated purchaseOrder!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedPurchaseOrder = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPurchaseOrder = {};
    });

    it('should respond with the updated purchaseOrder', function() {
      expect(updatedPurchaseOrder.name).to.equal('Updated PurchaseOrder');
      expect(updatedPurchaseOrder.info).to.equal('This is the updated purchaseOrder!!!');
    });

    it('should respond with the updated purchaseOrder on a subsequent GET', function(done) {
      request(app)
        .get(`/api/purchaseOrders/${newPurchaseOrder._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let purchaseOrder = res.body;

          expect(purchaseOrder.name).to.equal('Updated PurchaseOrder');
          expect(purchaseOrder.info).to.equal('This is the updated purchaseOrder!!!');

          done();
        });
    });
  });

  describe('PATCH /api/purchaseOrders/:id', function() {
    var patchedPurchaseOrder;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/purchaseOrders/${newPurchaseOrder._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched PurchaseOrder' },
          { op: 'replace', path: '/info', value: 'This is the patched purchaseOrder!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedPurchaseOrder = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedPurchaseOrder = {};
    });

    it('should respond with the patched purchaseOrder', function() {
      expect(patchedPurchaseOrder.name).to.equal('Patched PurchaseOrder');
      expect(patchedPurchaseOrder.info).to.equal('This is the patched purchaseOrder!!!');
    });
  });

  describe('DELETE /api/purchaseOrders/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/purchaseOrders/${newPurchaseOrder._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when purchaseOrder does not exist', function(done) {
      request(app)
        .delete(`/api/purchaseOrders/${newPurchaseOrder._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
