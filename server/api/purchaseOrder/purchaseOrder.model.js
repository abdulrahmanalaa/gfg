'use strict';

import mongoose from 'mongoose';
let mongooseAutoIncrement = require('mongoose-auto-increment');
let mongooseDeepPopulate = require('mongoose-deep-populate')(mongoose);
import {registerEvents} from './purchaseOrder.events';

let PurchaseOrderSchema = new mongoose.Schema({
  uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  created: { type: Date, default: Date.now },
  changed: { type: Date, default: Date.now },

  status: { type: Boolean, required: true, Default: true },
  orderNumber: { type: Number },
  orderStatus: {
    type: String,
    Default: null,
    enum: ['OPEN', 'ASSIGNED', 'STARTED', 'COMPLETED', 'CANCELLED']
  },
  orderCancellationReason: { type: mongoose.Schema.Types.ObjectId, ref: 'TaxonomyOrderCancelReason', default: null, required: false },
  notes: { type: String, trim: true },
  storageIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Storage', default: null, required: false },
  supplierIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Supplier', required: true },
  orderCostTotal: { type: String, required: false },
  materialInfo: [
    {
      materialIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Material', required: false },
      cost: { type: String, required: false },
      costTotal: { type: String, required: false },
      quantity: { type: Number, required: false },
      additionalCostPerItem: { type: String, required: false },
      additionalLumpSumCost: { type: String, required: false }
    }
  ],
  materialReturnedSpares: [
    {
      materialIdRef: { type: mongoose.Schema.Types.ObjectId, ref: 'Material', required: false },
      returnedSpares: { type: Number, required: false },
      returnedSparesActual: { type: Number, required: false }
    }
  ]
});

PurchaseOrderSchema.plugin(mongooseAutoIncrement.plugin, {
  model: 'PurchaseOrder',
  field: 'orderNumber',
  startAt: 30000000,
  incrementBy: 1
});

PurchaseOrderSchema.plugin(mongooseDeepPopulate, {
  populate: {
    'storageIdRef.currentLeader': {
      select: 'name plateNumber'
    },
    'storageIdRef.currentDispatcher': {
      select: 'name plateNumber'
    },
    'supplierIdRef.supplierGroup': {
      select: 'title'
    },
    'supplierIdRef.address.governorate': {
      select: 'title'
    },
    'supplierIdRef.address.area': {
      select: 'title'
    },
    'supplierIdRef.address.block': {
      select: 'title'
    },
  }
});

registerEvents(PurchaseOrderSchema);
export default mongoose.model('PurchaseOrder', PurchaseOrderSchema);
