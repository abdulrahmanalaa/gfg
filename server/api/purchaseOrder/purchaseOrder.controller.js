/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/purchaseOrders              ->  index
 * POST    /api/purchaseOrders              ->  create
 * GET     /api/purchaseOrders/:id          ->  show
 * PUT     /api/purchaseOrders/:id          ->  upsert
 * PATCH   /api/purchaseOrders/:id          ->  patch
 * DELETE  /api/purchaseOrders/:id          ->  destroy
 */

'use strict';

const _ = require('lodash');

import jsonpatch from 'fast-json-patch';
import PurchaseOrder from './purchaseOrder.model';
import Storage from '../storage/storage.model';

import * as customUtils from '../../utils';

const uidFieldsToPopulate = 'name email';
const storageFieldsToPopulate = 'title plateNumber status isAvailable currentLeader currentDispatcher';
const leaderFieldsToPopulate = 'status name email plateNumber phoneNumber1';
const supplierFieldsToPopulate = 'name email';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of PurchaseOrders
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  return PurchaseOrder
    .find(queryConditions)
    .select(queryFields)
    .sort(querySort)
    .populate({ path: 'storageIdRef', select: storageFieldsToPopulate })
    .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
    .populate({ path: 'orderCancellationReason', select: 'title' })
    .deepPopulate(''
      + ' storageIdRef.currentLeader'
      + ' storageIdRef.currentDispatcher'
    )
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single PurchaseOrder from the DB
export function show(req, res) {
  return PurchaseOrder
    .findById(req.params.id)
    .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function indexGroupedByField(req, res) {
  let leaderUid = req.user._id;

  Storage
    .find({ currentLeader: leaderUid })
    .select('_id')
    .exec()
    .then(function(data) {
      let storageIds = _.map(data, '_id');
      if(storageIds.length === 0) {
        res
          .status(403)
          .json({ error: 'User has no assigned storage.' })
          .end();
      }

      PurchaseOrder
        .find({
          storageIdRef: { $in: storageIds },
        })
        .populate({ path: 'uid', select: uidFieldsToPopulate })
        .populate({ path: 'storageIdRef', select: storageFieldsToPopulate })
        .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
        .populate({ path: 'orderCancellationReason', select: 'title' })
        .deepPopulate(''
          + ' customerIdRef.customerType'
          + ' customerIdRef.address.governorate'
          + ' customerIdRef.address.area'
          + ' customerIdRef.address.block'
        )
        //.sort('-created')
        .exec()
        .then(function(data) {
          let output = {};
          let field = req.params.field;

          _.forEach(data, (itm, idx) => {
            if(!output[itm[field]]) {
              output[itm[field]] = [];
            }
            output[itm[field]].push(itm);
          });

          res
            .status(200)
            .json(output)
            .end();
        })
        .catch(handleError(res));
    })
    .catch(handleError(res));

  // return PurchaseOrder
  //   .find({
  //     leaderIdRef: uid
  //   })
  //   .populate({ path: 'uid', select: uidFieldsToPopulate })
  //   .populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
  //   .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
  //   .populate({ path: 'orderCancellationReason', select: 'title' })
  //   .deepPopulate(''
  //     + ' customerIdRef.customerType'
  //     + ' customerIdRef.address.governorate'
  //     + ' customerIdRef.address.area'
  //     + ' customerIdRef.address.block'
  //   )
  //   //.sort('-created')
  //   .exec()
  //   .then(function(data) {
  //     let output = {};
  //     let field = req.params.field;
  //
  //     _.forEach(data, (itm, idx) => {
  //       if(!output[itm[field]]) {
  //         output[itm[field]] = [];
  //       }
  //       output[itm[field]].push(itm);
  //     });
  //
  //     res
  //       .status(200)
  //       .json(output)
  //       .end();
  //   })
  //   .catch(handleError(res));
}

// Creates a new PurchaseOrder in the DB
export function create(req, res) {
  // attach uid to created document
  req.body.uid = req.user._id;

  return PurchaseOrder
    .create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given PurchaseOrder in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }

  req.body.changed = Date.now();

  return PurchaseOrder.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true})
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing PurchaseOrder in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return PurchaseOrder.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a PurchaseOrder from the DB
export function destroy(req, res) {
  return PurchaseOrder.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function myOrders(req, res) {
  let leaderUid = req.user._id;

  Storage
    .find({ currentLeader: leaderUid })
    .select('_id')
    .exec()
    .then(function(data) {
      let storageIds = _.map(data, '_id');
      if(storageIds.length === 0) {
        res
          .status(403)
          .json({ error: 'User has no assigned storage.' })
          .end();
      }

      PurchaseOrder
        .find({
          storageIdRef: { $in: storageIds },
          orderStatus: { $nin: ['COMPLETED', 'CANCELLED'] }
        })
       .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
        .sort('-created')
        .exec()
        .then(respondWithResult(res))
        .catch(handleError(res));
    })
    .catch(handleError(res));

  // let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);
  //
  // queryConditions = queryConditions || {};
  // queryConditions.leaderIdRef = leaderUid;
  //
  // return PurchaseOrder
  //   .find(queryConditions)
  //   .sort(querySort)
  //   .limit(queryLimit)
  //   .skip(querySkip)
  //   //.populate({ path: 'uid', select: uidFieldsToPopulate })
  //   //.populate({ path: 'leaderIdRef', select: leaderFieldsToPopulate })
  //   .populate({ path: 'supplierIdRef', select: supplierFieldsToPopulate })
  //   .exec()
  //   .then(respondWithResult(res))
  //   .catch(handleError(res));
}


export function resetOrderNumberCount(req, res) {
  return PurchaseOrder
    .findOne({})
    .exec()
    .then(handleEntityNotFound(res))
    .then(function(data) {
      data.nextCount(function(error, count) {
        data.resetCount(function(err, nextCount) {
          if(err) {
            return res.status(500).json({ status: false, error: err });
          }
          else {
            return res.status(200).json({ status: true, nextCount });
          }
        });
      });
    })
    .catch(handleError(res));
}
