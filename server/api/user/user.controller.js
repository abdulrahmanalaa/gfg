'use strict';

const _ = require('lodash');

import User from './user.model';
import config from '../../config/environment';
import jwt from 'jsonwebtoken';

import * as customUtils from '../../utils';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    return res.status(statusCode).send(err);
  };
}

/**
 * Get list of users
 * restriction: 'admin'
 */
export function index(req, res) {
  let { queryFields, queryLimit, querySkip, querySort, queryConditions } = customUtils.applyQueryParams(req);

  if(_.isEmpty(queryFields)) {
    queryFields = '-salt -password';
  }

  return User
    .find(queryConditions)
    .select(queryFields)
    // .select(`${queryFields} -salt -password`)
    .sort(querySort)
    .exec()
    .then(users => {
      res.status(200).json(users);
    })
    .catch(handleError(res));

  // return User.find({}, '-salt -password').exec()
  //   .then(users => {
  //     res.status(200).json(users);
  //   })
  //   .catch(handleError(res));
}

/**
 * Creates a new user
 */
export function create(req, res) {
  let newUser = req.body;
  newUser.provider = 'local';

  return User.create(newUser)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
  // let newUser = new User(req.body);
  // newUser.provider = 'local';
  // newUser.save()
  //   .then(function(user) {
  //     let token = jwt.sign({ _id: user._id }, config.secrets.session, {
  //       expiresIn: 60 * 60 * 5
  //     });
  //     res.json({ token });
  //   })
  //   .catch(validationError(res));
}

/**
 * Get a single user
 */
export function show(req, res, next) {
  let userId = req.params.id;

  return User.findById(userId)
    .exec()
    .then(user => {
      if(!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
export function destroy(req, res) {
  return User
    .findById(req.params.id)
    .exec()
    .then(function(data) {
      if(data.role === 'admin') {
        res.status(501)
          .json({ error: 'Can not delete administrator users.' })
          .end();
      }
      else {
        return User
          .findByIdAndRemove(req.params.id)
          .exec()
          .then(function() {
            res.status(204).end();
          })
          .catch(handleError(res));
      }
    })
    .catch(handleError(res));
}

/**
 * Change a users password
 */
export function changePassword(req, res) {
  let userId = req.user._id;
  let oldPass = String(req.body.oldPassword);
  let newPass = String(req.body.newPassword);

  return User.findById(userId).exec()
    .then(user => {
      if(user.authenticate(oldPass)) {
        user.password = newPass;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * Get my info
 */
export function me(req, res, next) {
  let userId = req.user._id;

  return User.findOne({ _id: userId }, '-salt -password').exec()
    .then(user => { // don't ever give out the password or salt
      if(!user) {
        return res.status(401).end();
      }
      res.json(user);
    })
    .catch(err => next(err));
}

/**
 * Authentication callback
 */
export function authCallback(req, res) {
  res.redirect('/');
}

// Upserts the given User in the DB at the specified ID
export function upsert(req, res) {
  let userObj = req.body;

  if(userObj._id) {
    Reflect.deleteProperty(userObj, '_id');
  }
  if(userObj.provider) {
    Reflect.deleteProperty(userObj, 'provider');
  }
  if(userObj.password) {
    Reflect.deleteProperty(userObj, 'password');
  }

  return User.findOneAndUpdate(
      { _id: req.params.id },
      userObj,
      { new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true }
    )
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


export function upsertMyProfile(req, res) {
  let userId = req.user._id;
  let userObj = req.body;

  if(userObj._id) {
    Reflect.deleteProperty(userObj, '_id');
  }
  if(userObj.provider) {
    Reflect.deleteProperty(userObj, 'provider');
  }
  if(userObj.password) {
    Reflect.deleteProperty(userObj, 'password');
  }

  return User.findOneAndUpdate(
    { _id: userId },
    userObj,
    { new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true }
  )
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


/**
 * Change a users password
 */
export function adminChangePassword(req, res) {
  let userId = String(req.body.userId);
  let newPass = String(req.body.newPassword);

  return User.findById(userId)
    .exec()
    .then(user => {
      user.password = newPass;
      return user.save()
        .then(() => {
          res.status(204).end();
        })
        .catch(validationError(res));
    });
}
