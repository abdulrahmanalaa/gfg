'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.materialClassCtrlStub = {
  index: 'taxonomy.materialClassCtrl.index',
  show: 'taxonomy.materialClassCtrl.show',
  create: 'taxonomy.materialClassCtrl.create',
  upsert: 'taxonomy.materialClassCtrl.upsert',
  patch: 'taxonomy.materialClassCtrl.patch',
  destroy: 'taxonomy.materialClassCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.materialClassIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.materialClass.controller': taxonomy.materialClassCtrlStub
});

describe('TaxonomyMaterialClass API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.materialClassIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.materialClasses', function() {
    it('should route to taxonomy.materialClass.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.materialClassCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.materialClasses/:id', function() {
    it('should route to taxonomy.materialClass.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.materialClassCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.materialClasses', function() {
    it('should route to taxonomy.materialClass.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.materialClassCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.materialClasses/:id', function() {
    it('should route to taxonomy.materialClass.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.materialClassCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.materialClasses/:id', function() {
    it('should route to taxonomy.materialClass.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.materialClassCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.materialClasses/:id', function() {
    it('should route to taxonomy.materialClass.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.materialClassCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
