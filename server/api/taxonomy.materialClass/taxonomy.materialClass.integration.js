'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyMaterialClass;

describe('TaxonomyMaterialClass API:', function() {
  describe('GET /api/taxonomy.materialClasses', function() {
    var taxonomy.materialClasss;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.materialClasses')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.materialClasss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.materialClasss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.materialClasses', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.materialClasses')
        .send({
          name: 'New TaxonomyMaterialClass',
          info: 'This is the brand new taxonomy.materialClass!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyMaterialClass = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.materialClass', function() {
      expect(newTaxonomyMaterialClass.name).to.equal('New TaxonomyMaterialClass');
      expect(newTaxonomyMaterialClass.info).to.equal('This is the brand new taxonomy.materialClass!!!');
    });
  });

  describe('GET /api/taxonomy.materialClasses/:id', function() {
    var taxonomy.materialClass;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.materialClasses/${newTaxonomyMaterialClass._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.materialClass = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.materialClass = {};
    });

    it('should respond with the requested taxonomy.materialClass', function() {
      expect(taxonomy.materialClass.name).to.equal('New TaxonomyMaterialClass');
      expect(taxonomy.materialClass.info).to.equal('This is the brand new taxonomy.materialClass!!!');
    });
  });

  describe('PUT /api/taxonomy.materialClasses/:id', function() {
    var updatedTaxonomyMaterialClass;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.materialClasses/${newTaxonomyMaterialClass._id}`)
        .send({
          name: 'Updated TaxonomyMaterialClass',
          info: 'This is the updated taxonomy.materialClass!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyMaterialClass = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyMaterialClass = {};
    });

    it('should respond with the updated taxonomy.materialClass', function() {
      expect(updatedTaxonomyMaterialClass.name).to.equal('Updated TaxonomyMaterialClass');
      expect(updatedTaxonomyMaterialClass.info).to.equal('This is the updated taxonomy.materialClass!!!');
    });

    it('should respond with the updated taxonomy.materialClass on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.materialClasses/${newTaxonomyMaterialClass._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.materialClass = res.body;

          expect(taxonomy.materialClass.name).to.equal('Updated TaxonomyMaterialClass');
          expect(taxonomy.materialClass.info).to.equal('This is the updated taxonomy.materialClass!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.materialClasses/:id', function() {
    var patchedTaxonomyMaterialClass;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.materialClasses/${newTaxonomyMaterialClass._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyMaterialClass' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.materialClass!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyMaterialClass = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyMaterialClass = {};
    });

    it('should respond with the patched taxonomy.materialClass', function() {
      expect(patchedTaxonomyMaterialClass.name).to.equal('Patched TaxonomyMaterialClass');
      expect(patchedTaxonomyMaterialClass.info).to.equal('This is the patched taxonomy.materialClass!!!');
    });
  });

  describe('DELETE /api/taxonomy.materialClasses/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.materialClasses/${newTaxonomyMaterialClass._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.materialClass does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.materialClasses/${newTaxonomyMaterialClass._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
