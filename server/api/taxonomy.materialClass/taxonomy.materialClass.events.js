/**
 * TaxonomyMaterialClass model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyMaterialClassEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyMaterialClassEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyMaterialClass) {
  for(var e in events) {
    let event = events[e];
    TaxonomyMaterialClass.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyMaterialClassEvents.emit(event + ':' + doc._id, doc);
    TaxonomyMaterialClassEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyMaterialClassEvents;
