'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTaxonomyStorageType;

describe('TaxonomyStorageType API:', function() {
  describe('GET /api/taxonomy.storageTypes', function() {
    var taxonomy.storageTypes;

    beforeEach(function(done) {
      request(app)
        .get('/api/taxonomy.storageTypes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.storageTypes = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(taxonomy.storageTypes).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/taxonomy.storageTypes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/taxonomy.storageTypes')
        .send({
          name: 'New TaxonomyStorageType',
          info: 'This is the brand new taxonomy.storageType!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTaxonomyStorageType = res.body;
          done();
        });
    });

    it('should respond with the newly created taxonomy.storageType', function() {
      expect(newTaxonomyStorageType.name).to.equal('New TaxonomyStorageType');
      expect(newTaxonomyStorageType.info).to.equal('This is the brand new taxonomy.storageType!!!');
    });
  });

  describe('GET /api/taxonomy.storageTypes/:id', function() {
    var taxonomy.storageType;

    beforeEach(function(done) {
      request(app)
        .get(`/api/taxonomy.storageTypes/${newTaxonomyStorageType._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          taxonomy.storageType = res.body;
          done();
        });
    });

    afterEach(function() {
      taxonomy.storageType = {};
    });

    it('should respond with the requested taxonomy.storageType', function() {
      expect(taxonomy.storageType.name).to.equal('New TaxonomyStorageType');
      expect(taxonomy.storageType.info).to.equal('This is the brand new taxonomy.storageType!!!');
    });
  });

  describe('PUT /api/taxonomy.storageTypes/:id', function() {
    var updatedTaxonomyStorageType;

    beforeEach(function(done) {
      request(app)
        .put(`/api/taxonomy.storageTypes/${newTaxonomyStorageType._id}`)
        .send({
          name: 'Updated TaxonomyStorageType',
          info: 'This is the updated taxonomy.storageType!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTaxonomyStorageType = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTaxonomyStorageType = {};
    });

    it('should respond with the updated taxonomy.storageType', function() {
      expect(updatedTaxonomyStorageType.name).to.equal('Updated TaxonomyStorageType');
      expect(updatedTaxonomyStorageType.info).to.equal('This is the updated taxonomy.storageType!!!');
    });

    it('should respond with the updated taxonomy.storageType on a subsequent GET', function(done) {
      request(app)
        .get(`/api/taxonomy.storageTypes/${newTaxonomyStorageType._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let taxonomy.storageType = res.body;

          expect(taxonomy.storageType.name).to.equal('Updated TaxonomyStorageType');
          expect(taxonomy.storageType.info).to.equal('This is the updated taxonomy.storageType!!!');

          done();
        });
    });
  });

  describe('PATCH /api/taxonomy.storageTypes/:id', function() {
    var patchedTaxonomyStorageType;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/taxonomy.storageTypes/${newTaxonomyStorageType._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched TaxonomyStorageType' },
          { op: 'replace', path: '/info', value: 'This is the patched taxonomy.storageType!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTaxonomyStorageType = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTaxonomyStorageType = {};
    });

    it('should respond with the patched taxonomy.storageType', function() {
      expect(patchedTaxonomyStorageType.name).to.equal('Patched TaxonomyStorageType');
      expect(patchedTaxonomyStorageType.info).to.equal('This is the patched taxonomy.storageType!!!');
    });
  });

  describe('DELETE /api/taxonomy.storageTypes/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/taxonomy.storageTypes/${newTaxonomyStorageType._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when taxonomy.storageType does not exist', function(done) {
      request(app)
        .delete(`/api/taxonomy.storageTypes/${newTaxonomyStorageType._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
