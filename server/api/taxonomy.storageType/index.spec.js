'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var taxonomy.storageTypeCtrlStub = {
  index: 'taxonomy.storageTypeCtrl.index',
  show: 'taxonomy.storageTypeCtrl.show',
  create: 'taxonomy.storageTypeCtrl.create',
  upsert: 'taxonomy.storageTypeCtrl.upsert',
  patch: 'taxonomy.storageTypeCtrl.patch',
  destroy: 'taxonomy.storageTypeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var taxonomy.storageTypeIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './taxonomy.storageType.controller': taxonomy.storageTypeCtrlStub
});

describe('TaxonomyStorageType API Router:', function() {
  it('should return an express router instance', function() {
    expect(taxonomy.storageTypeIndex).to.equal(routerStub);
  });

  describe('GET /api/taxonomy.storageTypes', function() {
    it('should route to taxonomy.storageType.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'taxonomy.storageTypeCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/taxonomy.storageTypes/:id', function() {
    it('should route to taxonomy.storageType.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'taxonomy.storageTypeCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/taxonomy.storageTypes', function() {
    it('should route to taxonomy.storageType.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'taxonomy.storageTypeCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/taxonomy.storageTypes/:id', function() {
    it('should route to taxonomy.storageType.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'taxonomy.storageTypeCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/taxonomy.storageTypes/:id', function() {
    it('should route to taxonomy.storageType.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'taxonomy.storageTypeCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/taxonomy.storageTypes/:id', function() {
    it('should route to taxonomy.storageType.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'taxonomy.storageTypeCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
