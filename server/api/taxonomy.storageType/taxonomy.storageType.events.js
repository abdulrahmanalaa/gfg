/**
 * TaxonomyStorageType model events
 */

'use strict';

import {EventEmitter} from 'events';
var TaxonomyStorageTypeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TaxonomyStorageTypeEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(TaxonomyStorageType) {
  for(var e in events) {
    let event = events[e];
    TaxonomyStorageType.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TaxonomyStorageTypeEvents.emit(event + ':' + doc._id, doc);
    TaxonomyStorageTypeEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TaxonomyStorageTypeEvents;
