'use strict';

import Storage from '../api/storage/storage.model';

export function sanitizeCollectionId(_id) {
  let newId = _id
    .toLowerCase()
    .trim()
    .replace(/(<([^>]+)>)/ig, ' ') // strip html tags
    .replace(/[&\/\\#,+()$~%.'"\]\[:*?!^<>{}]/g, ' ') // remove special characters
    .replace(/_/g, '-')
    .replace(/ /g, '-')
    .replace(/(.)\1{2,}/g, '$1$1') // replace repetitive occurrence of all strings to xx
    .replace(/-+/g, '-') // remove repetitive dashes to single dash
    .replace(/-$/, '') // remove last character of string if dash
    .replace(/^-/, '') // remove first character of string if dash
  ;

  return newId;
}


export function applyQueryParams(req) {
  // select only some fields to return
  let queryFields = req.query.fields || '';
  queryFields = queryFields.replace(',', ' ');

  // apply query search
  let queryConditions = req.query.queryConditions || null;
  queryConditions = JSON.parse(queryConditions);

  // add pagination
  let queryLimit = req.query.limit ? parseInt(req.query.limit) : 99999;
  let queryPage = req.query.page ? Math.abs(parseInt(req.query.page)) : 0;
  let querySkip = queryPage * queryLimit;

  // add sorting
  let querySortDirection = req.query.direction || '';
  querySortDirection = (querySortDirection.toLowerCase() === 'desc') ? '-' : '';
  let querySort = req.query.sort ? req.query.sort : '';
  querySort = querySortDirection + querySort;

  return { queryFields, queryLimit, querySkip, querySort, queryConditions };
}


export function addStorageAssignmentHistoryItem(storageId, userId, userType, assignmentDate) {
  let obj = {};
  obj[userType] = userId;
  obj.fromDate = assignmentDate;

  Storage.findOneAndUpdate(
    {
      _id: storageId
    },
    {
      $push: {
        assignmentHistory: obj
      }
    },
    { new: true, upsert: false, setDefaultsOnInsert: true, runValidators: true }
  )
    .exec()
    .then();
}
