'use strict';

const _ = require('lodash');
let AWS_CREDENTIALS = require('./s3-credentials.js').default();
const AWS_BUCKET_NAME = AWS_CREDENTIALS.bucketName;

let AWS = require('aws-sdk');
let S3 = new AWS.S3({
  accessKeyId: AWS_CREDENTIALS.accessKeyId,
  secretAccessKey: AWS_CREDENTIALS.secretAccessKey,
  region: AWS_CREDENTIALS.region
});

export function uploadFileToAws(fileName, fileData, callback) {
  let params = {
    Bucket: AWS_BUCKET_NAME,
    ACL: 'public-read',
    Key: fileName,
    // Key: 'gogas-' + _.now().toString(),
    Body: new Buffer(fileData, 'base64'),
    ContentEncoding: 'base64',
    ContentType: 'image/jpeg'
  };

  S3.putObject(params, (error, data) => {
    if(error) {
      console.log(error, error.stack);
      return callback(null, error);
    }

    // Reflect.deleteProperty(params, 'Body');
    // Reflect.deleteProperty(params, 'ACL');

    return callback(data, null);
  });
}


export function deleteFileFromAws(fileName, callback) {
  let params = {
    Bucket: AWS_BUCKET_NAME,
    Key: fileName
    // Delete: {
    //   Objects: [
    //     { Key: fileName }
    //   ]
    // }
  };

  S3.deleteObject(params, (error, data) => {
    if(error) {
      console.log(error, error.stack);
      return callback(null, error);
    }

    callback(data, false);
  });
}

// AWS.config.loadFromPath('./s3-credentials.json');
// let S3 = new AWS.S3({
//   params: {
//     Bucket: 'myBucket'
//   }
// });
