/**
 * Socket.io configuration
 */
'use strict';

// import config from './environment';

// When the user disconnects.. perform this
function onDisconnect(/*socket*/) {}

// When the user connects.. perform this
function onConnect(socket) {
  // When the client emits 'info', this listens and executes
  socket.on('info', data => {
    socket.log(JSON.stringify(data, null, 2));
  });

  // Insert sockets below
  require('../api/purchaseOrder/purchaseOrder.socket').register(socket);
  require('../api/salesOrder/salesOrder.socket').register(socket);
  require('../api/customer/customer.socket').register(socket);
  require('../api/storage/storage.socket').register(socket);
  require('../api/material/material.socket').register(socket);
  require('../api/taxonomy.addressSection/taxonomy.addressSection.socket').register(socket);
  require('../api/taxonomy.materialClass/taxonomy.materialClass.socket').register(socket);
  require('../api/taxonomy.materialType/taxonomy.materialType.socket').register(socket);
  require('../api/taxonomy.amendmentReason/taxonomy.amendmentReason.socket').register(socket);
  require('../api/taxonomy.designation/taxonomy.designation.socket').register(socket);
  require('../api/taxonomy.customerType/taxonomy.customerType.socket').register(socket);
  require('../api/taxonomy.customerGroup/taxonomy.customerGroup.socket').register(socket);
  require('../api/taxonomy.orderCancelReason/taxonomy.orderCancelReason.socket').register(socket);
  require('../api/taxonomy.supplierGroup/taxonomy.supplierGroup.socket').register(socket);
  require('../api/taxonomy.paymentMethod/taxonomy.paymentMethod.socket').register(socket);
  require('../api/taxonomy.storageType/taxonomy.storageType.socket').register(socket);
  require('../api/supplier/supplier.socket').register(socket);
  require('../api/thing/thing.socket').register(socket);
}

export default function(socketio) {
  // socket.io (v1.x.x) is powered by debug.
  // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
  //
  // ex: DEBUG: "http*,socket.io:socket"

  // We can authenticate socket.io users and access their token through socket.decoded_token
  //
  // 1. You will need to send the token in `client/components/socket/socket.service.js`
  //
  // 2. Require authentication here:
  // socketio.use(require('socketio-jwt').authorize({
  //   secret: config.secrets.session,
  //   handshake: true
  // }));

  socketio.on('connection', function(socket) {
    socket.address = `${socket.request.connection.remoteAddress}:${socket.request.connection.remotePort}`;

    socket.connectedAt = new Date();

    socket.log = function(...data) {
      console.log(`SocketIO ${socket.nsp.name} [${socket.address}]`, ...data);
    };

    // Call onDisconnect.
    socket.on('disconnect', () => {
      onDisconnect(socket);
      socket.log('DISCONNECTED');
    });

    // Call onConnect.
    onConnect(socket);
    socket.log('CONNECTED');
  });
}
