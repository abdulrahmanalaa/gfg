import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';

function localAuthenticate(User, email, password, done) {
  User.findOne(
    {
      $or: [
        { email: email.toLowerCase(), status: 1 },
        { username: email.toLowerCase(), status: 1 }
      ]
    }
    // {
    //   email: email.toLowerCase(),
    //   status: 1
    // }
  ).exec()
    .then(user => {
      if(!user) {
        return done(null, false, {
          message: 'This username or email is not registered or account is suspended.'
        });
      }
      user.authenticate(password, function(authError, authenticated) {
        if(authError) {
          return done(authError);
        }
        if(!authenticated) {
          return done(null, false, { message: 'This password is not correct.' });
        } else {
          return done(null, user);
        }
      });
    })
    .catch(err => done(err));
}

export function setup(User/*, config*/) {
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password' // this is the virtual field on the model
  }, function(email, password, done) {
    return localAuthenticate(User, email, password, done);
  }));
}
